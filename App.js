import "expo-dev-client";

import { useFonts } from "expo-font";
import React from "react";
import ReduxThunk from "redux-thunk";
import { Provider } from "react-redux";
import { StatusBar } from "expo-status-bar";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import ContainerApp from "./ContainerApp";
import { ToastProvider } from "react-native-toast-notifications";
// STORE
import AuthReducer from "./store/reducers/AuthReducer";
import PetsReducer from "./store/reducers/PetsReducer";
import PlansReducer from "./store/reducers/PlansReducer";
import ProviderReducer from "./store/reducers/ProviderReducer";
import UiReducer from "./store/reducers/UiReducer";
import UserReducer from "./store/reducers/UserReducer";
import AppLoading from "expo-app-loading";

const rootReducer = combineReducers({
  AuthReducer: AuthReducer,
  PetsReducer: PetsReducer,
  PlansReducer: PlansReducer,
  ProviderReducer: ProviderReducer,
  UiReducer: UiReducer,
  UserReducer: UserReducer,
});
const composeEnhancers =
  (typeof window !== "undefined" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(ReduxThunk))
);

export default function App() {
  const [loaded] = useFonts({
    Baloo_Paaji_2_Bold: require("./assets/fonts/BalooPaaji2-Bold.ttf"),
    Baloo_Paaji_2_ExtraBold: require("./assets/fonts/BalooPaaji2-ExtraBold.ttf"),
    Baloo_Paaji_2_Medium: require("./assets/fonts/BalooPaaji2-Medium.ttf"),
    Baloo_Paaji_2_Regular: require("./assets/fonts/BalooPaaji2-Regular.ttf"),
    Baloo_Paaji_2_SemiBold: require("./assets/fonts/BalooPaaji2-SemiBold.ttf"),
    OpenSans_Bold: require("./assets/fonts/OpenSans-Bold.ttf"),
    OpenSans_ExtraBold: require("./assets/fonts/OpenSans-ExtraBold.ttf"),
    OpenSans_Light: require("./assets/fonts/OpenSans-Light.ttf"),
    OpenSans_Medium: require("./assets/fonts/OpenSans-Medium.ttf"),
    OpenSans_Regular: require("./assets/fonts/OpenSans-Regular.ttf"),
    OpenSans_SemiBold: require("./assets/fonts/OpenSans-SemiBold.ttf"),
  });
  if (!loaded) {
    return <AppLoading />;
  }
  return (
    <Provider store={store}>
      <ToastProvider offsetTop={30} offsetBottom={40} swipeEnabled={true}>
        <ContainerApp />
      </ToastProvider>
      <StatusBar hidden />
    </Provider>
  );
}

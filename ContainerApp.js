import React, { useEffect } from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import Preloader from "./components/Preloader";
import Navigation from "./navigation";
import { getAllWords } from "./store/actions/ui";

const ContainerApp = ({ loading, getAllWords, words, lang }) => {
  useEffect(async () => {
    if (words && words[lang].length == 0) await getAllWords();
  }, []);
  return (
    <View style={styles.container}>
      {words && words[lang].length > 0 && <Navigation />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "relative",
  },
});
const mapStateToProps = (state) => {
  return {
    loading: state.UiReducer.loading,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllWords: (pet) => dispatch(getAllWords()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContainerApp);

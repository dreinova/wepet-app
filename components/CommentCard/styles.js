import { Dimensions, StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  cardContainer: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    elevation: 5,
    marginHorizontal: 5,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: Dimensions.get("window").width - 50,
    marginBottom: 20,
  },
  cardPhoto: {
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    marginRight: 10,
  },
  cardName: {
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 20,
    textTransform: "uppercase",
    color: "#454545",
  },
  cardTime: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 12,
    color: "#565656",
  },
  cardComment: {
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 14,
    color: "#454545",
    lineHeight: 20,
  },
});

import * as Clipboard from "expo-clipboard";
import React, { useEffect, useRef, useState } from "react";

import { Animated, Button, Pressable, Text, View } from "react-native";
import styles from "./styles";
import { useToast } from "react-native-toast-notifications";
const Message = (props) => {
  const opacity = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
      Animated.delay(2000),
      Animated.timing(opacity, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }),
    ]).start(() => {
      props.onHide();
    });
  }, []);

  return (
    <Animated.View
      style={{
        opacity,
        transform: [
          {
            translateY: opacity.interpolate({
              inputRange: [0, 1],
              outputRange: [-20, 0],
            }),
          },
        ],
        margin: 10,
        marginBottom: 5,
        backgroundColor: "#FB883F",
        padding: 10,
        borderRadius: 4,
        shadowColor: "black",
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.15,
        shadowRadius: 5,
        elevation: 6,
        alignItems: "center",
        zIndex: 999,
      }}
    >
      <Text style={{ color: "#FFF", fontFamily: "Baloo_Paaji_2_Bold" }}>
        {props.message}
      </Text>
    </Animated.View>
  );
};
export const CuponCard = (props) => {
  const toast = useToast();
  const { code, category, discount } = props.cupon;
  const [messages, setMessages] = useState([]);
  const copyToClipboard = () => {
    Clipboard.setString(code);
    toast.show("Cupón copiado!", {
      type: "normal",
      placement: "bottom",
      duration: 2000,
      animationType: "zoom-in",
    });
  };
  return (
    <>
      <View
        style={{
          position: "absolute",
          top: 45,
          left: 0,
          right: 0,
        }}
      >
        {messages.map((message) => (
          <Message
            key={message}
            message={message}
            onHide={() => {
              setMessages((messages) =>
                messages.filter((currentMessage) => currentMessage !== message)
              );
            }}
          />
        ))}
      </View>
      <Pressable
        style={({ pressed }) => [
          {
            opacity: pressed ? 0.5 : 1,
          },
          styles.cuponContainer,
        ]}
        onPress={copyToClipboard}
      >
        <View style={[styles.left, styles.side]}>
          <Text style={styles.code}>{code}</Text>
          <Text style={styles.codeInst}>
            {props.text}
          </Text>
        </View>
        <View style={[styles.right, styles.side]}>
          <Text style={styles.discount}>-{discount}%</Text>
          <Text style={styles.codeInst}>En la categoría</Text>
          <Text style={styles.codeInst}>{category}</Text>
        </View>
      </Pressable>
    </>
  );
};

export default CuponCard;

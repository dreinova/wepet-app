const { StyleSheet, Dimensions } = require("react-native");

module.exports = StyleSheet.create({
  cuponContainer: {
    flexDirection: "row",
    marginBottom: 10,
    marginRight: 15,
  },
  side: {
    alignItems: "center",
    flex: 1,
    height: 109,
    justifyContent: "center",
    maxWidth: Dimensions.get("window").width - 200,
    paddingHorizontal: 20,
  },

  left: {
    backgroundColor: "#E5E4EB",
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15,
  },
  right: {
    backgroundColor: "#F2D8B6",
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
  },
  code: {
    textAlign: "center",
    color: "#336065",
    fontSize: 18,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    lineHeight: 41,
  },
  codeInst: {
    textAlign: "center",
    color: "#336065",
    fontSize: 12,
    fontStyle: "normal",
    fontWeight: "normal",
  },
  discount: {
    textAlign: "center",
    color: "#336065",
    fontSize: 40,
    fontStyle: "normal",
    fontWeight: "bold",
  },
});

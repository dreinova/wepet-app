import React, { useState } from "react";
import {
  View,
  Text,
  Pressable,
  TouchableWithoutFeedback,
  StyleSheet,
  Dimensions,
  ScrollView,
  TextInput,
  FlatList,
} from "react-native";
const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;

export const ModalPicker = (props) => {
  const onPressItem = (option) => {
    props.changeModalVisibility(false);
    props.setData(option);
  };
  const [sortOptions, setSortOptions] = useState(
    props.options.sort(function (a, b) {
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }
      return 0;
    })
  );
  const autocomplete = (val) => {
    var reg = new RegExp(val.toLowerCase());
    setSortOptions(
      props.options.filter(function (term) {
        if (term.toLowerCase().match(reg)) {
          return term;
        }
      })
    );
  };
  return (
    <TouchableWithoutFeedback
      style={{ flex: 1 }}
      onPress={() => props.changeModalVisibility(false)}
    >
      <View style={styles.container}>
        {props.isAutocomplete && (
          <TextInput
            placeholder="Buscar raza"
            placeholderTextColor="#336065"
            style={{
              backgroundColor: "#e5e4eb",
              width: "100%",
              borderRadius: 10,
              fontSize: 14,
              color: "#336065",
              paddingHorizontal: 25,
              marginBottom: 10,
              paddingVertical: 15,
            }}
            onChangeText={(text) => autocomplete(text)}
          />
        )}
        <Pressable onPress={() => props.changeModalVisibility(false)}>
          <View
            style={[styles.modal, { width: WIDTH - 2, maxHeight: HEIGHT / 2 }]}
          >
            <FlatList
              showsVerticalScrollIndicator={false}
              data={sortOptions}
              renderItem={({ item, index }) => (
                <Pressable
                  key={index}
                  onPress={() => onPressItem(item)}
                  style={({ pressed }) => [
                    {
                      opacity: pressed ? 0.5 : 1,
                    },
                    item === props.chooseData && { backgroundColor: "#309DA3" },
                  ]}
                >
                  <Text
                    style={[
                      styles.text,
                      item === props.chooseData && { color: "#FFF" },
                    ]}
                  >
                    {item}
                  </Text>
                  <View style={{ backgroundColor: "#336065", height: 1 }} />
                </Pressable>
              )}
              extraData={sortOptions}
            />
          </View>
        </Pressable>
      </View>
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,.8)",
  },
  modal: { backgroundColor: "#e5e4eb", borderRadius: 5 },
  text: {
    color: "#336065",
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 12,
    textAlign: "center",
  },
});

export default ModalPicker;

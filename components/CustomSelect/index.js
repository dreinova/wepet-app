import React, { useState } from "react";

import { View, Text, StyleSheet, Modal, Pressable } from "react-native";
import ModalPicker from "./ModalPicker";
export const CustomSelect = (props) => {
  const [chooseData, setChooseData] = useState(
    props.chooseData ? props.chooseData : ""
  );
  const [isModalVisible, setIsModalVisible] = useState(false);
  const changeModalVisibility = (bool) => {
    setIsModalVisible(bool);
  };
  const setData = (option) => {
    setChooseData(option);
    props.changeOption(option);
  };
  return (
    <View style={styles.container}>
      {props.label && <Text style={styles.label}>{props.label}</Text>}
      <Pressable
        style={({ pressed }) => [
          {
            opacity: pressed ? 0.5 : 1,
          },
          styles.touchableOpacity,
          { ...props.style },
        ]}
        onPress={() => changeModalVisibility(true)}
      >
        {chooseData ? (
          <Text style={[styles.text, { ...props.textStyle }]}>
            {chooseData}
          </Text>
        ) : (
          <Text style={[styles.text, { ...props.textStyle }]}>
            {!!props.defaultValue
              ? props.defaultValue
              : "Selecciona una opción"}
          </Text>
        )}
      </Pressable>
      <Modal
        transparent
        animationType="fade"
        visible={isModalVisible}
        onRequestClose={() => changeModalVisibility(false)}
      >
        <ModalPicker
          changeModalVisibility={setIsModalVisible}
          setData={setData}
          chooseData={chooseData}
          options={props.options}
          isAutocomplete={props.isAutocomplete}
        />
      </Modal>
    </View>
  );
};

export default CustomSelect;

const styles = StyleSheet.create({
  container: {
    marginBottom: 30,
  },
  touchableOpacity: {
    alignSelf: "stretch",
    paddingHorizontal: 20,
    backgroundColor: "#e5e4eb",
    width: "100%",
    borderRadius: 10,
    minHeight: 50,
    paddingHorizontal: 25,
    paddingVertical: 10,
    justifyContent: "center",
  },
  text: {
    fontSize: 14,
    color: "#336065",
  },
  label: {
    color: "#336065",
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    marginBottom: 8,
  },
});

import React, { useRef } from "react";
import styles from "./styles";
import { View, Text, Pressable } from "react-native";

export const CustomSwitch = (props) => {
  return (
    <Pressable
      onPress={() => props.onPress()}
      style={({ pressed }) => [
        {
          opacity: pressed ? 0.5 : 1,
        },
        styles.switchContainer,
        props.active && styles.switchContainerActive,
      ]}
    >
      <View
        style={[props.active ? styles.circleSwitchActive : styles.circleSwitch]}
      />
    </Pressable>
  );
};

export default CustomSwitch;

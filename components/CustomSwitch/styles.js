export default {
  switchContainer: {
    alignItems: "center",
    backgroundColor: "#e5e4eb",
    borderColor: "#e5e4eb",
    borderRadius: 100,
    borderWidth: 2,
    flexDirection: "row",
    height: 35,
    width: 72,
    padding: 4,
    marginBottom: 30,
  },
  switchContainerActive: {
    backgroundColor: "#309da3",
    borderColor: "#309da3",
  },
  circleSwitch: {
    backgroundColor: "#b2b2b2",
    height: 29,
    width: 29,
    borderRadius: 100,
    position: "absolute",
    left: 2,
  },
  circleSwitchActive: {
    left: 30,
    backgroundColor: "#336065",
    height: 29,
    width: 29,
    borderRadius: 100,
  },
};

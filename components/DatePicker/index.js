import React, { useState } from "react";
import styles from "./styles";
import { View, Text, Button, Pressable, Platform } from "react-native";
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import IconSvg from "../IconSvg";
export const DatePicker = (props) => {
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    setShow(Platform.OS === "ios");
    setDate(selectedDate);
    props.setDateValue(moment(selectedDate).format("YYYY-MM-DD"));
  };

  const showMode = (currentMode) => {
    setShow(!show);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };

  return (
    <>
      {Platform.OS === "android" ? (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: 30,
          }}
        >
          <View style={{ flexDirection: "column", alignItems: "flex-end" }}>
            {props.label && <Text style={styles.label}>{props.label}</Text>}
            <Text style={styles.text}>{moment(date).format("DD/MM/YYYY")}</Text>
          </View>
          <Pressable
            style={({ pressed }) => [
              {
                marginLeft: 10,
                alignItems: "center",
                padding: 20,
                justifyContent: "center",
                backgroundColor: "#eee",
                borderRadius: 40,
                opacity: pressed ? 0.5 : 1,
              },
            ]}
            onPress={showDatepicker}
          >
            <IconSvg
              width="20"
              height="20"
              icon={`<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M28.5722 3.40195H25.2225V1.54394C25.2225 0.69121 24.5313 0 23.6783 0H23.4335C22.5807 0 21.8898 0.69121 21.8898 1.54394V3.40195H10.0713V1.54394C10.0713 0.69121 9.38005 0 8.52757 0H8.28247C7.42974 0 6.73879 0.69121 6.73879 1.54394V3.40195H3.42811C1.67624 3.40195 0.250488 4.82719 0.250488 6.57957V28.8226C0.250488 30.574 1.67624 32.0003 3.42811 32.0003H28.5724C30.324 32.0003 31.75 30.5742 31.75 28.8226V6.57957C31.7498 4.82719 30.3238 3.40195 28.5722 3.40195ZM28.1576 28.4081H3.84268V11.0932H28.1576V28.4081Z" fill="#336065"/></svg>`}
            />
          </Pressable>

          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={onChange}
              style={{ width: "62%" }}
            />
          )}
        </View>
      ) : (
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: 30,
          }}
        >
          {props.label && <Text style={styles.label}>{props.label}</Text>}
          {/* 
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              styles.input,
            ]}
            onPress={showDatepicker}
          >
            <Text style={styles.text}>{moment(date).format("DD/MM/YYYY")}</Text>
          </Pressable> */}

          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={mode}
            is24Hour={true}
            display="default"
            onChange={onChange}
            style={{ width: "62%" }}
          />
        </View>
      )}
    </>
  );
};

export default DatePicker;

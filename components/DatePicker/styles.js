export default {
  input: {
    backgroundColor: "#e5e4eb",
    width: "100%",
    borderRadius: 10,
    marginBottom: 30,
    paddingHorizontal: 25,
    paddingVertical: 10,
    minHeight: 50,
    justifyContent: "center",
  },
  text: {
    fontSize: 14,
    color: "#336065",
  },
  label: {
    color: "#336065",
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    marginBottom: 8,
  },
};

import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Bold = (props) => {
  return <Text style={[props.style, Fonts.texto_bold]}>{props.children}</Text>;
};

export default Bold;

import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Caption = (props) => {
  return <Text style={[Fonts.caption, props.style]}>{props.children}</Text>;
};

export default Caption;

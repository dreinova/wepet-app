import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Display = (props) => {
  return <Text style={[Fonts.display, props.style]}>{props.children}</Text>;
};

export default Display;

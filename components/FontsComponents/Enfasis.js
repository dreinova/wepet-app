import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Enfasis = (props) => {
  return <Text style={[Fonts.enfasis, props.style]}>{props.children}</Text>;
};

export default Enfasis;

import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Regular = (props) => {
  return (
    <Text {...props} style={[Fonts.texto_regular, props.style]}>
      {props.children}
    </Text>
  );
};

export default Regular;

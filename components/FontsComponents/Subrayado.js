import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Subrayado = (props) => {
  return (
    <Text style={[Fonts.texto_subrayado, props.style]}>{props.children}</Text>
  );
};

export default Subrayado;

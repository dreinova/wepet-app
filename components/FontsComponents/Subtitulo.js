import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Subtitulo = (props) => {
  return <Text style={[Fonts.subtitulo, props.style]}>{props.children}</Text>;
};

export default Subtitulo;

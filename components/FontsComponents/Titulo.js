import React from "react";
import { Text } from "react-native";
import Fonts from "../../constants/Fonts";

export const Titulo = (props) => {
  return <Text style={[Fonts.titulo, props.style]}>{props.children}</Text>;
};

export default Titulo;

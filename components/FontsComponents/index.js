export { default as Bold } from "./Bold";
export { default as Caption } from "./Caption";
export { default as Display } from "./Display";
export { default as Enfasis } from "./Enfasis";
export { default as Regular } from "./Regular";
export { default as Subrayado } from "./Subrayado";
export { default as Subtitulo } from "./Subtitulo";
export { default as Titulo } from "./Titulo";

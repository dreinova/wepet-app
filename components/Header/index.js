import React, { cloneElement } from "react";
import styles from "./styles";
import { View, FlatList, Pressable } from "react-native";
import IconSvg from "../IconSvg";
import PetAvatar from "../PetAvatar";
import { MYPETS } from "../../data/myPets";

export const Header = (props) => {
  const activePet = "239";
  return (
    <View style={{ marginVertical: 10, paddingHorizontal: 20 }}>
      <View style={{ flexDirection: "row" }}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={MYPETS}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => (
            <PetAvatar key={index} pet={item} isActive={activePet} />
          )}
          fadingEdgeLength={100}
        />
        <Pressable
          onPress={() => {}}
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            styles.addPet,
          ]}
        >
          <IconSvg
            width="20"
            height="20"
            icon={`<svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.7031 24.0938H0.585938V15.5391H15.7031V0.304688H24.2578V15.5391H39.375V24.0938H24.2578V39.1328H15.7031V24.0938Z" fill="#FFBC00"/></svg>`}
          />
        </Pressable>
      </View>
    </View>
  );
};

export default Header;

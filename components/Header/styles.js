const { StyleSheet } = require("react-native");

const size = 60;
module.exports = StyleSheet.create({
  tinyLogo: {
    width: 180,
    resizeMode: "contain",
    height: 100,
  },
  addPet: {
    borderWidth: 3,
    borderColor: "#FFBC00",
    width: size,
    height: size,
    borderRadius: size / 2,
    alignItems: "center",
    justifyContent: "center",
  },
});

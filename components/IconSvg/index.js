import React from "react";
import { View } from "react-native";
import { SvgXml } from "react-native-svg";

const IconSvg = (props) => {
  return (
    <View style={{ ...props.style }}>
      <SvgXml xml={props.icon} width={props.width} height={props.height} />
    </View>
  );
};

export default IconSvg;

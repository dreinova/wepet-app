import React from "react";
import styles from "./styles";
import { View, Text, Pressable, Animated } from "react-native";
const animated = new Animated.Value(1);
export const MainBtn = (props) => {
  const { text, primary, primary2, secondary, tertiary } = props;
  const fadeIn = () => {
    Animated.timing(animated, {
      toValue: 0.1,
      duration: 100,
      useNativeDriver: true,
    }).start();
  };
  const fadeOut = () => {
    Animated.timing(animated, {
      toValue: 1,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };
  return (
    <Pressable
      onPressIn={fadeIn}
      onPressOut={fadeOut}
      {...props}
      style={({ pressed }) => [
        styles.mainBtn,
        primary && styles.mainBtn_primary,
        primary2 && styles.mainBtn_primary2,
        secondary && styles.mainBtn_secondary,
        tertiary && styles.mainBtn_tertiary,

        {
          opacity: pressed ? 0.5 : 1,
        },
        props.style,
      ]}
    >
      {props.children}
      <Text
        style={[
          styles.textBtn,
          primary && styles.textBtn_primary,
          primary2 && styles.textBtn_primary2,
          secondary && styles.textBtn_secondary,
          tertiary && styles.textBtn_tertiary,
          props.styleText,
        ]}
      >
        {text}
      </Text>
    </Pressable>
  );
};

export default MainBtn;

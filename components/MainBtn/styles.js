export default {
  mainBtn: {
    alignItems: "center",
    borderRadius: 100,
    justifyContent: "center",
    marginBottom: 15,
    paddingHorizontal: 30,
    paddingVertical: 8,
    textTransform: "uppercase",
    flexDirection: "row",
  },
  textBtn: {
    fontSize: 14,
    lineHeight: 22,
    textAlign: "center",
    textTransform: "uppercase",
  },
  mainBtn_primary: {
    backgroundColor: "#ff9700",
  },
  mainBtn_primary2: {
    backgroundColor: "#336065",
  },
  mainBtn_primary: {
    backgroundColor: "#ff9700",
  },
  mainBtn_secondary: {
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "#ff9700",
  },
  mainBtn_tertiary: {},
  textBtn_primary2: { color: "#fff", fontStyle: "normal", fontWeight: "bold" },
  textBtn_primary: { color: "#fff", fontStyle: "normal", fontWeight: "bold" },
  textBtn_secondary: {
    color: "#ff9700",
    fontStyle: "normal",
    fontWeight: "bold",
  },
  textBtn_tertiary: {
    color: "#ff9700",
    fontStyle: "normal",
    fontWeight: "normal",
    textDecorationLine: "underline",
  },
};

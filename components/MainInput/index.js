import React, { forwardRef, useState, useImperativeHandle } from "react";
import styles from "./styles";
import { View, Text, TextInput } from "react-native";

export const MainInput = forwardRef((props, ref) => {
  const { label } = props;
  const [error, setError] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");
  useImperativeHandle(ref, () => ({
    errorFunction(state = false, msg = "Este campo es obligatorio") {
      setError(state);
      if (!state) setErrorMessage(msg);
    },
  }));

  return (
    <View style={styles.inputContainer}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        autoCapitalize="none"
        multiline={props.textarea}
        style={[
          styles.input,
          !error && styles.errorInput,
          props.textarea && styles.textareaInput,
        ]}
        {...props}
      />
      {!error && <Text style={styles.labelError}>{errorMessage}</Text>}
    </View>
  );
});

export default MainInput;

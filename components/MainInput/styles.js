export default {
  inputContainer: {
    marginBottom: 30,
  },
  label: {
    color: "#336065",
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    lineHeight: 14,
    marginBottom: 8,
  },
  labelError: {
    color: "#F00",
    fontSize: 10,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    lineHeight: 14,
    padding: 8,
  },
  input: {
    backgroundColor: "#e5e4eb",
    width: "100%",
    borderRadius: 10,
    fontSize: 14,
    color: "#336065",
    paddingHorizontal: 25,
    paddingVertical: 15,
    
    textAlignVertical: "center",
  },
  textareaInput: {
    textAlignVertical: "top",
    minHeight: 100,
  },
  errorInput: {
    borderWidth: 1,
    borderColor: "#f00",
  },
};

import React from "react";
import styles from "./styles";
import { View, Text, Image, Dimensions } from "react-native";
const SIZE = Dimensions.get("window").width - 40;
export const NewsCard = (props) => {
  const {
    new: {
      image: { url },
      name,
    },
  } = props;
  return (
    <View style={styles.newContainer}>
      <Image
        source={{ uri: url }}
        style={{ width: SIZE, height: SIZE, borderRadius: 8 }}
      />
      <View style={styles.nameContainer}>
        <Text style={styles.name}>{name}</Text>
      </View>
    </View>
  );
};

export default NewsCard;

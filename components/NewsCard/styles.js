const { StyleSheet } = require("react-native");

module.exports = StyleSheet.create({
  newContainer: {},
  nameContainer: {
    alignItems: "center",
    backgroundColor: "rgba(51,96,101,.6)",
    bottom: 0,
    left: 0,
    paddingHorizontal: 18,
    paddingVertical: 5,
    position: "absolute",
    width: "100%",
  },
  name: {
    color: "#fff",
    letterSpacing: -0.02,
    lineHeight: 33,
    fontSize: 18,
    fontStyle: "normal",
    fontWeight: "bold",
  },
});

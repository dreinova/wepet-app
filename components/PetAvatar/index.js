import React from "react";
import styles from "./styles";
import { View, Text, Pressable, Image } from "react-native";
import MainBtn from "../MainBtn";

export const PetAvatar = (props) => {
  const {
    isActive,
    pet: { id, image, name },
    onPress,
    closeActionSheet,
    navigation,
    setCancelSub,
    setToDelete,
    setActivePet,
    selccionar,
    editar,
    cancelar,
    pets,
  } = props;
  return (
    <Pressable
      style={({ pressed }) => [
        {
          opacity: pressed ? 0.5 : 1,
        },
        {
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginVertical: 15,
        },
      ]}
    >
      <View
        style={{ alignItems: "center", justifyContent: "center", width: 70 }}
      >
        <Image
          style={[styles.avatar, isActive === id && styles.activePet]}
          source={{
            uri:
              image.length > 0
                ? image[0].url
                : "https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg",
          }}
        />
        <Text style={styles.namePet}>{name}</Text>
      </View>
      <View
        style={{
          marginVertical: 10,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {isActive != id ? (
          <MainBtn
            primary
            text="Seleccionar"
            style={{
              marginBottom: 0,
              marginHorizontal: 8,
              paddingHorizontal: 8,
              paddingVertical: 0,
            }}
            styleText={{ fontSize: 12 }}
            onPress={() => {
              onPress();
              closeActionSheet();
            }}
          />
        ) : (
          <MainBtn
            primary
            text={selccionar}
            style={{
              marginBottom: 0,
              marginHorizontal: 8,
              paddingHorizontal: 8,
              paddingVertical: 0,
              opacity: 0,
            }}
            styleText={{ fontSize: 12 }}
          />
        )}
        <MainBtn
          secondary
          text={editar}
          style={{
            marginBottom: 0,
            marginHorizontal: 15,
            paddingHorizontal: 8,
            paddingVertical: 0,
          }}
          styleText={{ fontSize: 12 }}
          onPress={() => {
            setActivePet();
            navigation.navigate("PetProfile");
            closeActionSheet();
          }}
        />
        {pets.length > 1 && (
          <MainBtn
            tertiary
            text={cancelar}
            style={{
              marginBottom: 0,
              borderRadius: 0,
              marginHorizontal: 15,
              paddingHorizontal: 0,
              paddingVertical: 0,
              width: 80,
            }}
            styleText={{ fontSize: 10, color: "#f77053", lineHeight: 14 }}
            onPress={() => {
              setToDelete();
              closeActionSheet();
              setCancelSub(true);
            }}
          />
        )}
      </View>
    </Pressable>
  );
};

export default PetAvatar;

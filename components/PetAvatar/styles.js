import { StyleSheet } from "react-native";
const size = 40;
module.exports = StyleSheet.create({
  avatar: {
    width: size,
    height: size,
    borderRadius: size / 2,
    
  },
  activePet: {
    width: size * 1.1,
    height: size * 1.1,
    borderRadius: (size * 1.1) / 2,
    borderColor: "#FB883F",
    borderWidth: 2,
  },
  namePet: {
    color: "#309DA3",
    fontFamily: "OpenSans_Bold",
    fontSize: 12,
    lineHeight: 22,
    letterSpacing: -0.02,
  },
});

import React from "react";
import styles from "./styles";
import { View, Text, useWindowDimensions } from "react-native";
import RenderHtml, {
  HTMLContentModel,
  HTMLElementModel,
} from "react-native-render-html";
import MainBtn from "../MainBtn";
import { useNavigation } from "@react-navigation/native";
import { getAllDataRegister } from "../../store/actions/ui";
import { connect } from "react-redux";

export const PlanCard = ({
  getAllData,
  name,
  price,
  period,
  desc,
  recommended,
  plan,
  planPeriod,
  createPet,
  finalPrice,
  validatePay,
  uemail,
  uname,
  ulastname,
  appleID,
}) => {
  const navigation = useNavigation();
  const { width } = useWindowDimensions();
  const source = {
    html: desc,
  };
  const customHTMLElementModels = {
    strong: HTMLElementModel.fromCustomModel({
      tagName: "strong",
      mixedUAStyles: {
        // backgroundColor: "#f00",
        width: 200,
        borderRadius: 5,
        fontStyle: "italic",
        color: "#565656",
        textAlign: "left",
        fontSize: 12,
        lineHeight: 20,
        position: "absolute",
      },
      contentModel: HTMLContentModel.block,
    }),
    li: HTMLElementModel.fromCustomModel({
      tagName: "li",
      mixedUAStyles: {
        marginBottom: 10,
        color: "#565656",
        // backgroundColor: "#f00",
      },
      contentModel: HTMLContentModel.block,
    }),
  };
  const currencyFormat = (num) => {
    return (
      "$" +
      parseInt(num)
        .toFixed(0)
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    );
  };
  return (
    <View
      style={[
        styles.cardContainer,
        { width: width - 50 },
        recommended && styles.recommended,
      ]}
    >
      {recommended && (
        <View style={styles.badge}>
          <Text style={styles.badgeText}>Recomendado</Text>
        </View>
      )}
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.price}>{currencyFormat(price)}</Text>
      <Text style={styles.period}>{period}</Text>
      <RenderHtml
        contentWidth={width}
        source={source}
        customHTMLElementModels={customHTMLElementModels}
      />
      <Text style={styles.terms}>
        * Aplican tiempos de carencia para el uso de los servicios
      </Text>
      <MainBtn
        primary
        text={`Seleccionar plan ${name}`}
        style={{ paddingVertical: 10 }}
        onPress={async () => {
          if (createPet) {
            if (finalPrice == 0) {
              await getAllData();
              validatePay();
            } else {
              validatePay();
            }
          } else {
            if (finalPrice == 0) {
              await getAllData();
              navigation.navigate("RegisterScreen", {
                plan,
                planPeriod,
                finalPrice: 0,
                uemail: uemail ? uemail : "",
                uname: uname ? uname : "",
                ulastname: ulastname ? ulastname : "",
                appleID: appleID ? appleID : "",
              });
            } else {
              navigation.navigate("PayScreen", {
                plan,
                planPeriod,
                finalPrice: planPeriod ? price : finalPrice,
                uemail: uemail ? uemail : "",
                uname: uname ? uname : "",
                ulastname: ulastname ? ulastname : "",
                appleID: appleID ? appleID : "",
              });
            }
          }
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllData: async () => await dispatch(getAllDataRegister()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlanCard);

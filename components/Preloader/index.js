import React from "react";
import { StyleSheet, Text, View, Button, Dimensions } from "react-native";
import AnimatedLottieView from "lottie-react-native";
export default class Preloader extends React.Component {
  componentDidMount() {
    this.animation.play();
  }

  render() {
    return (
      <View style={styles.container}>
        <AnimatedLottieView
          autoPlay
          loop
          ref={(animation) => {
            this.animation = animation;
          }}
          style={{
            width: 180,
            height: 180,
          }}
          source={require("../../assets/preloader.json")}
          duration={1000}
        />
      </View>
    );
    return <View style={styles.container}></View>;
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height - 90,
    backgroundColor: "rgba(255,255,255,.8)",
  },
});

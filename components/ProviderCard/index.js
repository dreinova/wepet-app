import React, { useState } from "react";
import styles from "./styles";
import {
  View,
  Text,
  Image,
  Dimensions,
  Pressable,
  Modal,
  ScrollView,
  TouchableWithoutFeedback,
  FlatList,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
const SIZE = Dimensions.get("window").width - 60;
import RenderHtml, {
  HTMLContentModel,
  HTMLElementModel,
} from "react-native-render-html";
import IconSvg from "../IconSvg";
import MainBtn from "../MainBtn";
import { useNavigation } from "@react-navigation/native";
import RenderHTML from "react-native-render-html";
import { connect } from "react-redux";
import { getSingleProvider } from "../../store/actions/provider";
export const ProviderCard = (props) => {
  const navigation = useNavigation();
  const { name, subtitle, images, id, description, schedule, places } =
    props.provider;
  const { getSingleProvider, serviceID } = props;
  const scheduleSource = {
    html: schedule,
  };
  const placesSource = {
    html: places,
  };
  const customHTMLElementModels = {
    ul: HTMLElementModel.fromCustomModel({
      tagName: "ul",
      mixedUAStyles: {
        margin: 0,
        marginBottom: 30,
        color: "#565656",
      },
      contentModel: HTMLContentModel.block,
    }),
    li: HTMLElementModel.fromCustomModel({
      tagName: "li",
      mixedUAStyles: {
        marginBottom: 10,
        fontStyle: "normal",
        fontSize: 14,
        lineHeight: 25,
        letterSpacing: -0.02,
        color: "#565656",
      },
      contentModel: HTMLContentModel.block,
    }),
    p: HTMLElementModel.fromCustomModel({
      tagName: "p",
      mixedUAStyles: {
        color: "#565656",
        letterSpacing: -0.02,
        lineHeight: 22,
        textAlign: "justify",
        marginBottom: 15,
      },
      contentModel: HTMLContentModel.block,
    }),
  };

  const [providerModalVisible, setProviderModalVisible] = useState(false);
  return (
    <View>
      <Pressable
        style={({ pressed }) => [
          {
            opacity: pressed ? 0.5 : 1,
          },
          styles.cardContainer,
        ]}
        onPress={async () => {
          props.onPress();
          const prov = await getSingleProvider(id);
          if (prov) {
            props.removeLoading();
            navigation.navigate("ProviderScreen", { serviceID });
          }
        }}
      >
        {props.provider &&
        props.provider.profile_image &&
        props.provider.profile_image.url ? (
          <Image
            style={styles.cardImage}
            source={{
              uri: props.provider.profile_image.url,
            }}
          />
        ) : (
          <View
            style={[
              styles.cardImage,
              {
                backgroundColor: "#ccc",
                alignItems: "center",
                justifyContent: "center",
              },
            ]}
          >
            <Image
              style={{ width: 80, height: 80 }}
              source={require("../../assets/logo.png")}
            />
          </View>
        )}
        <Text style={styles.nameProvider}>{name}</Text>
        <Text style={styles.subtitleProvider}>{subtitle}</Text>
      </Pressable>
    </View>
  );
};
const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSingleProvider: (id) => dispatch(getSingleProvider(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProviderCard);

import { Dimensions, StyleSheet } from "react-native";
const SIZE = Dimensions.get("window").width - 60;
module.exports = StyleSheet.create({
  cardContainer: {
    padding: 20,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    elevation: 5,
    marginHorizontal: 5,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    flex: 1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  cardImage: {
    width: SIZE / 2 - 20,
    height: SIZE / 2 - 20,
    borderRadius: SIZE / 2,
    marginBottom: 15,
  },
  nameProvider: {
    color: "#309DA3",
    fontSize: 18,
    fontStyle: "normal",
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
    width: 150,
  },
  subtitleProvider: {
    color: "#565656",
    fontSize: 12,
    fontStyle: "normal",
    fontWeight: "normal",
    textAlign: "center",
  },
  modalNameProvider: {
    color: "#336065",
    fontSize: 30,
    fontStyle: "normal",
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
  },
  modalSubtitleProvider: {
    color: "#336065",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "normal",
    textAlign: "center",
    marginBottom: 16,
  },
});

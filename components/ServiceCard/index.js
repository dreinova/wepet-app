import React, { useState } from "react";
import styles from "./styles";
import {
  View,
  Text,
  Image,
  Dimensions,
  Pressable,
  Linking,
} from "react-native";
import IconSvg from "../IconSvg";
import { useNavigation } from "@react-navigation/native";

function CompleteCard(props) {
  const navigation = useNavigation();
  const {
    name,
    image: { url },
  } = props.service;
  return (
    <Pressable
      onPress={() => {
        props.onPress();
      }}
    >
      <View style={styles.cardContainer}>
        <View style={styles.circle}>
          <Text style={styles.circleTxt}>{props.count}</Text>
        </View>

        <Image
          style={styles.cardImage}
          source={{
            uri: url,
          }}
        />
        <Text style={styles.cardTitle} textBreakStrategy="simple">
          {name}
        </Text>
      </View>
    </Pressable>
  );
}
function ExtraCard(props) {
  const {
    name,
    image: { url },
  } = props.service;
  const { buy, setBuy } = props;
  return (
    <View
      style={[
        styles.cardContainer,
        { overflow: "hidden", width: Dimensions.get("window").width - 40 },
      ]}
    >
      <View
        style={{
          backgroundColor: "rgba(229, 228, 235, .6)",
          width: Dimensions.get("window").width - 40,
          height: Dimensions.get("window").width / 2 - 40,
          position: "absolute",
          top: 0,
          left: 0,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Text style={styles.noServices}>Agotaste tus servicios </Text>
        <Pressable
          onPress={async () => {
            setBuy(true);
            await props.onPressExtra();
            setBuy(false);
          }}
          style={{
            backgroundColor: "#FFBC00",
            borderRadius: 20,
            paddingVertical: 8,
            paddingHorizontal: 20,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <IconSvg
            width="14"
            height="23"
            icon={`<svg width="14" height="23" viewBox="0 0 14 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.41699 10.0028C4.67512 9.29019 3.79337 8.55339 3.79337 7.40592C3.79337 6.08935 5.01332 5.17137 7.05463 5.17137C9.20464 5.17137 10.0018 6.19805 10.0743 7.70788H12.7437C12.6591 5.63036 11.3909 3.72193 8.86643 3.10592V0.460693H5.24282V3.06968C2.89955 3.57699 1.01527 5.0989 1.01527 7.43008C1.01527 10.2202 3.3223 11.6093 6.69226 12.4186C9.71194 13.1433 10.3159 14.2062 10.3159 15.3295C10.3159 16.1629 9.72402 17.4916 7.05463 17.4916C4.56641 17.4916 3.58803 16.3804 3.45517 14.9551H0.797852C0.942796 17.6003 2.92371 19.086 5.24282 19.5812V22.2023H8.86643V19.6054C11.2218 19.1584 13.094 17.7936 13.094 15.3174C13.094 11.8871 10.1589 10.7155 7.41699 10.0028Z" fill="#336065"/></svg>`}
          />
          <Text
            style={{
              color: "#336065",
              fontSize: 12,
              lineHeight: 20,
              textAlign: "center",
            }}
          >
            {buy ? `Comprando...` : `Comprar uno extra`}
          </Text>
        </Pressable>
      </View>
      <Image
        style={[
          styles.cardImage,
          {
            zIndex: -2,
            width: Dimensions.get("window").width - 40,
            height: Dimensions.get("window").width / 2 - 40,
          },
        ]}
        source={{
          uri: url,
        }}
      />

      <Text style={[styles.cardTitle, { color: "#565656" }]}>{name}</Text>
    </View>
  );
}
function UpCard(props) {
  const {
    name,
    image: { url },
  } = props.service;
  const { buy, setBuy } = props;
  return (
    <View
      style={[
        styles.cardContainer,
        { overflow: "hidden", width: Dimensions.get("window").width - 40 },
      ]}
    >
      <View
        style={{
          backgroundColor: "rgba(229, 228, 235, .6)",
          width: Dimensions.get("window").width - 40,
          height: Dimensions.get("window").width / 2 - 40,
          position: "absolute",
          top: 0,
          left: 0,
          alignItems: "center",
          justifyContent: "center",
          paddingHorizontal: 20,
        }}
      >
        <Text style={styles.noServices}>{props.nodisponible}</Text>
        <View style={{ flexDirection: "row" }}>
          <Pressable
            onPress={() => {
              Linking.openURL(
                `https://wepet.co/mi-cuenta/s/upgradePlan/?userID=${props.userID}&petID=${props.petID}`
              );
            }}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                backgroundColor: "#FFBC00",
                borderRadius: 20,
                paddingVertical: 8,
                paddingHorizontal: 20,
                alignItems: "center",
                justifyContent: "center",
                marginRight: 5,
              },
            ]}
          >
            <IconSvg
              width="25"
              height="25"
              icon={`<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.8145 24.618C19.482 24.618 24.8932 19.2067 24.8932 12.5393C24.8932 5.87191 19.482 0.460678 12.8145 0.460677C6.14712 0.460676 0.735887 5.87191 0.735887 12.5393C0.735886 19.2067 6.14712 24.618 12.8145 24.618ZM12.8145 10.1236L17.646 14.9551L7.98308 14.9551L12.8145 10.1236Z" fill="#336065"/></svg>`}
            />
            <Text
              style={{
                color: "#336065",
                fontSize: 12,
                lineHeight: 20,
                textAlign: "center",
              }}
            >
              {props.mejorar}
            </Text>
          </Pressable>
          <Pressable
            onPress={async () => {
              setBuy(true);
              await props.onPressExtra();
              setBuy(false);
            }}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                backgroundColor: "#FFBC00",
                borderRadius: 20,
                paddingVertical: 8,
                paddingHorizontal: 20,
                alignItems: "center",
                marginLeft: 5,
                justifyContent: "center",
              },
            ]}
          >
            <IconSvg
              width="14"
              height="23"
              icon={`<svg width="14" height="23" viewBox="0 0 14 23" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.41699 10.0028C4.67512 9.29019 3.79337 8.55339 3.79337 7.40592C3.79337 6.08935 5.01332 5.17137 7.05463 5.17137C9.20464 5.17137 10.0018 6.19805 10.0743 7.70788H12.7437C12.6591 5.63036 11.3909 3.72193 8.86643 3.10592V0.460693H5.24282V3.06968C2.89955 3.57699 1.01527 5.0989 1.01527 7.43008C1.01527 10.2202 3.3223 11.6093 6.69226 12.4186C9.71194 13.1433 10.3159 14.2062 10.3159 15.3295C10.3159 16.1629 9.72402 17.4916 7.05463 17.4916C4.56641 17.4916 3.58803 16.3804 3.45517 14.9551H0.797852C0.942796 17.6003 2.92371 19.086 5.24282 19.5812V22.2023H8.86643V19.6054C11.2218 19.1584 13.094 17.7936 13.094 15.3174C13.094 11.8871 10.1589 10.7155 7.41699 10.0028Z" fill="#336065"/></svg>`}
            />
            <Text
              style={{
                color: "#336065",
                fontSize: 12,
                lineHeight: 20,
                textAlign: "center",
              }}
            >
              {buy ? `Comprando...` : `Comprar uno extra`}
            </Text>
          </Pressable>
        </View>
      </View>
      <Image
        style={[
          styles.cardImage,
          {
            zIndex: -2,
            width: Dimensions.get("window").width - 40,
            height: Dimensions.get("window").width / 2 - 40,
          },
        ]}
        source={{
          uri: url,
        }}
      />

      <Text style={[styles.cardTitle, { color: "#565656" }]}>{name}</Text>
    </View>
  );
}

export const ServiceCard = (props) => {
  const [buy, setBuy] = useState(false);
  return props.count > 0 ? (
    <CompleteCard {...props} />
  ) : props.avaible && props.count == 0 ? (
    <ExtraCard {...props} buy={buy} setBuy={(val) => setBuy(val)} />
  ) : (
    !props.count &&
    !props.avaible && <UpCard {...props} buy={buy} setBuy={(val) => setBuy(val)} />
  );
};

export default ServiceCard;

import { Dimensions, StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  cardContainer: {
    width: Dimensions.get("window").width / 2 - 40,
    borderRadius: 5,
    marginBottom: 30,
  },
  cardImage: {
    width: Dimensions.get("window").width / 2 - 40,
    height: Dimensions.get("window").width / 2 - 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 10,
  },
  cardTitle: {
    color: "#309da3",
    fontSize: 17,
    fontStyle: "normal",
    fontWeight: "normal",
    textAlign: "center",
    paddingHorizontal: 10,
  },
  circle: {
    alignItems: "center",
    backgroundColor: "#FB883F",
    borderRadius: 30,
    height: 35,
    justifyContent: "center",
    position: "absolute",
    right: -12,
    top: -12,
    width: 35,
    zIndex: 3,
  },
  circleTxt: { color: "#fff", fontSize: 18 },
  noServices: {
    color: "#336065",
    fontSize: 18,
    lineHeight: 20,
    textAlign: "center",
    textTransform: "uppercase",
    marginBottom: 20,
  },
});

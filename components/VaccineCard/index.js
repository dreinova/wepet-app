import React from "react";
import styles from "./styles";
import { View, Text, Pressable, Modal, Image } from "react-native";
import IconSvg from "../IconSvg";
import moment from "moment";
import MainInput from "../MainInput";
import DatePicker from "../DatePicker";
import MainBtn from "../MainBtn";

export const VaccineCard = ({
  id,
  name,
  date_application,
  due_date,
  lote,
  veterinarian,
  vaccine,
  setReminders,
  activePet,
  navigation,
  bath,
}) => {
  const [activeReminderModal, setActiveReminderModal] = React.useState(false);
  const [sended, setSended] = React.useState(false);
  const [sending, setSending] = React.useState(false);
  const [formValues, setFormValues] = React.useState({
    message: name,
    type: vaccine
      ? "vacuna"
      : !vaccine && !bath
      ? "desparacitacion"
      : bath
      ? "bano"
      : "",
    fecha: "",
  });
  const send = async () => {
    setSending(false);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let fechaFormat = moment(new Date()).format("YYYY-MM-DD");
    var raw = JSON.stringify({
      fecha: formValues.fecha != "" ? formValues.fecha : fechaFormat,
      pet: activePet?.id,
      wepetuser: activePet?.wepetuser,
      message: formValues.message,
      type: formValues.type,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://agile-sands-59528.herokuapp.com/reminders/", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        setSending(false);
        setSended(true);
      })
      .catch((error) => console.log("error", error));
  };
  const edit = async (id) => {
    console.log("EDIT!");
    setSending(false);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let fechaFormat = moment(new Date()).format("YYYY-MM-DD");
    var raw = JSON.stringify({
      fecha: formValues.fecha != "" ? formValues.fecha : fechaFormat,
    });

    var requestOptions = {
      method: "PUT",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      "https://agile-sands-59528.herokuapp.com/reminders/" + id,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        setReminders([result]);
        setSending(false);
        setSended(true);
      })
      .catch((error) => console.log("error", error));
  };
  return (
    <View style={styles.cardContainer}>
      <Modal transparent animationType="fade" visible={activeReminderModal}>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "rgba(0,0,0,.8)",
          }}
        >
          <View
            style={{
              paddingVertical: 50,
              paddingHorizontal: 20,
              backgroundColor: "#fff",
              borderRadius: 8,
            }}
          >
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Image
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 50,
                  overflow: "hidden",
                  borderColor: "#FB883F",
                  borderWidth: 5,
                  marginBottom: 15,
                }}
                source={{
                  uri:
                    activePet?.image?.length > 0
                      ? activePet?.image[0].url
                      : "https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg",
                }}
              />
            </View>
            {sended ? (
              <View>
                <Text
                  style={{
                    color: "#336065",
                    fontSize: 22,
                    marginBottom: 10,
                    textAlign: "center",
                    fontFamily: "Baloo_Paaji_2_Bold",
                  }}
                >
                  ¡Recordatorio creado!
                </Text>
                <Text
                  style={{
                    color: "#336065",
                    fontSize: 14,
                    marginBottom: 10,
                    textAlign: "center",
                    fontFamily: "Baloo_Paaji_2_Regular",
                  }}
                >
                  Te estaremos avisando por correo
                </Text>

                <MainBtn
                  text="Cerrar"
                  secondary
                  onPress={() => {
                    setActiveReminderModal(false);
                    setSended(false);
                  }}
                />
              </View>
            ) : (
              <View>
                <Text
                  style={{
                    color: "#336065",
                    fontSize: 16,
                    textAlign: "center",
                    fontFamily: "Baloo_Paaji_2_Bold",
                    marginBottom: 30,
                  }}
                >
                  Crear recordatorio para {name}
                </Text>
                <DatePicker
                  label="Elige una fecha para el recordatorio"
                  setDateValue={(date) => {
                    setFormValues((prevState) => {
                      return {
                        ...prevState,
                        fecha: date,
                      };
                    });
                  }}
                />
                <MainBtn
                  text={sending ? "Creando..." : "Crear recordatorio"}
                  primary
                  onPress={async () => {
                    if ((vaccine && !bath) || (!vaccine && !bath)) {
                      await send();
                    } else if (bath) {
                      console.log(id);
                      await edit(id);
                    }
                  }}
                />
                <MainBtn
                  text="Cancelar"
                  secondary
                  onPress={() => {
                    setActiveReminderModal(false);
                  }}
                />
              </View>
            )}
          </View>
        </View>
      </Modal>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Pressable
          onPress={() => {
            vaccine
              ? navigation.navigate("EditVaccine", { id })
              : navigation.navigate("EditDeworming", { id });
          }}
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            { flexDirection: "row", alignItems: "center" },
          ]}
        >
          <View style={{ marginBottom: 8 }}>
            <Text style={styles.nameVaccine}>{name}</Text>
            {!bath && (
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#336065",
                    textDecorationLine: "underline",
                    marginRight: 10,
                  }}
                >
                  Editar
                </Text>
                <IconSvg
                  width="15"
                  height="15"
                  icon={`<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.3431 5.47114L20.7776 0.882253C20.4759 0.582081 20.0676 0.413574 19.642 0.413574C19.2164 0.413574 18.8082 0.582081 18.5065 0.882253L2.32091 17.0445L0.843133 23.4223C0.792155 23.6554 0.793903 23.897 0.848248 24.1294C0.902594 24.3617 1.00817 24.5791 1.15725 24.7654C1.30633 24.9518 1.49516 25.1025 1.70995 25.2065C1.92473 25.3105 2.16004 25.3653 2.39869 25.3667C2.50989 25.3779 2.62194 25.3779 2.73313 25.3667L9.18091 23.8889L25.3431 7.74225C25.6433 7.44056 25.8118 7.03228 25.8118 6.6067C25.8118 6.18111 25.6433 5.77284 25.3431 5.47114ZM8.40313 22.4889L2.3598 23.7567L3.73647 17.83L15.8465 5.7667L20.5131 10.4334L8.40313 22.4889ZM21.5554 9.30559L16.8887 4.63892L19.5954 1.94781L24.1842 6.61448L21.5554 9.30559Z" fill="#AEAEAE"/></svg>`}
                />
              </View>
            )}
          </View>
        </Pressable>
        <Pressable
          onPress={() => {
            setActiveReminderModal(true);
          }}
        >
          <IconSvg
            style={{ marginLeft: 10 }}
            width="25"
            height="25"
            icon={`<svg width="37" height="33" viewBox="0 0 37 33" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.55978 3.02875L6.27963 0.72125C0.951172 6.03988 0.951172 9.89437 0.951172 14.875H2.56831L4.18544 14.7726C4.18544 10.0146 4.18544 7.3935 8.55978 3.02875ZM31.198 0.72125L28.9211 3.02875C33.2938 7.3935 33.2938 10.0146 33.2938 14.875L36.5281 14.7726C36.5281 9.89437 36.5281 6.03988 31.198 0.72125ZM18.7396 32.75C19.7411 32.7513 20.7181 32.439 21.5348 31.8565C22.3515 31.2741 22.9674 30.4504 23.2967 29.5H14.1826C14.5119 30.4504 15.1277 31.2741 15.9444 31.8565C16.7611 32.439 17.7381 32.7513 18.7396 32.75ZM30.0596 20.7023V13.25C30.0596 8.02238 26.5261 3.61862 21.7394 2.29425C21.2656 1.095 20.1077 0.25 18.7396 0.25C17.3715 0.25 16.2137 1.095 15.7399 2.29425C10.9515 3.61862 7.41971 8.02238 7.41971 13.25V20.7023L4.65926 23.4761C4.5088 23.6268 4.38947 23.8058 4.30815 24.003C4.22684 24.2002 4.18513 24.4116 4.18544 24.625V26.25C4.18544 26.681 4.35581 27.0943 4.65909 27.399C4.96236 27.7038 5.37368 27.875 5.80257 27.875H31.6767C32.1056 27.875 32.5169 27.7038 32.8202 27.399C33.1235 27.0943 33.2938 26.681 33.2938 26.25V24.625C33.2941 24.4116 33.2524 24.2002 33.1711 24.003C33.0898 23.8058 32.9705 23.6268 32.82 23.4761L30.0596 20.7023Z" fill="#AEAEAE"/></svg>`}
          />
        </Pressable>
      </View>
      {date_application && (
        <View style={styles.limiter}>
          <Text style={styles.tableHeaderText}>
            {bath ? "Fecha programada" : "Fecha de aplicación"}
          </Text>
          <Text style={styles.tableBodyText}>{date_application}</Text>
        </View>
      )}
      {due_date && (
        <View style={styles.limiter}>
          <Text style={styles.tableHeaderText}>Fecha de vencimiento</Text>
          <Text style={styles.tableBodyText}>{due_date}</Text>
        </View>
      )}
      {lote && (
        <View style={styles.limiter}>
          <Text style={styles.tableHeaderText}>Lote</Text>
          <Text style={styles.tableBodyText}>{lote}</Text>
        </View>
      )}
      {veterinarian && (
        <View style={styles.limiter}>
          <Text style={styles.tableHeaderText}>Veterinario</Text>
          <Text style={styles.tableBodyText}>{veterinarian}</Text>
        </View>
      )}
    </View>
  );
};

export default VaccineCard;

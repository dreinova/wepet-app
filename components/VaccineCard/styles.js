import { Dimensions, StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  cardContainer: {
    padding: 15,
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    elevation: 5,
    marginHorizontal: 5,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    width: Dimensions.get("window").width - 50,
    marginBottom: 20,
    height: 290,
  },
  nameVaccine: {
    fontSize: 22,
    letterSpacing: -0.02,
    color: "#336065",
    fontFamily: "Baloo_Paaji_2_Bold",
  },
  tableHeaderText: {
    fontSize: 16,
    letterSpacing: -0.02,
    color: "#565656",
    fontFamily: "OpenSans_Bold",
  },
  tableBodyText: {
    fontSize: 16,
    letterSpacing: -0.02,
    color: "#565656",
    fontFamily: "OpenSans_Regular",
  },
  limiter: {
    marginBottom: 10,
  },
});

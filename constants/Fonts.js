export default {
  display: {
    color: "#336065",
    fontFamily: "Baloo_Paaji_2_Bold",
    fontSize: 18,
    lineHeight: 25,
    textAlign: "center",
    marginBottom: 15,
  },
  texto_regular: {
    color: "#565656",
    fontFamily: "OpenSans_Regular",
    fontSize: 13,
    lineHeight: 18,
  },
};

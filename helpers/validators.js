export const validateInput = (value, type) => {
  switch (type) {
    case "email":
      var re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      return re.test(value);
    case "numbers":
      var re = /^[0-9]*$/;
      return re.test(value);
    case "required":
      return value === "" ? false : true;
  }
};

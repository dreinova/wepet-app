import CreatePet from "../screens/CreatePet";
import DashboardScreen from "../screens/DashboardScreen";
import LoginScreen from "../screens/LoginScreen";
import PayScreen from "../screens/PayScreen";
import PetProfile from "../screens/PetProfile";
import PlansScreen from "../screens/PlansScreen";
import moment from "moment";
import ProviderScreen from "../screens/ProviderScreen";
import React, { useEffect, useState } from "react";
import RecoverPassword from "../screens/RecoverPassword";
import RegisterScreen from "../screens/RegisterScreen";
import ServiceScreen from "../screens/ServiceScreen";
import UserProfile from "../screens/UserProfile";
import { NavigationContainer } from "@react-navigation/native";
import { Entypo } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from "@react-navigation/drawer";
import { getHeaderTitle } from "@react-navigation/elements";
import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Modal,
  TouchableWithoutFeedback,
  Platform,
  Linking,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import IconSvg from "../components/IconSvg";
import PetAvatar from "../components/PetAvatar";
import MainBtn from "../components/MainBtn";
import { connect } from "react-redux";
import { deletePet, setActivePet } from "../store/actions/pets";
import ActionSheets from "../components/ActionSheets";
import { userLogout } from "../store/actions/auth";
import WizzardScreen from "../screens/WizzardScreen";
import AddVaccine from "../screens/AddVaccine";
import AddDeworming from "../screens/AddDeworming";
import { createStackNavigator } from "@react-navigation/stack";
import Display from "../components/FontsComponents/Display";
import { useToast } from "react-native-toast-notifications";
import WhatScreen from "../screens/WhatScreen";
import { setLang } from "../store/actions/ui";
import CuponesScreen from "../screens/CuponesScreen";
import EditDeworming from "../screens/EditDeworming";
import EditVaccine from "../screens/EditVaccine";
import MainInput from "../components/MainInput";
import BlogScreen from "../screens/BlogScreen";
import Regular from "../components/FontsComponents/Regular";
import { fetchSinToken } from "../helpers/fetch";

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function Navigation(props) {
  return (
    <NavigationContainer>
      <RootNavigator {...props} />
    </NavigationContainer>
  );
}
function MyHeader({
  title,
  style,
  navigation,
  user,
  pets,
  activePet,
  setActivePet,
  userLogout,
  deletePet,
  words,
  lang,
  setLang,
  plans,
}) {
  const actionItems = [
    {
      id: 1,
      label: "Action Item 1",
      onPress: () => {},
    },
    {
      id: 2,
      label: "Action Item 2",
      onPress: () => {},
    },
    {
      id: 3,
      label: "Action Item 3",
      onPress: () => {},
    },
    {
      id: 4,
      label: "Action Item 4",
      onPress: () => {},
    },
  ];
  Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
      var k = new_index - this.length;
      while (k-- + 1) {
        this.push(undefined);
      }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this;
  };
  const [actionSheet, setActionSheet] = useState(false);
  const closeActionSheet = () => setActionSheet(false);
  const [cancelSub, setCancelSub] = useState(false);
  const closeCancelSub = () => setCancelSub(false);
  const [active, setActive] = useState(false);
  const [deleteSub, setDeleteSub] = useState(false);
  const [toDelete, setToDelete] = useState({});
  const [codegeneratedModal, setCodegeneratedModal] = useState(false);
  const closeCodeGenerated = () => setCodegeneratedModal(false);

  const toast = useToast();
  const petsOrder = pets;
  if (activePet) {
    pets?.map((item, index) => {
      if (item.id === activePet.id) return pets.move(index, 0);
    });
  }
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);

  const createCodeNoCancel = async () => {
    setLoading(true);
    var formData = new FormData();
    formData.append("email", user?.email);
    const url = `https://wepet.co/mi-cuenta/s/backCodes/`;
    const method = "POST";
    await fetch(url, {
      method,
      body: formData,
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
  };
  return (
    <View>
      <LinearGradient colors={["#309da3", "#3cfbfe"]} style={[style]}>
        <Pressable
          onPress={() => navigation.toggleDrawer()}
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            {
              marginLeft: 20,
              opacity: user ? 1 : 0,
              width: 50,
              height: 30,
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        >
          <IconSvg
            width="22"
            height="16"
            icon={`<svg width="18" height="12" viewBox="0 0 18 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0 12H18V10H0V12ZM0 7H18V5H0V7ZM0 0V2H18V0H0Z" fill="white" fill-opacity="0.87"/></svg>`}
          />
        </Pressable>
        <Pressable
          style={styles.containerLogo}
          onPress={() => {
            user && navigation.navigate("DashboardScreen");
          }}
        >
          <Image
            style={styles.tinyLogo}
            source={require("../assets/logo.png")}
          />
        </Pressable>
        {user && activePet && (
          <Pressable
            onPress={() => setActionSheet(true)}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                alignItems: "center",
                justifyContent: "center",
                paddingHorizontal: 20,
                paddingVertical: 10,
              },
              active && { backgroundColor: "#FB883F" },
            ]}
          >
            {activePet && (
              <Image
                style={styles.profileImage}
                source={{
                  uri:
                    activePet?.image?.length > 0
                      ? activePet?.image[0].url
                      : "https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg",
                }}
              />
            )}
            <Text style={styles.namePet}>{activePet?.name}</Text>
          </Pressable>
        )}
        {activePet && (
          <Modal
            transparent
            visible={actionSheet}
            onRequestClose={() => closeCancelSub(false)}
          >
            <TouchableWithoutFeedback onPress={() => closeActionSheet()}>
              <View
                style={{
                  flex: 1,
                  margin: 0,
                  justifyContent: "flex-end",
                  backgroundColor: "rgba(0,0,0,.5)",
                }}
              >
                <View
                  style={{
                    backgroundColor: "#FFF",
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                    overflow: "hidden",
                  }}
                >
                  <View style={{ paddingHorizontal: 20 }}>
                    {petsOrder?.map((item, index) => {
                      if (item.status || typeof item.status === "undefined") {
                        return (
                          <PetAvatar
                            closeActionSheet={() => closeActionSheet()}
                            pets={pets}
                            setCancelSub={(val) => setCancelSub(val)}
                            key={index}
                            pet={item}
                            isActive={activePet.id}
                            selccionar={`${
                              words[localLang].length > 0 && findWords("329")
                            }`}
                            editar={`${
                              words[localLang].length > 0 && findWords("330")
                            }`}
                            cancelar={`${
                              words[localLang].length > 0 && findWords("31")
                            }`}
                            onPress={() => {
                              setActivePet(item);
                              closeActionSheet();
                              navigation.navigate("DashboardScreen");
                            }}
                            navigation={navigation}
                            setActivePet={() => setActivePet(item)}
                            setToDelete={() => setToDelete(item)}
                          />
                        );
                      }
                    })}
                  </View>
                  <MainBtn
                    text="Agregar mascota"
                    primary
                    onPress={() => {
                      navigation.navigate("CreatePet", {
                        planID: 8,
                        planType: "Mensual",
                        idSub: 0,
                        userID: user.id,
                        user: user,
                      });
                      closeActionSheet();
                    }}
                    style={{
                      marginBottom: 0,
                      borderRadius: 0,
                      paddingVertical: 15,
                    }}
                    styleText={{ fontSize: 18 }}
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        )}
        <Modal
          transparent
          visible={cancelSub}
          onRequestClose={() => closeCancelSub()}
        >
          <TouchableWithoutFeedback onPress={() => closeCancelSub()}>
            <View
              style={{
                flex: 1,
                margin: 0,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "rgba(0,0,0,.5)",
              }}
            >
              <View
                style={{
                  backgroundColor: "#FFF",
                  borderRadius: 8,
                  padding: 40,
                }}
              >
                <Display>
                  {`${
                    words[localLang].length > 0 &&
                    findWords("345").replace(
                      "_",
                      user && toDelete && toDelete.name
                    )
                  }`}
                </Display>
                <Regular style={{ marginBottom: 30 }}>
                  Sigue disfrutando de tu membresía y obtén un código de
                  descuento del 10% en nuestro Marketplace
                </Regular>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <MainBtn
                    tertiary
                    text={`${words[localLang].length > 0 && findWords("32")}`}
                    style={{ marginBottom: 0 }}
                    styleText={{ fontSize: 14, textTransform: "none" }}
                    onPress={() => {
                      closeCancelSub();
                      setCodegeneratedModal(true);
                    }}
                  />
                  <MainBtn
                    text={
                      deleteSub
                        ? `${words[localLang].length > 0 && findWords("349")}`
                        : `${words[localLang].length > 0 && findWords("347")}`
                    }
                    primary
                    style={{ marginBottom: 0, backgroundColor: "#309da3" }}
                    onPress={async () => {
                      setDeleteSub(true);
                      const a = await deletePet(
                        toDelete.id,
                        toDelete.id_sub,
                        user?.id,
                        user?.email,
                        toDelete.name,
                        moment(toDelete.created_at).format("YYYY-MM-DD"),
                        plans.find((plan) => plan.id == toDelete?.plan)?.name
                      );
                      setDeleteSub(false);
                      toast.show(
                        `${words[localLang].length > 0 && findWords("351")}`,
                        {
                          type: "danger ",
                          placement: "bottom",
                          duration: 2000,
                          animationType: "zoom-in",
                        }
                      );
                      closeCancelSub();
                    }}
                    styleText={{ fontSize: 14, textTransform: "none" }}
                  />
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
        <Modal
          transparent
          visible={codegeneratedModal}
          onRequestClose={() => closeCodeGenerated()}
        >
          <TouchableWithoutFeedback onPress={() => closeCodeGenerated()}>
            <View
              style={{
                flex: 1,
                margin: 0,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "rgba(0,0,0,.5)",
              }}
            >
              <View
                style={{
                  backgroundColor: "#FFF",
                  borderRadius: 8,
                  padding: 40,
                }}
              >
                <Display>¡Has tomado una buena decisión!</Display>
                <Regular style={{ marginBottom: 30 }}>
                  Tu código de descuento llegará en breve
                </Regular>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <MainBtn
                    tertiary
                    text="Cerrar"
                    style={{ marginBottom: 0 }}
                    styleText={{ fontSize: 14, textTransform: "none" }}
                    onPress={async () => {
                      closeCodeGenerated();
                      let fields = {
                        cancelcode: true,
                      };
                      await fetchSinToken(`pets/${activePet.id}`, fields, "PUT")
                        .then((res) => res.json())
                        .then(async (data) => {
                          console.log("Codigo enviado", data);
                          await createCodeNoCancel();
                        })
                        .catch((err) => console.error(err));
                    }}
                  />
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </LinearGradient>
      {active && (
        <View
          style={{
            backgroundColor: "#f00",
            width: "100%",
            alignItems: "flex-end",
          }}
        >
          <View
            style={{
              top: -40,
              position: "absolute",
              zIndex: 999,
              backgroundColor: "#FFF",
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              elevation: 5,
              overflow: "hidden",
              shadowColor: "#000",
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              width: 220,
              shadowOffset: {
                width: 0,
                height: 2,
              },
            }}
          >
            <MainBtn
              tertiary
              text="Editar mascota"
              onPress={() => {
                navigation.navigate("PetProfile");
              }}
            />
            <Pressable
              onPress={() => {
                Linking.openURL(
                  "https://wepet.co/mi-cuenta/s/newPetApp/?userID=" + user?.id
                );
              }}
              style={({ pressed }) => [
                {
                  opacity: pressed ? 0.5 : 1,
                },
                {
                  flexDirection: "row",
                  alignItems: "center",
                  marginVertical: 10,
                },
              ]}
            >
              <Text>
                {" "}
                {`${words[localLang].length > 0 && findWords("66")}`}
              </Text>
            </Pressable>
            <View style={{ paddingHorizontal: 20 }}>
              {user != null &&
                pets?.map((item, index) => (
                  <PetAvatar
                    key={index}
                    pet={item}
                    isActive={activePet.id}
                    onPress={() => {
                      setActivePet(item);
                      setActive(false);
                    }}
                    closeActionSheet={() => closeActionSheet()}
                    pets={pets}
                  />
                ))}
            </View>
          </View>
        </View>
      )}
    </View>
  );
}
function CustomDrawerContent({
  progress,
  user,
  userLogout,
  deletePet,
  setLang,
  words,
  lang,
  ...rest
}) {
  const toast = useToast();
  const [language, setLanguage] = useState(false);
  const [localLang, setLocalLang] = useState(lang);
  const [modalOpen, setModalOpen] = useState(false);
  const [modalOpenHelp, setModalOpenHelp] = useState(false);
  const [loading, setLoading] = useState(false);
  const [guestEmail, setGuestEmail] = useState(false);

  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);
  const sendInvitation = async () => {
    setLoading(true);
    var formData = new FormData();
    formData.append("guestEmail", guestEmail);
    formData.append("user", user.id);
    formData.append("userName", user.name);
    const url = `https://wepet.co/mi-cuenta/s/createInvitation/`;
    const method = "POST";
    await fetch(url, {
      method,
      body: formData,
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        toast.show("Invitación enviada!", {
          type: "success",
          placement: "bottom",
          duration: 2000,
          animationType: "zoom-in",
        });
        setModalOpen(false);
        setLoading(false);
        setGuestEmail("");
      });
  };
  return (
    <View style={{ flex: 1 }}>
      <Modal
        transparent
        visible={modalOpen}
        onRequestClose={() => {
          setModalOpen(false);
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            setModalOpen(false);
          }}
        >
          <View
            style={{
              flex: 1,
              margin: 0,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(0,0,0,.5)",
            }}
          >
            <View
              style={{
                backgroundColor: "#FFF",
                borderRadius: 8,
                padding: 40,
              }}
            >
              <Display>
                ¡No te pierdas la oportunidad de obtener un código del 10% de
                descuento por cada amigo que se registre con tu invitación!
              </Display>
              <MainInput
                label="Correo del invitado:"
                returnKeyType="next"
                keyboardType="default"
                onChangeText={(text) => {
                  setGuestEmail(text);
                }}
              />
              <MainBtn
                primary
                text={loading ? "Enviando..." : "Invitar"}
                onPress={async () => await sendInvitation()}
                disabled={loading}
              />
              <MainBtn
                tertiary
                text="Cerrar"
                onPress={() => setModalOpen(false)}
                disabled={loading}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
      <Modal
        transparent
        visible={modalOpenHelp}
        onRequestClose={() => {
          setModalOpenHelp(false);
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            setModalOpenHelp(false);
          }}
        >
          <View
            style={{
              flex: 1,
              margin: 0,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(0,0,0,.5)",
            }}
          >
            <View
              style={{
                backgroundColor: "#FFF",
                borderRadius: 8,
                padding: 40,
              }}
            >
              <Pressable
                onPress={() => {
                  rest.navigation.navigate("WizzardScreen");
                  setModalOpenHelp(false);
                }}
                style={({ pressed }) => [
                  {
                    opacity: pressed ? 0.5 : 1,
                    alignItems: "center",
                    padding: 8,
                  },
                ]}
              >
                <Text
                  style={{
                    fontSize: 12,
                    letterSpacing: -0.02,
                    color: "#565656",
                    fontFamily: "OpenSans_Bold",
                    textTransform: "uppercase",
                  }}
                >
                  Guia de uso
                </Text>
              </Pressable>
              <View
                style={{
                  width: 250,
                  height: 1,
                  backgroundColor: "#ccc",
                  marginVertical: 15,
                }}
              />
              <Pressable
                onPress={() => {
                  Linking.openURL(`https://forms.gle/tadJys4FD8YzCkR67`);
                  setModalOpenHelp(false);
                }}
                style={({ pressed }) => [
                  {
                    opacity: pressed ? 0.5 : 1,
                    alignItems: "center",

                    padding: 8,
                  },
                ]}
              >
                <Text
                  style={{
                    fontSize: 12,
                    letterSpacing: -0.02,
                    color: "#565656",
                    fontFamily: "OpenSans_Bold",
                    textTransform: "uppercase",
                  }}
                >
                  Reporta un problema con la app
                </Text>
              </Pressable>
              <View
                style={{
                  width: 250,
                  height: 1,
                  backgroundColor: "#ccc",
                  marginVertical: 15,
                }}
              />
              <Pressable
                onPress={() => {
                  Linking.openURL(`https://wa.me/573233072418`);
                  setModalOpenHelp(false);
                }}
                style={({ pressed }) => [
                  {
                    opacity: pressed ? 0.5 : 1,
                    alignItems: "center",

                    padding: 8,
                  },
                ]}
              >
                <Text
                  style={{
                    fontSize: 12,
                    letterSpacing: -0.02,
                    color: "#565656",
                    fontFamily: "OpenSans_Bold",
                    textTransform: "uppercase",
                  }}
                >
                  Chatea con nosotros
                </Text>
              </Pressable>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
      <DrawerContentScrollView
        {...rest}
        style={{ paddingVertical: 20, paddingBottom: 0 }}
      >
        <View style={{ paddingHorizontal: 30 }}>
          <Pressable
            onPress={() => {
              rest.navigation.toggleDrawer();
            }}
          >
            <Entypo
              name="cross"
              size={40}
              color="black"
              style={{ marginBottom: 50 }}
            />
          </Pressable>
          {user && (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text style={styles.welcomeText}>¡Hola!</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Pressable
                  onPress={() => rest.navigation.navigate("UserProfile")}
                  style={({ pressed }) => [
                    {
                      opacity: pressed ? 0.5 : 1,
                    },
                    {
                      alignItems: "center",
                      justifyContent: "center",
                      flexDirection: "row",
                      borderWidth: 1,
                      borderColor: "#336065",
                      paddingVertical: 5,
                      paddingHorizontal: 20,
                      borderRadius: 30,
                    },
                  ]}
                >
                  <Text style={styles.userName}>
                    {user.name} {user.lastname}
                  </Text>
                  <IconSvg
                    width="15"
                    height="15"
                    icon={`<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.3431 5.47114L20.7776 0.882253C20.4759 0.582081 20.0676 0.413574 19.642 0.413574C19.2164 0.413574 18.8082 0.582081 18.5065 0.882253L2.32091 17.0445L0.843133 23.4223C0.792155 23.6554 0.793903 23.897 0.848248 24.1294C0.902594 24.3617 1.00817 24.5791 1.15725 24.7654C1.30633 24.9518 1.49516 25.1025 1.70995 25.2065C1.92473 25.3105 2.16004 25.3653 2.39869 25.3667C2.50989 25.3779 2.62194 25.3779 2.73313 25.3667L9.18091 23.8889L25.3431 7.74225C25.6433 7.44056 25.8118 7.03228 25.8118 6.6067C25.8118 6.18111 25.6433 5.77284 25.3431 5.47114ZM8.40313 22.4889L2.3598 23.7567L3.73647 17.83L15.8465 5.7667L20.5131 10.4334L8.40313 22.4889ZM21.5554 9.30559L16.8887 4.63892L19.5954 1.94781L24.1842 6.61448L21.5554 9.30559Z" fill="#AEAEAE"/></svg>`}
                  />
                </Pressable>
              </View>
            </View>
          )}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              paddingVertical: 10,
            }}
          >
            <Pressable
              onPress={() => {
                setModalOpen(true);
              }}
              style={({ pressed }) => [
                {
                  opacity: pressed ? 0.5 : 1,
                  alignItems: "center",

                  padding: 8,
                },
              ]}
            >
              <Text
                style={{
                  fontSize: 11,
                  letterSpacing: -0.02,
                  textDecorationLine: "underline",
                  color: "#ff9700",
                  fontFamily: "OpenSans_Bold",
                }}
              >
                Invita a un amigo y obtén beneficios
              </Text>
            </Pressable>
          </View>

          <Pressable
            onPress={() => {
              rest.navigation.navigate("DashboardScreen");
            }}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
                alignItems: "center",

                padding: 8,
              },
            ]}
          >
            <Text
              style={{
                fontSize: 12,
                letterSpacing: -0.02,
                color: "#565656",
                fontFamily: "OpenSans_Bold",
              }}
            >
              INICIO
            </Text>
          </Pressable>
          <View
            style={{
              width: "100%",
              height: 1,
              backgroundColor: "#ccc",
              marginVertical: 15,
            }}
          />
          <Pressable
            onPress={() => {
              Linking.openURL(`https://wepet.co/blog/`);
            }}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
                alignItems: "center",

                padding: 8,
              },
            ]}
          >
            <Text
              style={{
                fontSize: 12,
                letterSpacing: -0.02,
                color: "#565656",
                fontFamily: "OpenSans_Bold",
              }}
            >
              BLOG
            </Text>
          </Pressable>
          <View
            style={{
              width: "100%",
              height: 1,
              backgroundColor: "#ccc",
              marginVertical: 15,
            }}
          />
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
                alignItems: "center",

                padding: 8,
              },
            ]}
            onPress={() => {
              Linking.openURL(`https://wepet.co/tienda/`);
            }}
          >
            <Text
              style={{
                fontSize: 12,
                letterSpacing: -0.02,
                color: "#565656",
                fontFamily: "OpenSans_Bold",
              }}
            >
              TIENDA
            </Text>
          </Pressable>
          <View
            style={{
              width: "100%",
              height: 1,
              backgroundColor: "#ccc",
              marginVertical: 15,
            }}
          />
          <Pressable
            onPress={() => {
              rest.navigation.navigate("CuponesScreen");
            }}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
                alignItems: "center",

                padding: 8,
              },
            ]}
          >
            <Text
              style={{
                fontSize: 12,
                letterSpacing: -0.02,
                color: "#565656",
                fontFamily: "OpenSans_Bold",
              }}
            >
              CÓDIGOS DE DESCUENTO
            </Text>
          </Pressable>
          <View
            style={{
              width: "100%",
              height: 1,
              backgroundColor: "#ccc",
              marginVertical: 15,
            }}
          />
          <Pressable
            style={{
              alignItems: "center",

              padding: 8,
              borderRadius: 25,
            }}
            onPress={() => {
              Linking.openURL(`https://wepet.co/terminos-y-condiciones/`);
            }}
          >
            <Text
              style={{
                fontSize: 12,
                letterSpacing: -0.02,
                color: "#565656",
                fontFamily: "OpenSans_Bold",
              }}
            >
              TÉRMINOS Y CONDICIONES
            </Text>
          </Pressable>
          <View
            style={{
              width: "100%",
              height: 1,
              backgroundColor: "#ccc",
              marginVertical: 15,
            }}
          />
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
                alignItems: "center",

                padding: 8,
              },
            ]}
            onPress={() => {
              setModalOpenHelp(true);
            }}
          >
            <Text
              style={{
                fontSize: 12,
                letterSpacing: -0.02,
                color: "#565656",
                fontFamily: "OpenSans_Bold",
              }}
            >
              AYUDA
            </Text>
          </Pressable>
        </View>
      </DrawerContentScrollView>

      {user && (
        <Pressable
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            { padding: 8, backgroundColor: "#336065" },
          ]}
          onPress={async () => {
            const user = await AsyncStorage.getItem("user");
            if (user) {
              await AsyncStorage.removeItem("user");
            }
            await userLogout();
            rest.navigation.navigate("Login");
          }}
        >
          <Text
            style={{
              color: "#FFF",
              fontFamily: "Baloo_Paaji_2_Bold",
              fontSize: 18,
              padding: 8,
              alignItems: "center",
              textAlign: "center",
            }}
          >
            Cerrar Sesión
          </Text>
        </Pressable>
      )}
    </View>
  );
}
function StackNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        options={{ drawerLabel: "Login" }}
        name="Login"
        component={LoginScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Planes" }}
        name="PlansScreen"
        component={PlansScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Recuperar Contraseña" }}
        name="RecoverPassword"
        component={RecoverPassword}
      />
      <Stack.Screen
        options={{ drawerLabel: "Registro" }}
        name="RegisterScreen"
        component={RegisterScreen}
      />
    </Stack.Navigator>
  );
}
function StackNavigatorSession() {
  return (
    <Stack.Navigator
      initialRouteName="Registro"
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}
    >
      <Stack.Screen
        options={{ drawerLabel: "Login" }}
        name="Login"
        component={LoginScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Recuperar Contraseña" }}
        name="RecoverPassword"
        component={RecoverPassword}
      />
      <Stack.Screen
        options={{ drawerLabel: "Registro" }}
        name="RegisterScreen"
        component={RegisterScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Planes" }}
        name="PlansScreen"
        component={PlansScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "WizzardScreen" }}
        name="WizzardScreen"
        component={WizzardScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "WhatScreen" }}
        name="WhatScreen"
        component={WhatScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Dashboard" }}
        name="DashboardScreen"
        component={DashboardScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Cupones" }}
        name="CuponesScreen"
        component={CuponesScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "AddVaccine" }}
        name="AddVaccine"
        component={AddVaccine}
      />
      <Stack.Screen
        options={{ drawerLabel: "AddDeworming" }}
        name="AddDeworming"
        component={AddDeworming}
      />
      <Stack.Screen
        options={{ drawerLabel: "EditVaccine" }}
        name="EditVaccine"
        component={EditVaccine}
      />
      <Stack.Screen
        options={{ drawerLabel: "EditDeworming" }}
        name="EditDeworming"
        component={EditDeworming}
      />
      <Stack.Screen
        options={{ drawerLabel: "Servicio individual" }}
        name="ServiceScreen"
        component={ServiceScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Proveedor individual" }}
        name="ProviderScreen"
        component={ProviderScreen}
      />

      <Stack.Screen
        options={{ drawerLabel: "Crear mascota" }}
        name="CreatePet"
        component={CreatePet}
      />
      <Stack.Screen
        options={{ drawerLabel: "Perfil Mascota" }}
        name="PetProfile"
        component={PetProfile}
      />
      <Stack.Screen
        options={{ drawerLabel: "Perfil Usuario" }}
        name="UserProfile"
        component={UserProfile}
      />
      <Stack.Screen
        options={{ drawerLabel: "Pagos" }}
        name="PayScreen"
        component={PayScreen}
      />
      <Stack.Screen
        options={{ drawerLabel: "Blog" }}
        name="BlogScreen"
        component={BlogScreen}
      />
    </Stack.Navigator>
  );
}
function RootNavigator({
  user,
  pets,
  activePet,
  setActivePet,
  userLogout,
  deletePet,
  setLang,
  words,
  plans,
  lang,
}) {
  return (
    <Drawer.Navigator
      drawerContent={(props) => (
        <CustomDrawerContent
          {...props}
          user={user}
          pets={pets}
          userLogout={userLogout}
          activePet={activePet}
          setLang={setLang}
          words={words}
          lang={lang}
        />
      )}
      initialRouteName="all"
      screenOptions={{
        sceneContainerStyle: { backgroundColor: "#FFF" },
        headerStyle: {
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          minHeight: 90,
          marginBottom: 40,
        },
        header: ({ navigation, route, options }) => {
          const title = getHeaderTitle(options, route.name);

          return (
            <MyHeader
              setActivePet={setActivePet}
              activePet={activePet}
              user={user}
              pets={pets}
              title={title}
              style={options.headerStyle}
              navigation={navigation}
              userLogout={userLogout}
              deletePet={deletePet}
              words={words}
              lang={lang}
              plans={plans}
            />
          );
        },
      }}
    >
      <Drawer.Screen name="all" component={StackNavigatorSession} />
    </Drawer.Navigator>
  );
}

const styles = StyleSheet.create({
  tinyLogo: {
    width: 40,
    resizeMode: "contain",
    height: 40,
  },
  containerLogo: {
    position: "absolute",
    right: Dimensions.get("window").width / 2 - 40 / 2,
    top: Platform.OS == "ios" ? 40 : 30,
  },
  nameContainer: {
    alignItems: "center",
    backgroundColor: "#309da3",
    borderRadius: 20,
    height: 40,
    justifyContent: "center",
    marginLeft: 5,
    width: 40,
  },
  simpleName: {
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: "bold",
  },
  welcomeText: {
    fontSize: 22,
    letterSpacing: -0.02,
    color: "#336065",
    fontFamily: "OpenSans_Regular",
    marginBottom: 8,
  },
  userName: {
    color: "#336065",
    fontFamily: "Baloo_Paaji_2_Bold",
    fontSize: 18,
    marginRight: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  tourText: {
    fontFamily: "OpenSans_Regular",
    fontSize: 14,
    letterSpacing: -0.02,
    color: "#565656",
    alignItems: "center",
    flexDirection: "row",
  },
  profileImage: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    borderColor: "#FB883F",
    borderWidth: 2,
    alignSelf: "center",
    marginBottom: 5,
  },
  namePet: {
    color: "#FFF",
    fontSize: 12,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
  },
  buttonStyle: {
    height: 50,
    backgroundColor: "rgb(0,98,255)",
    width: Dimensions.get("window").width - 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    margin: 10,
  },
  buttonText: {
    fontSize: 17,
    color: "rgb(255,255,255)",
  },
});

const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
    plans: state.PlansReducer.plans,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setActivePet: (pet) => dispatch(setActivePet(pet)),
    userLogout: () => dispatch(userLogout()),
    deletePet: (id, subID, user, email, petname, userdate, planname) =>
      dispatch(deletePet(id, subID, user, email, petname, userdate, planname)),
    setLang: (lang) => dispatch(setLang(lang)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);

import React, { useState } from "react";
import { StyleSheet, Text, View, ScrollView, Pressable } from "react-native";
import { connect } from "react-redux";
import CustomSelect from "../../components/CustomSelect";
import DatePicker from "../../components/DatePicker";
import Display from "../../components/FontsComponents/Display";
import IconSvg from "../../components/IconSvg";
import MainBtn from "../../components/MainBtn";
import MainInput from "../../components/MainInput";
import { updateVaccines } from "../../store/actions/pets";

const AddDeworming = ({
  activePet,
  dewormings,
  navigation,
  updateVaccines,
}) => {
  const [loading, setLoading] = useState(false);
  const [formValues, setFormValues] = useState({
    date_application: "",
    lote: "",
    next_application: "",
    dewormer: "",
  });
  const send = async () => {
    setLoading(true);
    let allDewormings = activePet.dewormings;
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    allDewormings.push({
      date_application: formValues.date_application,
      lote: formValues.lote,
      next_application: formValues.next_application,
      dewormer: formValues.dewormer,
    });
    var raw = JSON.stringify({
      dewormings: allDewormings,
    });

    var requestOptions = {
      method: "PUT",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(
      "https://agile-sands-59528.herokuapp.com/pets/" + activePet.id,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => {
        navigation.navigate("PetProfile");
        updateVaccines(result);
        setLoading(false);
      })
      .catch((error) => console.log("error", error));
  };
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <Pressable
        onPress={() => navigation.goBack()}
        style={({ pressed }) => [
          {
            opacity: pressed ? 0.5 : 1,
          },
          {
            width: 50,
            height: 50,
            alignItems: "center",
            justifyContent: "center",
          },
        ]}
      >
        <IconSvg
          width="20"
          height="18"
          icon={`<svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.7979 7.59619C18.7161 7.58291 18.6332 7.57678 18.5504 7.57785H4.44077L4.74844 7.43476C5.04917 7.29242 5.32275 7.0987 5.55695 6.86236L9.51363 2.90567C10.0347 2.40823 10.1223 1.60799 9.72112 1.00961C9.25422 0.371974 8.35882 0.233526 7.72114 0.700431C7.66962 0.738173 7.62066 0.779314 7.57464 0.823585L0.419694 7.97853C-0.139465 8.53706 -0.139957 9.4431 0.418576 10.0023L0.419694 10.0034L7.57464 17.1583C8.13425 17.7164 9.04029 17.7151 9.59837 17.1555C9.64229 17.1115 9.68329 17.0646 9.72112 17.0152C10.1223 16.4169 10.0347 15.6166 9.51363 15.1192L5.5641 11.1553C5.35415 10.9452 5.11276 10.7689 4.84861 10.633L4.41931 10.4398H18.4716C19.2026 10.467 19.844 9.95634 19.9813 9.2378C20.1078 8.45769 19.578 7.72274 18.7979 7.59619Z" fill="#309DA3"/></svg>`}
        />
      </Pressable>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#fff",
        }}
      >
        <View style={{ paddingHorizontal: 20 }}>
          <Display>Agregando desparasitación para {activePet?.name}</Display>
          <CustomSelect
            label="Desparasitantes"
            options={dewormings.map((deworming) => deworming.name)}
            defaultValue="Selecciona un Desparasitante"
            changeOption={(option) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  dewormer: dewormings.find((dew) => dew.name == option).id,
                };
              });
            }}
          />
          <DatePicker
            label="Fecha de aplicación"
            setDateValue={(date) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  date_application: date,
                };
              });
            }}
          />
          <MainInput
            label="Lote"
            returnKeyType="next"
            keyboardType="default"
            onChangeText={(text) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  lote: text,
                };
              });
            }}
            onEndEditing={() => {}}
          />
          <DatePicker
            label="Fecha nueva desparasitación"
            setDateValue={(date) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  next_application: date,
                };
              });
            }}
          />
          <MainBtn
            primary
            text={loading ? "Creando..." : "Guardar"}
            onPress={send}
            disabled={loading}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({});

const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    activePet: state.PetsReducer.activePet,
    dewormings: state.PetsReducer.dewormings,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateVaccines: (pet) => dispatch(updateVaccines(pet)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddDeworming);

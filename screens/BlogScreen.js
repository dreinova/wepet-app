import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Pressable,
  useWindowDimensions,
  ScrollView,
} from "react-native";
import { connect } from "react-redux";
import { getNew } from "../store/actions/news";
import RenderHTML, {
  HTMLContentModel,
  HTMLElementModel,
} from "react-native-render-html";

const BlogScreen = ({
  route,
  navigation,
  activePet,
  lang,
  getNew,
  words,
  ...props
}) => {
  const blog = route.params.blog;
  const { width } = useWindowDimensions();
  const source = {
    html: blog.body,
  };
  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: "p",
      mixedUAStyles: {
        fontSize: 15,
        lineHeight: 20,
        marginBottom: 10,
        fontFamily: "Baloo_Paaji_2_Regular",
      },
      contentModel: HTMLContentModel.block,
    }),
    h2: HTMLElementModel.fromCustomModel({
      tagName: "h2",
      mixedUAStyles: {
        color: "#336065",
        fontSize: 15,
        lineHeight: 20,
        marginBottom: 30,
        fontWeight: "bold",
        textTransform: "uppercase",
      },
      contentModel: HTMLContentModel.block,
    }),
  };
  return (
    <View
      style={{
        justifyContent: "space-between",
        flex: 1,
        paddingBottom: 20,
        paddingHorizontal: 20,
        backgroundColor: "#fff",
      }}
    >
      <ScrollView
        contentContainerStyle={{ paddingBottom: 30 }}
        showsVerticalScrollIndicator={false}
      >
        <Image
          style={{
            width: width - 40,
            height: (width / 16) * 9,
            marginBottom: 30,
          }}
          source={{ uri: blog.banner_image.url }}
        />
        <Text style={styles.textH2}>{blog.name}</Text>

        <RenderHTML
          contentWidth={width}
          source={source}
          customHTMLElementModels={customHTMLElementModels}
        />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  textH2: {
    color: "#336065",
    fontSize: 18,
    lineHeight: 25,
    marginBottom: 30,
    textAlign: "center",
    fontFamily: "Baloo_Paaji_2_Bold",
    textTransform: "uppercase",
  },
  text: {
    color: "#336065",
    fontSize: 15,
    lineHeight: 20,
    marginBottom: 10,
    textAlign: "center",
    fontFamily: "Baloo_Paaji_2_Regular",
  },
});
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getNew: (id) => dispatch(getNew(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BlogScreen);

import React, { useEffect, useState } from "react";
import styles from "./styles";
import { View, Text, Pressable, ScrollView } from "react-native";
import Display from "../../components/FontsComponents/Display";
import IconSvg from "../../components/IconSvg";
import MainInput from "../../components/MainInput";
import MainBtn from "../../components/MainBtn";
import CustomSelect from "../../components/CustomSelect";
import Autocomplete from "react-native-autocomplete-input";
import DatePicker from "../../components/DatePicker";
import { connect } from "react-redux";
import { setActivePet } from "../../store/actions/pets";
import { useToast } from "react-native-toast-notifications";
import moment from "moment";
import { setUserLogin } from "../../store/actions/auth";
export const CreatePet = ({
  route,
  navigation,
  setActivePet,
  setUserLogin,
}) => {
  const toast = useToast();
  const [WidthPetIs, setWidthPetIs] = useState(0);
  const [WidthGender, setWidthGender] = useState(0);
  const [activeStep, setActiveStep] = useState(0);
  const [formValues, setFormValues] = useState({
    type: "",
    gender: "",
    name: "",
    breed: "",
    hair_color: "",
    hair_type: "",
    birthday: "",
  });
  const [breeds, setBreeds] = useState([]);
  const [colors, setColors] = useState([]);
  const [loading, setLoading] = useState(false);
  const fetchPetsData = () => {
    const urls = [
      "https://agile-sands-59528.herokuapp.com/colors",
      "https://agile-sands-59528.herokuapp.com/breeds?_limit=350",
    ];
    const allRequests = urls.map(async (url) => {
      let response = await fetch(url);
      return response.json();
    });
    return Promise.all(allRequests);
  };
  const startFetch = () => {
    fetchPetsData()
      .then((arrayOfResponses) => {
        return arrayOfResponses;
      })
      .then((data) => {
        let colors = data[0];
        let breeds = data[1];
        let names = [];
        let colorNames = [];
        breeds.map((breed) => {
          if (breed.type.toLowerCase() == formValues.type.toLowerCase()) {
            names.push(breed.name);
          }
        });
        colors.map((color) => {
          colorNames.push(color.name);
        });
        setBreeds(names);
        setColors(colorNames);
      });
  };

  const submitPet = async () => {
    setLoading(true);
    var formData = new FormData();
    formData.append("name", formValues.name);
    formData.append("especie", formValues.type);
    formData.append("razaInput", formValues.breed);
    formData.append("gender", formValues.gender);
    formData.append("e_date", moment(formValues.birthday).format("YYYY-MM-DD"));
    formData.append("hair_size", formValues.hair_type);
    formData.append("hair_color", formValues.hair_color);
    formData.append(
      "user_id",
      route.params && route.params.user.id ? route.params.user.id : ""
    );
    formData.append(
      "plan",
      route.params && route.params.planID ? route.params.planID : ""
    );
    formData.append(
      "image",
      formValues.type.toLowerCase() === "perro" ? "42" : "43"
    );
    formData.append(
      "id_sub",
      route.params && route.params.idSub ? route.params.idSub : ""
    );
    formData.append(
      "plan_type",
      route.params && route.params.planType ? route.params.planType : ""
    );
    formData.append(
      "user_email",
      route.params && route.params.user ? route.params.user.email : ""
    );
    formData.append(
      "user_name",
      route.params && route.params.user ? route.params.user.name : ""
    );
    formData.append(
      "user_lastname",
      route.params && route.params.user ? route.params.user.lastname : ""
    );
    formData.append("name", formValues.name);
    formData.append(
      "plan_name",
      route.params && route.params.plan ? route.params.plan.name : "Gratuito"
    );
    const url = `https://wepet.co/mi-cuenta/s/pet_create/`;
    const method = "POST";
    await fetch(url, {
      method,
      body: formData,
    })
      .then((res) => res.text())
      .then((data) => {
        toast.show("Mascota Creada", {
          type: "success",
          placement: "bottom",
          duration: 2000,
          animationType: "zoom-in",
        });

        setUserLogin(route.params.user);
        setLoading(false);
        setTimeout(() => {
          // setActivePet(data.resp);
          setWidthPetIs(0);
          setWidthGender(0);
          setActiveStep(0);
          setLoading(false);
          setFormValues({
            type: "",
            gender: "",
            name: "",
            breed: "",
            hair_color: "",
            hair_type: "",
            birthday: "",
          });
          if (route.params.isRegister) {
            navigation.navigate("WizzardScreen");
          } else {
            navigation.navigate("DashboardScreen");
          }
        }, 2000);
      });
  };

  return (
    <View
      style={{
        paddingHorizontal: 20,
        flex: 1,
        backgroundColor: "#fff",
      }}
    >
      <View
        style={{
          alignItems: "center",
          marginBottom: 25,
          flexDirection: "row",
        }}
      >
        {!!formValues.type && (
          <View
            style={{
              backgroundColor: "#FB883F",
              borderRadius: 10,
              paddingVertical: 10,
              paddingHorizontal: 20,
              overflow: "hidden",
              alignItems: "center",
              justifyContent: "center",
              alignSelf: "center",
              marginRight: 10,
            }}
          >
            {!!formValues.type &&
              formValues.type.toLocaleLowerCase() == "perro" && (
                <IconSvg
                  width="38"
                  height="38"
                  icon={`<svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.66882 38C5.38767 38 6.08793 37.8516 6.7696 37.5547C7.45127 37.2578 7.95323 36.9733 8.27547 36.7012C8.59772 36.429 8.98193 36.0703 9.42812 35.625C11.0145 37.2083 13.3942 38 16.5671 38H34.4144C34.7367 38 35.0155 37.8825 35.251 37.6475C35.4865 37.4124 35.6042 37.1341 35.6042 36.8125C35.6042 36.4909 35.4865 36.2126 35.251 35.9775C35.0155 35.7425 34.7367 35.625 34.4144 35.625C34.117 35.625 33.7637 35.3467 33.3547 34.79C32.9457 34.2334 32.5553 33.5283 32.1835 32.6748C31.8117 31.8213 31.4956 30.8008 31.2354 29.6133C30.9751 28.4258 30.8449 27.263 30.8449 26.125C30.8449 25.4818 30.9255 24.9375 31.0866 24.4922C31.2477 24.0469 31.4461 23.6634 31.6815 23.3418C31.917 23.0202 32.1525 22.6676 32.388 22.2842C32.6235 21.9007 32.8218 21.3317 32.9829 20.5771C33.144 19.8226 33.2246 18.901 33.2246 17.8125C33.2246 17.1693 33.144 16.6436 32.9829 16.2354C32.8218 15.8271 32.6235 15.5179 32.388 15.3076C32.1525 15.0973 31.917 14.8809 31.6815 14.6582C31.4461 14.4355 31.2477 14.0954 31.0866 13.6377C30.9255 13.18 30.8449 12.5924 30.8449 11.875C30.8449 10.2917 31.6382 9.5 33.2246 9.5C34.3401 9.5 35.4121 9.07324 36.4408 8.21973C37.4695 7.36621 37.9839 6.60547 37.9839 5.9375C37.9839 5.36849 37.7174 4.8304 37.1845 4.32324C36.6515 3.81608 36.1248 3.5625 35.6042 3.5625C35.1828 3.5625 34.7924 3.4388 34.433 3.19141C34.0736 2.94401 33.7327 2.64714 33.4105 2.30078C33.0883 1.95443 32.735 1.60807 32.3508 1.26172C31.9666 0.915365 31.4461 0.61849 30.7892 0.371094C30.1323 0.123698 29.3577 0 28.4653 0C27.3003 0 26.3521 0.136068 25.6209 0.408203C24.8896 0.680339 24.3443 1.03288 23.9849 1.46582C23.6254 1.89876 23.3342 2.50488 23.1111 3.28418C22.888 4.06348 22.7331 4.83659 22.6463 5.60352C22.5596 6.37044 22.4046 7.34766 22.1815 8.53516C21.9584 9.72266 21.6734 10.8359 21.3264 11.875C20.8554 13.2852 19.7461 15.085 17.9986 17.2744C16.251 19.4639 14.9806 20.8307 14.1874 21.375C11.0145 23.4779 9.42812 26.6445 9.42812 30.875C9.42812 32.0872 8.72166 33.182 7.30874 34.1592C5.89583 35.1364 4.22264 35.625 2.28917 35.625C1.64469 35.625 0.888653 35.5013 0.0210724 35.2539C0.244164 36.1198 0.789501 36.7939 1.65708 37.2764C2.52466 37.7588 3.52858 38 4.66882 38ZM26.0856 26.125C26.16 26.5208 26.2592 27.0342 26.3831 27.665C26.507 28.2959 26.7735 29.2546 27.1825 30.541C27.5915 31.8275 28.0191 32.7305 28.4653 33.25H23.706C23.706 32.1862 23.8299 31.2646 24.0778 30.4854C24.3257 29.7061 24.5984 29.1432 24.8958 28.7969C25.1933 28.4505 25.4659 28.0485 25.7138 27.5908C25.9617 27.1331 26.0856 26.6445 26.0856 26.125Z" fill="#F2D8B6"/></svg>`}
                />
              )}

            {!!formValues.type &&
              formValues.type.toLocaleLowerCase() == "gato" && (
                <IconSvg
                  width="38"
                  height="38"
                  icon={`<svg width="86" height="98" viewBox="0 0 86 98" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.13452 48.9999C6.13452 51.036 5.11208 55.1081 3.06721 61.2163C1.02234 67.3244 -9.15527e-05 71.3965 -9.15527e-05 73.4326C-9.15527e-05 77.5683 0.622955 81.4814 1.86905 85.1717C3.11514 88.8621 4.93636 91.9003 7.33269 94.2863C9.72902 96.6723 12.3969 97.8653 15.3364 97.8653H58.2788C59.1095 97.8653 59.8284 97.5631 60.4355 96.9586C61.0425 96.3541 61.3461 95.6384 61.3461 94.8112C61.3461 93.9841 61.0425 93.2682 60.4355 92.6638C59.8284 92.0593 59.1095 91.7571 58.2788 91.7571C57.3841 91.7571 56.6492 91.4708 56.0741 90.8982C55.499 90.3255 55.2114 89.5938 55.2114 88.703C55.2114 87.0487 55.8505 85.124 57.1285 82.9289C58.4066 80.7338 59.9402 78.475 61.7295 76.1526C63.5187 73.8303 65.308 71.4284 67.0973 68.9469C68.8865 66.4655 70.4202 63.7295 71.6982 60.739C72.9763 57.7486 73.6153 54.8536 73.6153 52.054C73.6153 47.282 71.6982 43.3371 67.8641 40.2194C68.6309 37.9288 70.548 36.7836 73.6153 36.7836C75.4685 36.7836 77.4015 36.1314 79.4144 34.827C81.4273 33.5227 83.0089 32.1547 84.1592 30.7231C85.3094 29.2915 85.8845 28.2576 85.8845 27.6213C85.8845 26.7942 85.581 26.0784 84.9739 25.4739C84.3668 24.8694 83.6479 24.5672 82.8172 24.5672C82.306 24.5672 81.7628 24.1218 81.1877 23.2311C80.6126 22.3403 79.9576 21.2268 79.2227 19.8906C78.4878 18.5545 77.6411 17.2024 76.6826 15.8344C75.7241 14.4665 74.446 13.1939 72.8485 12.0168C71.2509 10.8397 69.4616 10.0285 67.4807 9.5831V0.134521C66.0109 0.134521 64.2856 1.2639 62.3046 3.52265C60.3236 5.7814 59.0456 8.11969 58.4705 10.5375C55.4032 11.81 53.0707 14.0211 51.4732 17.1706C49.8756 20.3201 49.0768 23.8037 49.0768 27.6213C49.0768 29.4665 48.6774 31.2162 47.8787 32.8705C47.0799 34.5248 45.9456 36.0041 44.4759 37.3085C43.0061 38.6128 41.8079 39.599 40.8814 40.2671C39.9548 40.9352 38.7886 41.6828 37.3827 42.51C37.1271 42.7009 36.9354 42.8281 36.8076 42.8917C33.1013 44.9914 30.034 47.0275 27.6057 48.9999C23.1964 52.6266 19.538 57.1759 16.6305 62.6479C13.7229 68.1198 12.2691 72.7327 12.2691 76.4867C12.2691 76.7412 12.2372 77.0275 12.1733 77.3456C12.1094 77.6638 11.8218 78.1092 11.3106 78.6818C10.7994 79.2544 10.0965 79.5408 9.20183 79.5408C8.3072 79.5408 7.57232 79.0477 6.9972 78.0614C6.42208 77.0752 6.13452 75.5323 6.13452 73.4326C6.13452 71.2057 6.54989 68.9787 7.38062 66.7518C8.21134 64.5248 9.23378 62.4093 10.4479 60.405C11.6621 58.4008 12.8762 56.317 14.0903 54.1537C15.3045 51.9904 16.3269 49.3976 17.1577 46.3753C17.9884 43.353 18.4038 40.1558 18.4038 36.7836C18.4038 33.7295 17.1257 30.9299 14.5696 28.3848C12.0135 25.8397 9.20183 24.5672 6.13452 24.5672C4.40916 24.5672 2.95538 25.1558 1.77319 26.3329C0.591003 27.51 -9.15527e-05 28.9575 -9.15527e-05 30.6754C-9.15527e-05 31.6934 0.191612 32.6001 0.575027 33.3954C0.958443 34.1908 1.45368 34.9225 2.06075 35.5906C2.66782 36.2586 3.00331 36.6563 3.06721 36.7836C5.11208 40.1558 6.13452 44.2279 6.13452 48.9999Z" fill="#F2D8B6"/></svg>`}
                />
              )}
            {!!formValues.gender && (
              <Text
                style={{
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 14,
                  textAlign: "center",
                  letterSpacing: -0.02,
                  color: "#F2D8B6",
                  textTransform: "uppercase",
                  marginTop: 8,
                }}
              >
                {formValues.gender}
              </Text>
            )}
          </View>
        )}
        {!!formValues.name && (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: 17,
                color: "#336065",
                marginVertical: 10,
              }}
            >
              {formValues.name}
            </Text>

            <View
              style={{
                width: 1,
                height: 30,
                backgroundColor: "#FB883F",
                marginHorizontal: 10,
              }}
            />
          </View>
        )}
        {!!formValues.breed && (
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={{
                fontStyle: "normal",
                fontWeight: "normal",
                width: 150,
                fontSize: 17,
                color: "#336065",
                marginVertical: 10,
              }}
            >
              {formValues.breed}
            </Text>
          </View>
        )}
      </View>
      {activeStep == 0 && (
        <View>
          <Display style={{ textTransform: "uppercase" }}>
            Tu mascota es: ¿perro o gato?
          </Display>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Pressable
              style={({ pressed }) => [
                {
                  opacity: pressed ? 0.5 : 1,
                },
                styles.pressable,
                { width: WidthPetIs, height: WidthPetIs },
              ]}
              onPress={() => {
                setActiveStep(activeStep + 1);
                setFormValues((prevState) => {
                  return {
                    ...prevState,
                    type: "Perro",
                  };
                });
              }}
              onLayout={(event) => {
                var { x, y, width, height } = event.nativeEvent.layout;

                setWidthPetIs(width);
              }}
            >
              <IconSvg
                width="100"
                height="97"
                icon={`<svg width="100" height="97" viewBox="0 0 100 97" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.3618 96.8655C14.2386 96.8655 16.0669 96.4895 17.8466 95.7375C19.6263 94.9856 20.9369 94.265 21.7782 93.5757C22.6195 92.8864 23.6226 91.9778 24.7875 90.8499C28.9294 94.8603 35.1423 96.8655 43.4261 96.8655H90.0225C90.8638 96.8655 91.5919 96.5678 92.2067 95.9725C92.8215 95.3772 93.1289 94.6723 93.1289 93.8577C93.1289 93.0431 92.8215 92.3381 92.2067 91.7428C91.5919 91.1475 90.8638 90.8499 90.0225 90.8499C89.2459 90.8499 88.3237 90.1449 87.2559 88.735C86.188 87.3251 85.1687 85.5392 84.198 83.3773C83.2272 81.2155 82.4021 78.6306 81.7225 75.6228C81.043 72.615 80.7032 69.6698 80.7032 66.7874C80.7032 65.1581 80.9136 63.7795 81.3342 62.6516C81.7549 61.5237 82.2726 60.5524 82.8874 59.7378C83.5023 58.9232 84.1171 58.0302 84.7319 57.059C85.3467 56.0877 85.8644 54.6464 86.2851 52.7352C86.7058 50.824 86.9161 48.4898 86.9161 45.7327C86.9161 44.1034 86.7058 42.7719 86.2851 41.7379C85.8644 40.704 85.3467 39.9207 84.7319 39.3881C84.1171 38.8554 83.5023 38.3071 82.8874 37.7432C82.2726 37.1792 81.7549 36.3176 81.3342 35.1583C80.9136 33.9991 80.7032 32.5108 80.7032 30.6936C80.7032 26.6832 82.7742 24.678 86.9161 24.678C89.8284 24.678 92.6274 23.597 95.3131 21.4352C97.9989 19.2733 99.3418 17.3464 99.3418 15.6545C99.3418 14.2133 98.6461 12.8504 97.2547 11.5658C95.8632 10.2812 94.488 9.63892 93.1289 9.63892C92.0287 9.63892 91.0095 9.3256 90.0711 8.69897C89.1327 8.07235 88.2428 7.32039 87.4015 6.44312C86.5601 5.56584 85.6379 4.68856 84.6348 3.81128C83.6317 2.934 82.2726 2.18205 80.5576 1.55542C78.8426 0.928792 76.8202 0.615479 74.4904 0.615479C71.4487 0.615479 68.9732 0.960124 67.0641 1.64941C65.1549 2.3387 63.7311 3.23165 62.7927 4.32825C61.8543 5.42485 61.0939 6.96008 60.5115 8.93396C59.929 10.9078 59.5245 12.866 59.298 14.8086C59.0715 16.7511 58.667 19.2263 58.0846 22.2341C57.5021 25.2419 56.7579 28.0618 55.8518 30.6936C54.6222 34.2654 51.7261 38.8241 47.1635 44.3698C42.601 49.9154 39.2842 53.3775 37.2133 54.7561C28.9294 60.0824 24.7875 68.1033 24.7875 78.8186C24.7875 81.8891 22.9431 84.6619 19.2542 87.1371C15.5653 89.6123 11.1969 90.8499 6.14898 90.8499C4.46633 90.8499 2.49246 90.5365 0.227354 89.9099C0.809809 92.1031 2.23359 93.8107 4.49869 95.0326C6.76379 96.2545 9.38484 96.8655 12.3618 96.8655ZM68.2775 66.7874C68.4717 67.79 68.7305 69.0902 69.0541 70.6881C69.3777 72.286 70.0734 74.7142 71.1413 77.9727C72.2091 81.2311 73.3255 83.5183 74.4904 84.8342H62.0647C62.0647 82.1397 62.3883 79.8055 63.0354 77.8317C63.6826 75.8578 64.3945 74.4322 65.1711 73.5549C65.9477 72.6777 66.6596 71.6594 67.3068 70.5001C67.9539 69.3409 68.2775 68.1033 68.2775 66.7874Z" fill="#565656"/></svg>
          `}
              />
            </Pressable>
            <Pressable
              onPress={() => {
                setActiveStep(activeStep + 1);
                setFormValues((prevState) => {
                  return {
                    ...prevState,
                    type: "Gato",
                  };
                });
              }}
              style={({ pressed }) => [
                {
                  opacity: pressed ? 0.5 : 1,
                },
                styles.pressable,
                { width: WidthPetIs, height: WidthPetIs },
              ]}
            >
              <IconSvg
                width="100"
                height="97"
                icon={`<svg width="87" height="98" viewBox="0 0 87 98" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M6.63452 48.9999C6.63452 51.036 5.61208 55.1081 3.56721 61.2163C1.52234 67.3244 0.499908 71.3965 0.499908 73.4326C0.499908 77.5683 1.12296 81.4814 2.36905 85.1717C3.61514 88.8621 5.43636 91.9003 7.83269 94.2863C10.229 96.6723 12.8969 97.8653 15.8364 97.8653H58.7788C59.6095 97.8653 60.3284 97.5631 60.9355 96.9586C61.5425 96.3541 61.8461 95.6384 61.8461 94.8112C61.8461 93.9841 61.5425 93.2682 60.9355 92.6638C60.3284 92.0593 59.6095 91.7571 58.7788 91.7571C57.8841 91.7571 57.1492 91.4708 56.5741 90.8982C55.999 90.3255 55.7114 89.5938 55.7114 88.703C55.7114 87.0487 56.3505 85.124 57.6285 82.9289C58.9066 80.7338 60.4402 78.475 62.2295 76.1526C64.0187 73.8303 65.808 71.4284 67.5973 68.9469C69.3865 66.4655 70.9202 63.7295 72.1982 60.739C73.4763 57.7486 74.1153 54.8536 74.1153 52.054C74.1153 47.282 72.1982 43.3371 68.3641 40.2194C69.1309 37.9288 71.048 36.7836 74.1153 36.7836C75.9685 36.7836 77.9015 36.1314 79.9144 34.827C81.9273 33.5227 83.5089 32.1547 84.6592 30.7231C85.8094 29.2915 86.3845 28.2576 86.3845 27.6213C86.3845 26.7942 86.081 26.0784 85.4739 25.4739C84.8668 24.8694 84.1479 24.5672 83.3172 24.5672C82.806 24.5672 82.2628 24.1218 81.6877 23.2311C81.1126 22.3403 80.4576 21.2268 79.7227 19.8906C78.9878 18.5545 78.1411 17.2024 77.1826 15.8344C76.2241 14.4665 74.946 13.1939 73.3485 12.0168C71.7509 10.8397 69.9616 10.0285 67.9807 9.5831V0.134521C66.5109 0.134521 64.7856 1.2639 62.8046 3.52265C60.8236 5.7814 59.5456 8.11969 58.9705 10.5375C55.9032 11.81 53.5707 14.0211 51.9732 17.1706C50.3756 20.3201 49.5768 23.8037 49.5768 27.6213C49.5768 29.4665 49.1774 31.2162 48.3787 32.8705C47.5799 34.5248 46.4456 36.0041 44.9759 37.3085C43.5061 38.6128 42.3079 39.599 41.3814 40.2671C40.4548 40.9352 39.2886 41.6828 37.8827 42.51C37.6271 42.7009 37.4354 42.8281 37.3076 42.8917C33.6013 44.9914 30.534 47.0275 28.1057 48.9999C23.6964 52.6266 20.038 57.1759 17.1305 62.6479C14.2229 68.1198 12.7691 72.7327 12.7691 76.4867C12.7691 76.7412 12.7372 77.0275 12.6733 77.3456C12.6094 77.6638 12.3218 78.1092 11.8106 78.6818C11.2994 79.2544 10.5965 79.5408 9.70183 79.5408C8.8072 79.5408 8.07232 79.0477 7.4972 78.0614C6.92208 77.0752 6.63452 75.5323 6.63452 73.4326C6.63452 71.2057 7.04989 68.9787 7.88062 66.7518C8.71134 64.5248 9.73378 62.4093 10.9479 60.405C12.1621 58.4008 13.3762 56.317 14.5903 54.1537C15.8045 51.9904 16.8269 49.3976 17.6577 46.3753C18.4884 43.353 18.9038 40.1558 18.9038 36.7836C18.9038 33.7295 17.6257 30.9299 15.0696 28.3848C12.5135 25.8397 9.70183 24.5672 6.63452 24.5672C4.90916 24.5672 3.45538 25.1558 2.27319 26.3329C1.091 27.51 0.499908 28.9575 0.499908 30.6754C0.499908 31.6934 0.691612 32.6001 1.07503 33.3954C1.45844 34.1908 1.95368 34.9225 2.56075 35.5906C3.16782 36.2586 3.50331 36.6563 3.56721 36.7836C5.61208 40.1558 6.63452 44.2279 6.63452 48.9999Z" fill="#565656"/>
          </svg>
          
          `}
              />
            </Pressable>
          </View>
        </View>
      )}
      {activeStep == 1 && (
        <View>
          <Display style={{ textTransform: "uppercase" }}>
            tu {formValues.type} es: ¿hembra o macho?
          </Display>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Pressable
              onPress={() => {
                setActiveStep(activeStep + 1);
                setFormValues((prevState) => {
                  return {
                    ...prevState,
                    gender: "Hembra",
                  };
                });
              }}
              style={({ pressed }) => [
                {
                  opacity: pressed ? 0.5 : 1,
                },
                styles.pressable,
                { width: WidthGender, height: WidthGender },
              ]}
              onLayout={(event) => {
                var { x, y, width, height } = event.nativeEvent.layout;

                setWidthGender(width);
              }}
            >
              <IconSvg
                width="52"
                height="88"
                icon={`<svg width="52" height="88" viewBox="0 0 52 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M26 0.666748C11.661 0.666748 0 12.3277 0 26.6667C0 39.5238 9.39033 50.2054 21.6667 52.2767V65.6667H8.66667V74.3334H21.6667V87.2814H30.3333V74.3334H43.3333V65.6667H30.3333V52.2767C42.6097 50.2011 52 39.5238 52 26.6667C52 12.3277 40.339 0.666748 26 0.666748ZM26 44.0001C16.4407 44.0001 8.66667 36.2261 8.66667 26.6667C8.66667 17.1074 16.4407 9.33342 26 9.33342C35.5593 9.33342 43.3333 17.1074 43.3333 26.6667C43.3333 36.2261 35.5593 44.0001 26 44.0001Z" fill="#565656"/></svg>`}
              />
              <Text style={styles.pressableTxt}>Hembra</Text>
            </Pressable>
            <Pressable
              onPress={() => {
                setActiveStep(activeStep + 1);
                setFormValues((prevState) => {
                  return {
                    ...prevState,
                    gender: "Macho",
                  };
                });
              }}
              style={({ pressed }) => [
                {
                  opacity: pressed ? 0.5 : 1,
                },
                styles.pressable,
                { width: WidthGender, height: WidthGender },
              ]}
            >
              <IconSvg
                width="78"
                height="78"
                icon={`<svg width="78" height="78" viewBox="0 0 78 78" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M47.2874 0V10.8603H59.1399L43.5167 26.3796C39.1329 23.5427 33.9075 21.8964 28.2969 21.8964C12.8045 21.8964 0.243896 34.4545 0.243896 49.9469C0.243831 65.4394 12.8045 78 28.2969 78C43.7893 78 56.3475 65.4394 56.3475 49.9469C56.3475 44.0582 54.5338 38.5934 51.4335 34.0811L66.7943 18.7202V30.4687H77.6547C77.6547 20.9479 77.7578 9.49611 77.7561 0H47.2874ZM28.2969 33.1874C37.5537 33.1874 45.0565 40.6901 45.0565 49.9469C45.0565 59.2037 37.5537 66.709 28.2969 66.709C19.0401 66.709 11.5349 59.2037 11.5349 49.9469C11.5349 40.6901 19.0401 33.1874 28.2969 33.1874Z" fill="#565656"/></svg>`}
              />
              <Text style={styles.pressableTxt}>MACHO</Text>
            </Pressable>
          </View>
        </View>
      )}
      {activeStep == 2 && (
        <View>
          <Display style={{ textTransform: "uppercase" }}>
            ¿Cómo se llama tu {formValues.type}?
          </Display>
          <MainInput
            label=""
            value={formValues.name}
            returnKeyType="next"
            keyboardType="default"
            onChangeText={(text) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  name: text,
                };
              });
            }}
          />
          <MainBtn
            primary
            text="Siguiente"
            onPress={() => {
              setActiveStep(activeStep + 1);
              startFetch();
            }}
          />
        </View>
      )}
      {activeStep == 3 && (
        <View>
          <Display style={{ textTransform: "uppercase" }}>
            ¿Qué raza es {formValues.name}?
          </Display>
          <CustomSelect
            isAutocomplete={true}
            defaultValue="Selecciona una raza"
            options={breeds}
            changeOption={(option) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  breed: option,
                };
              });
            }}
          />

          <MainBtn
            primary
            text="Siguiente"
            onPress={() => {
              setActiveStep(activeStep + 1);
            }}
          />
        </View>
      )}
      {activeStep == 4 && (
        <View>
          <Display style={{ textTransform: "uppercase" }}>
            ¿Cómo es el pelo de {formValues.name}?
          </Display>
          <CustomSelect
            label="COLOR"
            defaultValue="Selecciona un color"
            options={colors}
            changeOption={(option) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  hair_color: option,
                };
              });
            }}
          />
          <CustomSelect
            label="TIPO"
            defaultValue="Selecciona un tipo de pelaje"
            options={["Largo", "Corto"]}
            changeOption={(option) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  hair_type: option,
                };
              });
            }}
          />
          <MainBtn
            primary
            text="Siguiente"
            onPress={() => {
              setActiveStep(activeStep + 1);
            }}
          />
        </View>
      )}
      {activeStep == 5 && (
        <View>
          <Display style={{ textTransform: "uppercase" }}>
            ¿Cuándo nació {formValues.name}?
          </Display>
          <DatePicker
            label="Fecha de nacimiento"
            setDateValue={(date) => {
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  birthday: date,
                };
              });
            }}
          />
          <MainBtn
            primary
            text={loading ? "Creando mascota..." : "Siguiente"}
            onPress={async () => {
              await submitPet();
            }}
          />
        </View>
      )}
    </View>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    setActivePet: (pet) => dispatch(setActivePet(pet)),
    setUserLogin: (user) => dispatch(setUserLogin(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreatePet);

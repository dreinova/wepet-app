export default {
  pressable: {
    flex: 0.5,
    padding: 20,
    backgroundColor: "#E5E4EB",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 10,
  },
  pressableTxt: {
    color: "#AEAEAE",
    fontSize: 26,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    lineHeight: 46,
    textAlign: "center",
    textTransform: "uppercase",
  },
  autocompleteContainer: {
    backgroundColor: "#ffffff",
    borderWidth: 0,
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: "center",
  },
  itemText: {
    fontSize: 15,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
  },
  infoText: {
    textAlign: "center",
    fontSize: 16,
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 1,
  },
};

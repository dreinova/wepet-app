import React, { useEffect, useState, useRef, useCallback } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  FlatList,
  Dimensions,
  Linking,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { useToast } from "react-native-toast-notifications";
import { connect } from "react-redux";
import CuponCard from "../components/CuponCard";
import MainBtn from "../components/MainBtn";
import { CODES } from "../data/codes";

export const CuponesScreen = ({ activePet, words, lang }) => {
  const toast = useToast();
  const [extraCodes, setExtraCodes] = useState([]);
  const [ready, setReady] = useState(false);
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  const [refreshing, setRefreshing] = useState(false);
  const wait = (timeout) => {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  };
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);
  useEffect(() => {
    setLocalLang(lang);
    if (extraCodes.length === 0 && !ready) {
      fetch(`https://wepet.co/mi-cuenta/g/get_codes/?planid=${activePet.plan}`)
        .then(function (response) {
          return response.json();
        })
        .then(function (text) {
          setExtraCodes(text);
          setReady(true);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }, [lang]);
  return (
    <View style={{ backgroundColor: "#fff", flex: 1, position: "relative" }}>
      <Text
        style={{
          color: "#336065",
          fontSize: 23,
          lineHeight: 35,
          marginBottom: 10,
          textAlign: "center",
          fontFamily: "Baloo_Paaji_2_Bold",
          textTransform: "uppercase",
        }}
      >
        Tus CÓDIGOS DE DESCUENTO
      </Text>
      <FlatList
        contentContainerStyle={{
          backgroundColor: "#fff",
          paddingLeft: 10,
          paddingBottom: 60,
        }}
        style={{ marginBottom: 30 }}
        data={extraCodes}
        extraData={extraCodes}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        ListEmptyComponent={() => {
          return !ready ? (
            <ActivityIndicator size="large" color="#3cfbfe" />
          ) : (
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  color: "#336065",
                  fontSize: 18,
                  marginBottom: 10,
                  textAlign: "center",
                  fontFamily: "Baloo_Paaji_2_Regular",
                  width:'80%'
                }}
              >
                No tienes códigos de descuento disponibles en este plan
              </Text>
            </View>
          );
        }}
        renderItem={({ item }) => (
          <CuponCard
            cupon={item}
            text={`${words[localLang].length > 0 && findWords("270")}`}
          />
        )}
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
      />
      <MainBtn
        text={`${words[localLang].length > 0 && findWords("327")}`}
        primary
        style={{
          position: "absolute",
          bottom: -15,
          paddingVertical: 15,
          left: 0,
          borderRadius: 0,
          width: "100%",
        }}
        onPress={() => {
          Linking.openURL(`https://wepet.co/tienda/`);
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setActivePet: (pet) => dispatch(setActivePet(pet)),
    getSingleService: (id, lang) => dispatch(getSingleService(id, lang)),
    setUserLogin: (user) => dispatch(setUserLogin(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CuponesScreen);

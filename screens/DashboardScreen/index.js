import React, { useEffect, useState, useRef } from "react";
import styles from "./styles";
import {
  View,
  Text,
  Image,
  ScrollView,
  FlatList,
  Dimensions,
  Linking,
  Modal,
  Pressable,
  ImageBackground,
} from "react-native";
import MainBtn from "../../components/MainBtn";
import ServiceCard from "../../components/ServiceCard";
import NewsCard from "../../components/NewsCard";
import CuponCard from "../../components/CuponCard";
import { CODES } from "../../data/codes";
import { connect } from "react-redux";
import { getSingleService, setActivePet } from "../../store/actions/pets";
import { setLoadingOff, setLoadingOn } from "../../store/actions/ui";
import Preloader from "../../components/Preloader";
import { useIsFocused } from "@react-navigation/native";
import { setUserLogin } from "../../store/actions/auth";
import { fetchSinToken } from "../../helpers/fetch";
import { useToast } from "react-native-toast-notifications";
import IconSvg from "../../components/IconSvg";
import { CreditCardInput } from "react-native-credit-card-input";
import moment from "moment";
import MainInput from "../../components/MainInput";
import { getNew, getNews } from "../../store/actions/news";
const windowWidth = Dimensions.get("window").width;
const ITEM_WIDTH = windowWidth - 60;
const ITEM_GAP = 20;
export const DashboardScreen = ({
  activePet,
  getSingleService,
  plans,
  user,
  setUserLogin,
  navigation,
  words,
  getNews,
  getNew,
  setActivePetF,
  lang,
}) => {
  const image = {
    uri: "https://res.cloudinary.com/hsgwzwbhc/image/upload/v1637682018/razones_por_las_que_la_alimentacion_natural_horneada_es_la_mejor_alternativa_para_tu_peludo_0c4184672d.jpg",
  };
  const [extraCodes, setExtraCodes] = useState([]);
  const [news, setNews] = useState([]);
  const [ready, setReady] = useState(false);
  const [loading, setLoading] = useState(false);
  const isFocused = useIsFocused();
  useEffect(() => {
    if (user) {
      fetchSinToken("wepetusers/" + user?.id)
        .then((res) => res.json())
        .then((data) => setUserLogin(data));
    }
  }, [isFocused]);
  useEffect(async () => {
    if (extraCodes.length === 0 && !ready) {
      fetch(`https://wepet.co/mi-cuenta/g/get_codes/?planid=${activePet?.plan}`)
        .then(function (response) {
          return response.json();
        })
        .then(function (text) {
          setExtraCodes(text);
          setReady(true);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    const newsRes = await getNews();
    setNews(newsRes);
  }, []);
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs?.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn?.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
    if (extraCodes.length === 0 && !ready) {
      fetch(`https://wepet.co/mi-cuenta/g/get_codes/?planid=${activePet?.plan}`)
        .then(function (response) {
          return response.json();
        })
        .then(function (text) {
          setExtraCodes(text);
          setReady(true);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }, [lang]);
  const [serviceID, setServiceID] = useState(0);
  const [serviceValue, setServiceValue] = useState(0);
  const buyExtra = async (idService, value) => {
    setServiceID(idService);
    setServiceValue(value);
    if (user.pay_data.token) {
      var formData = new FormData();
      formData.append("userID", user.id);
      formData.append("address", user.address);
      formData.append("city", user.city);
      formData.append("customer_id", user.customer_id);
      formData.append("doc_num", user.doc_num);
      formData.append("email", user.email);
      formData.append("lastname", user.lastname);
      formData.append("name", user?.name);
      formData.append("token", user.pay_data.token);
      formData.append("phone", user.phone);
      formData.append("service", idService);
      formData.append("value", value);
      const url = `https://wepet.co/mi-cuenta/s/payServiceApp/`;
      const method = "POST";
      await fetch(url, {
        method,
        body: formData,
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.pay.data.estado == "Aprobada") {
            objIndex = activePet?.services_rel.findIndex(
              (obj) => obj.service.id == idService
            );
            petActiveService[objIndex].quantity =
              petActiveService[objIndex].quantity + 1;
            var raw = { services_rel: petActiveService };
            fetch(
              `https://agile-sands-59528.herokuapp.com/pets/${activePet?.id}`,
              {
                method: "PUT",
                body: raw,
              }
            )
              .then((res) => res.json())
              .then((data) => {
                if (resp.statusCode != 400 && resp.statusCode != 500) {
                  console.log(data);
                }
              });
            setstatusPay(1);
            setActionSheet(true);
          } else if (data.pay.data.estado == "Pendiente") {
            setstatusPay(2);
            setActionSheet(true);
          } else {
            setstatusPay(3);
            setActionSheet(true);
          }
        });
    } else {
      setPayModal(true);
    }
  };
  // MODAL PAGO APROBADO
  const [statusPay, setstatusPay] = useState(1);
  const [actionSheet, setActionSheet] = useState(false);
  const [payModal, setPayModal] = useState(false);
  // CARD VALIDATION
  const [cardInfo, setCardInfo] = useState();
  const onChange = (form) => setCardInfo(form);

  const [docNum, setDocNum] = useState("");
  const [changeFreePlan, setChangeFreePlan] = useState(false);
  const [hideAlert, setHideAlert] = useState(false);
  useEffect(() => {}, [activePet]);
  const [gnrlInfo, setGnrlInfo] = useState(null);
  const [gnrlReady, setGnrlReady] = useState(false);
  useEffect(() => {
    if (!gnrlReady) {
      async function getGeneralInfo() {
        const resp = await fetch(
          "https://agile-sands-59528.herokuapp.com/info-gnrl"
        )
          .then((response) => response.json())
          .then((result) => setGnrlInfo(result))
          .then(() => gnrlInfo)
          .catch((error) => console.log("error", error));
      }
      getGeneralInfo();
    }
  }, []);
  const renderItem = ({ item }) => (
    <Pressable
      onPress={async () => {
        const blog = await getNew(item.id);
        if (blog) {
          navigation.navigate("BlogScreen", { idBlog: item.id, blog });
        }
      }}
    >
      <ImageBackground
        resizeMode="cover"
        source={{ uri: item.image.url }}
        style={styles.image}
      >
        <Text style={styles.text}>{item.name}</Text>
      </ImageBackground>
    </Pressable>
  );
  return (
    <View>
      <Modal
        transparent
        animationType="fade"
        visible={actionSheet}
        onRequestClose={() => {
          changeModalVisibility(false);
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "rgba(0,0,0,.8)",
          }}
        >
          <View
            style={{
              paddingVertical: 50,
              paddingHorizontal: 20,
              backgroundColor: "#fff",
              borderRadius: 8,
            }}
          >
            {statusPay && statusPay == 1 ? (
              <View>
                <IconSvg
                  style={{ alignSelf: "center", marginBottom: 30 }}
                  width={60}
                  height={60}
                  icon={`<?xml version="1.0" encoding="utf-8"?><!-- Generator: Adobe Illustrator 25.4.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve">		<path fill="#309DA3" class="st0" d="M152.5,0C68.4,0,0,68.4,0,152.5S68.4,305,152.5,305c84.1,0,152.5-68.4,152.5-152.5S236.6,0,152.5,0z			 M152.5,280C82.2,280,25,222.8,25,152.5C25,82.2,82.2,25,152.5,25C222.8,25,280,82.2,280,152.5C280,222.8,222.8,280,152.5,280z"/>		<path fill="#309DA3" class="st0" d="M218.5,94l-90.5,90.5l-41.4-41.4c-4.9-4.9-12.8-4.9-17.7,0c-4.9,4.9-4.9,12.8,0,17.7l50.2,50.2			c2.4,2.4,5.6,3.7,8.8,3.7c3.2,0,6.4-1.2,8.8-3.7l99.4-99.4c4.9-4.9,4.9-12.8,0-17.7C231.3,89.1,223.4,89.1,218.5,94z"/></svg>`}
                />
                <Text
                  style={{
                    color: "#309DA3",
                    fontFamily: "Baloo_Paaji_2_Bold",
                    fontSize: 30,
                    textAlign: "center",
                    marginBottom: 20,
                    textAlign: "center",
                    marginBottom: 20,
                  }}
                >
                  Pago Aprobado
                </Text>
                <Text
                  style={{ color: "#565656", marginBottom: 20, fontSize: 14 }}
                >
                  Tu pago ha sido aprobado puedes usar nuevamente tu servicio
                </Text>
                <MainBtn
                  primary
                  text="Continuar"
                  onPress={() => {
                    setActionSheet(false);
                  }}
                />
              </View>
            ) : statusPay == 2 ? (
              <View>
                <IconSvg
                  style={{ alignSelf: "center", marginBottom: 30 }}
                  width={60}
                  height={60}
                  icon={`<?xml version="1.0" encoding="utf-8"?><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve"><g>	<path fill="#FB883F" class="st0" d="M152.5,0C68.4,0,0,68.4,0,152.5S68.4,305,152.5,305c84.1,0,152.5-68.4,152.5-152.5S236.6,0,152.5,0z		 M152.5,280C82.2,280,25,222.8,25,152.5C25,82.2,82.2,25,152.5,25C222.8,25,280,82.2,280,152.5C280,222.8,222.8,280,152.5,280z"/>	<path fill="#FB883F" class="st0" d="M227.7,77.3c-7.4-7.4-19.4-7.4-26.8,0l-48.4,48.4l-48.4-48.4c-7.4-7.4-19.4-7.4-26.8,0s-7.4,19.4,0,26.8		l48.4,48.4l-48.4,48.4c-7.4,7.4-7.4,19.4,0,26.8v0c7.4,7.4,19.4,7.4,26.8,0l48.4-48.4l48.4,48.4c7.4,7.4,19.4,7.4,26.8,0v0		c7.4-7.4,7.4-19.4,0-26.8l-48.4-48.4l48.4-48.4C235.1,96.7,235.1,84.7,227.7,77.3z"/></g></svg>`}
                />
                <Text
                  style={{
                    color: "#FB883F",
                    fontFamily: "Baloo_Paaji_2_Bold",
                    textAlign: "center",
                    marginBottom: 20,
                    fontSize: 30,
                  }}
                >
                  Pago Pendiente
                </Text>
                <Text
                  style={{ color: "#565656", marginBottom: 20, fontSize: 14 }}
                >
                  Tu pago está siendo confirmado por tu banco, esto puede tradar
                  varios minutos.En cuanto sea aprobado te enviaremos un correo
                  electrónico, para que finalices tu proceso de inscripción.
                </Text>
                <MainBtn
                  primary
                  text="Continuar"
                  onPress={() => {
                    setActionSheet(false);
                  }}
                />
              </View>
            ) : (
              statusPay == 3 && (
                <View>
                  <IconSvg
                    style={{ alignSelf: "center", marginBottom: 30 }}
                    width={60}
                    height={60}
                    icon={`<?xml version="1.0" encoding="utf-8"?><!-- Generator: Adobe Illustrator 26.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve"><path fill="#FB883F" class="st0" d="M15,4C8.9,4,4,8.9,4,15s4.9,11,11,11s11-4.9,11-11S21.1,4,15,4z M21.7,16.8c-0.1,0.4-0.5,0.6-0.9,0.5l-5.6-1.1	c-0.2,0-0.4-0.2-0.6-0.3C14.2,15.7,14,15.4,14,15l0,0l0.2-8c0-0.5,0.4-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8l0.1,6.9l5.2,1.8	C21.6,15.8,21.8,16.3,21.7,16.8z"/></svg>`}
                  />
                  <Text
                    style={{
                      color: "#FB883F",
                      fontFamily: "Baloo_Paaji_2_Bold",
                      textAlign: "center",
                      marginBottom: 20,
                      fontSize: 30,
                    }}
                  >
                    Pago Rechazado
                  </Text>
                  <Text
                    style={{ color: "#565656", marginBottom: 20, fontSize: 14 }}
                  >
                    Tu pago ha sido rechazado, intentalo nuevamente más tarde.
                  </Text>
                  <MainBtn
                    primary
                    text="Continuar"
                    onPress={() => {
                      setActionSheet(false);
                    }}
                  />
                </View>
              )
            )}
          </View>
        </View>
      </Modal>
      <Modal
        transparent
        animationType="fade"
        visible={payModal}
        onRequestClose={() => {
          changeModalVisibility(false);
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,.8)",
            flex: 1,
          }}
        >
          <View
            style={{
              backgroundColor: "#fff",
              borderRadius: 8,
              paddingVertical: 50,
              paddingHorizontal: 20,
              height: 600,
              width: 380,
            }}
          >
            <ScrollView
              contentContainerStyle={{
                paddingHorizontal: 20,
                backgroundColor: "#fff",
              }}
              showsVerticalScrollIndicator={false}
            >
              <Text style={styles.textH2}>Agregar método de pago</Text>
              <MainInput
                label="Número de documento"
                value={docNum}
                returnKeyType="next"
                keyboardType={"default"}
                onChangeText={(text) => {
                  setDocNum(text);
                }}
              />
              <CreditCardInput
                onChange={onChange}
                cardScale={0.8}
                labels={{
                  number: "Número de tarjeta",
                  expiry: "Vence",
                  cvc: "CVC/CCV",
                }}
              />
              <MainBtn
                primary
                text="enviar"
                onPress={async () => {
                  var formData = new FormData();
                  if (cardInfo && cardInfo.valid) {
                    formData.append("id", user.id);
                    formData.append("address", user.address);
                    formData.append("city", user.city);
                    formData.append("customer_id", user.customer_id);
                    formData.append("doc_num", user.doc_num);
                    formData.append("email", user.email);
                    formData.append("lastname", user.lastname);
                    formData.append("name", user?.name);
                    formData.append("phone", user.phone);
                    formData.append("service", serviceID);
                    formData.append("value", serviceValue);
                    formData.append("month", cardInfo.values.expiry);
                    formData.append("card_number", cardInfo.values.number);
                    formData.append("secure_code", cardInfo.values.cvc);
                    const url = `https://wepet.co/mi-cuenta/p/buyServiceNoCardApp/`;
                    const method = "POST";
                    await fetch(url, {
                      method,
                      body: formData,
                    })
                      .then((res) => res.json())
                      .then((data) => {
                        if (data.pay.data.estado == "Aprobada") {
                          objIndex = activePet?.services_rel.findIndex(
                            (obj) => obj.service.id == idService
                          );
                          petActiveService[objIndex].quantity =
                            petActiveService[objIndex].quantity + 1;
                          var raw = { services_rel: petActiveService };
                          fetch(
                            `https://agile-sands-59528.herokuapp.com/pets/${activePet?.id}`,
                            {
                              method: "PUT",
                              body: raw,
                            }
                          )
                            .then((res) => res.json())
                            .then((data) => {
                              if (
                                resp.statusCode != 400 &&
                                resp.statusCode != 500
                              ) {
                                console.log(data);
                              }
                            });
                          setPayModal(false);
                          setstatusPay(1);
                          setActionSheet(true);
                        } else if (data.pay.data.estado == "Pendiente") {
                          setPayModal(false);
                          setstatusPay(2);
                          setActionSheet(true);
                        } else {
                          setPayModal(false);
                          setstatusPay(3);
                          setActionSheet(true);
                        }
                      });
                  }
                }}
                style={{ marginTop: 30 }}
              />
              <MainBtn
                tertiary
                text="Cancelar"
                onPress={() => setPayModal(false)}
              />
            </ScrollView>
          </View>
        </View>
      </Modal>
      {moment(new Date()).format("yyyy-MM-DD") > activePet?.sub_end &&
        !!activePet?.id_sub && (
          <View
            style={{
              alignItems: "center",
              backgroundColor: "rgba(255,255,255,.8)",
              flex: 1,
              height: Dimensions.get("window").height - 120,
              justifyContent: "center",
              left: 0,
              padding: 20,
              position: "absolute",
              top: 0,
              zIndex: 5,
            }}
          >
            <Text style={styles.textH2}>{findWords("252")}</Text>
          </View>
        )}

      {!hideAlert &&
        activePet?.plan != 8 &&
        moment(new Date()).format("yyyy-MM-DD") > activePet?.sub_end &&
        !activePet?.id_sub && (
          <View
            style={{
              alignItems: "center",
              backgroundColor: "rgba(255,255,255,.95)",
              flex: 1,
              width: Dimensions.get("window").width,
              height: Dimensions.get("window").height - 120,
              justifyContent: "center",
              left: 0,
              padding: 20,
              position: "absolute",
              top: 0,
              zIndex: 5,
            }}
          >
            <Text>{activePet?.id_sub}</Text>
            <Text style={styles.textH2}>
              La suscripción de {activePet?.name} está vencida
            </Text>
            <Text style={styles.text}>
              cambia tu método de pago para poder renovarla y seguir usando tus
              servicios
            </Text>

            <MainBtn
              primary
              text="Renovar suscripción"
              onPress={() => {
                Linking.openURL(
                  `https://wepet.co/mi-cuenta/s/upgradePlan/?userID=${user.id}&petID=${activePet?.id}&nosub=1`
                );
              }}
            />
            <Text style={styles.textH2}>ó</Text>
            <Text style={styles.text}>pásala a nuestro plan gratuito</Text>
            <MainBtn
              primary2
              text={
                changeFreePlan ? "Actualizando..." : "Cambiar al plan gratuito"
              }
              onPress={async () => {
                setChangeFreePlan(true);

                var formdata = new FormData();
                formdata.append("petID", activePet?.id);

                var requestOptions = {
                  method: "POST",
                  body: formdata,
                  redirect: "follow",
                };
                await fetch(
                  "https://wepet.co/mi-cuenta/s/petFree/",
                  requestOptions
                )
                  .then((response) => response.json())
                  .then((result) => {
                    setChangeFreePlan(false);
                    setActivePetF(result);
                    setHideAlert(true);
                  })
                  .catch((error) => console.log("error", error));
              }}
            />
          </View>
        )}

      <ScrollView contentContainerStyle={{ backgroundColor: "#fff" }}>
        <View style={{ padding: 20 }}>
          {gnrlInfo?.banner && (
            <Pressable
              onPress={() => {
                Linking.openURL(gnrlInfo?.link_banner);
              }}
            >
              <Image
                style={[
                  {
                    width: Dimensions.get("window").width - 40,
                    height: Dimensions.get("window").width / 2,
                    marginBottom: 50,
                  },
                ]}
                source={{
                  uri: gnrlInfo?.banner.url,
                }}
              />
            </Pressable>
          )}
          <Text style={styles.textH2}>{findWords("26")}</Text>
          <FlatList
            data={news}
            extraData={news}
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item) => item.id}
            renderItem={renderItem}
            contentContainerStyle={styles.contentContainerStyle}
            snapToInterval={ITEM_WIDTH + ITEM_GAP}
            snapToAlignment="center"
          />

          <Text style={styles.textH2}>
            {`${words[localLang].length > 0 && findWords("28")} `}
            {localLang == "es" && activePet?.plan.id
              ? localLang == "es" &&
                plans &&
                plans.find((plan) => plan.id == activePet?.plan.id)?.name
              : localLang == "es" &&
                plans &&
                plans.find((plan) => plan.id == activePet?.plan)?.name}{" "}
            {activePet?.name}
          </Text>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "space-between",
              marginTop: 30,
            }}
          >
            {activePet?.services_rel
              .sort((a, b) =>
                a.quantity > b.quantity ? -1 : b.quantity > a.quantity ? 1 : 0
              )
              .map((service, index) => {
                if (service.service.available_upgrade) {
                  return (
                    <ServiceCard
                      service={service.service}
                      key={index}
                      count={service.quantity}
                      userID={user?.id}
                      petID={activePet?.id}
                      avaible={service.available}
                      nodisponible={`${
                        words[localLang].length > 0 && findWords("30")
                      }`}
                      mejorar={`${
                        words[localLang].length > 0 && findWords("27")
                      }`}
                      onPress={async () => {
                        setLoading(true);
                        const resp = await getSingleService(
                          service.service.id,
                          localLang
                        );
                        setLoading(false);
                        if (resp) {
                          navigation.navigate("ServiceScreen");
                        }
                      }}
                      onPressExtra={async () => {
                        if (user.pay_data) {
                          await buyExtra(
                            service.service.id,
                            service.service.price
                          );
                        } else {
                          setPayModal(true);
                        }
                      }}
                    />
                  );
                }
              })}
          </View>
          <Text style={styles.textH2}>{`${
            words[localLang].length > 0 && findWords("264")
          }`}</Text>
          <FlatList
            scrollEnabled={extraCodes.length > 0}
            style={{ marginBottom: 30 }}
            data={extraCodes}
            ListEmptyComponent={() => (
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  flex: 1,
                }}
              >
                <Text
                  style={{
                    color: "#336065",
                    fontSize: 18,
                    marginBottom: 10,
                    textAlign: "center",
                    fontFamily: "Baloo_Paaji_2_Regular",
                    width: 380,
                  }}
                >
                  No tienes códigos de descuento disponibles en este plan
                </Text>
              </View>
            )}
            renderItem={({ item }) => (
              <CuponCard
                cupon={item}
                text={`${words[localLang].length > 0 && findWords("270")}`}
              />
            )}
            decelerationRate="fast"
            horizontal
            showsHorizontalScrollIndicator={false}
          />
          <MainBtn
            tertiary
            text={`${words[localLang].length > 0 && findWords("327")}`}
            onPress={() => {
              Linking.openURL(`https://wepet.co/tienda/`);
            }}
            styleText={{ color: "#336065" }}
          />
        </View>
      </ScrollView>
      {loading && <Preloader />}
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setActivePetF: (pet) => dispatch(setActivePet(pet)),
    getSingleService: (id, lang) => dispatch(getSingleService(id, lang)),
    setUserLogin: (user) => dispatch(setUserLogin(user)),
    getNews: () => dispatch(getNews()),
    getNew: (id) => dispatch(getNew(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);

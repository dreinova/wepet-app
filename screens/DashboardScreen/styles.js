import { Dimensions } from "react-native";
const windowWidth = Dimensions.get("window").width;
const ITEM_WIDTH = windowWidth - 60;
const ITEM_GAP = 20;
export default {
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    borderColor: "#FB883F",
    borderWidth: 2,
    alignSelf: "center",
  },
  textH2: {
    color: "#336065",
    fontSize: 23,
    lineHeight: 35,
    marginBottom: 10,
    textAlign: "center",
    fontFamily: "Baloo_Paaji_2_Bold",
    textTransform: "uppercase",
  },
  text: {
    color: "#336065",
    fontSize: 15,
    lineHeight: 20,
    marginBottom: 10,
    textAlign: "center",
    fontFamily: "Baloo_Paaji_2_Regular",
  },
  namePet: {
    color: "#fb883f",
    fontSize: 30,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    textAlign: "center",
  },
  image: {
    width: ITEM_WIDTH,
    height: Dimensions.get("window").width - 40,
    resizeMode: "cover",
    justifyContent: "flex-end",
    marginBottom: 30,
    marginHorizontal: ITEM_GAP / 2,
  },
  text: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
    paddingHorizontal: 20,
    color: "#336065",
    paddingVertical: 20,
  },
  contentContainerStyle: {
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  item: {
    width: windowWidth - 40,
    height: 200,
    backgroundColor: "#ccc",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 10,
  },
  title: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
    backgroundColor: "rgba(51,96,101,.6)",
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
};

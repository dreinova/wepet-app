import React, { createRef, useEffect, useRef, useState } from "react";
import * as AppleAuthentication from "expo-apple-authentication";
import {
  ActivityIndicator,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Alert,
  Platform,
} from "react-native";
import { maybeCompleteAuthSession } from "expo-web-browser";
import Constants from "expo-constants";
import MainInput from "../../components/MainInput";
import MainBtn from "../../components/MainBtn";
import IconSvg from "../../components/IconSvg";
import { connect } from "react-redux";
import { setUserLogin, startLogin, userLogout } from "../../store/actions/auth";
import { validateInput } from "../../helpers/validators";
import { getAllPlans } from "../../store/actions/plans";
import {
  getAllDataRegister,
  setLoadingOff,
  setLoadingOn,
} from "../../store/actions/ui";
import Preloader from "../../components/Preloader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useToast } from "react-native-toast-notifications";
import { useIsFocused } from "@react-navigation/native";
import * as Linking from "expo-linking";
import * as Google from "expo-auth-session/providers/google";
import * as Facebook from "expo-auth-session/providers/facebook";
import { ResponseType } from "expo-auth-session";
const perro2 = require("../../assets/perro2.jpg");
const perro3 = require("../../assets/perro3.jpg");
maybeCompleteAuthSession();
const useProxy = Constants.appOwnership === "expo" && false;
const prefix = Linking.createURL("/");
export const LoginScreen = ({
  login,
  error,
  navigation,
  getAllPlans,
  setUserLogin,
  setLoadingOn,
  setLoadingOff,
  loading,
  userLogout,
  user,
  words,
  getAllData,
  lang,
}) => {
  const [localLang, setLocalLang] = useState(lang);
  const [data, setData] = useState(null);

  const [gnrlInfo, setGnrlInfo] = useState(null);
  const [gnrlReady, setGnrlReady] = useState(false);
  useEffect(() => {
    if (!gnrlReady) {
      async function getGeneralInfo() {
        const resp = await fetch(
          "https://agile-sands-59528.herokuapp.com/info-gnrl"
        )
          .then((response) => response.json())
          .then((result) => setGnrlInfo(result))
          .then(() => gnrlInfo)
          .catch((error) => console.log("error", error));
      }
      getGeneralInfo();
    }
  }, []);

  const linking = {
    prefixes: [prefix],
    config: {
      screens: {
        Home: "home",
        Settings: "settings",
      },
    },
  };
  function handleDeepLink(event) {
    let data = Linking.parse(event.url);
    setData(data);
    console.log(data);
    if (data.queryParams.idFB) {
      checkUserWepetByFBId(data.queryParams.idFB, data.queryParams.email);
    } else {
      checkUserWepet(data.queryParams.email, "", "", true);
    }
  }

  useEffect(() => {
    const subscription = Linking.addEventListener("url", handleDeepLink);
    return () => {
      subscription.remove();
    };
  }, []);

  const url = Linking.useURL();
  useEffect(() => {
    setLocalLang(lang);
    setFormValues((prevState) => {
      return {
        ...prevState,
        email: {
          ...prevState.email,
          label: `${words[localLang].length > 0 && findWords("7")}`,
        },
        password: {
          ...prevState.password,
          label: `${words[localLang].length > 0 && findWords("8")}`,
        },
      };
    });
  }, [lang]);
  const [getUser, setGetUser] = useState(false);
  const getUserAsyncStorage = async () => {
    try {
      const user = await AsyncStorage.getItem("user");
      let userData = JSON.parse(user);
      if (userData) {
        setLoadingOn();
        if (userData.useremail !== undefined) {
          const values = {
            email: userData.useremail,
            password: userData.userpass,
          };
          const res = await login(values);
          if (res) {
            await getAllPlans();
            try {
              const value = await AsyncStorage.getItem("wizzard");
              console.log(value);
              if (value != null) {
                navigation.navigate("DashboardScreen");
                setFormValues((prevState) => {
                  return {
                    ...prevState,
                    email: { ...prevState.email, value: "" },
                    password: { ...prevState.password, value: "" },
                  };
                });
              } else {
                navigation.navigate("WizzardScreen");
              }
            } catch (e) {
              console.error(e);
              setLoadingOff();
            }
            setLoadingOff();
          } else {
            setLoadingOff();
          }
          if (error) {
            setErrorForm(error);
            setLoadingOff();
          }
        }
      } else {
        setGetUser(true);
        setLoadingOff();
      }
    } catch (e) {
      // error reading value
      console.log(e);
    }
  };
  useEffect(() => {
    if (!getUser) {
      getUserAsyncStorage();
    }
  }, []);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs.localizations[0].id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs.Name;
    } else {
      return wordEn.Name;
    }
  };
  const toast = useToast();
  const [formValues, setFormValues] = useState({
    email: {
      type: "email",
      keyboardType: "email-address",
      label: `${words[localLang].length > 0 && findWords("7")}`,
      value: "",
      validate: {
        email: true,
      },
      message: {
        email: "El correo electrónico no es válido",
      },
    },
    password: {
      type: "password",
      keyboardType: "",
      label: `${words[localLang].length > 0 && findWords("8")}`,
      value: "",
      validate: {
        required: true,
      },
      onSubmitEditing: ({ text, eventCount, target }) => {
        submitLogin();
      },
    },
  });
  const [errorForm, setErrorForm] = useState("");
  const [googleSubmitting, setGoogleSubmitting] = useState(false);
  const fields = ["email", "password"];
  const refs = useRef(fields.map(() => createRef()));
  const submitLogin = async () => {
    fields.map((field, i) => {
      const entries = Object.entries(formValues[field].validate);
      entries.map((entry) => {
        refs.current[i].current.errorFunction(
          validateInput(formValues[field].value, entry[0])
        );
      });
    });
    if (
      validateInput(formValues.email.value, "email") &&
      validateInput(formValues.password.value, "required")
    ) {
      setLoadingOn();
      const values = {
        email: formValues.email.value,
        password: formValues.password.value,
      };
      const res = await login(values);
      if (res) {
        await getAllPlans();
        try {
          const value = await AsyncStorage.getItem("wizzard");
          console.log(value);
          if (value != null) {
            navigation.navigate("DashboardScreen");
            setFormValues((prevState) => {
              return {
                ...prevState,
                email: { ...prevState.email, value: "" },
                password: { ...prevState.password, value: "" },
              };
            });
          } else {
            navigation.navigate("WizzardScreen");
          }
        } catch (e) {
          console.error(e);
          setLoadingOff();
        }
        setLoadingOff();
      } else {
        setLoadingOff();
      }
      if (error) {
        setErrorForm(error);
        setLoadingOff();
      }
    } else {
      setLoadingOff();
    }
  };
  const [request, response, promptAsync] = Google.useAuthRequest({
    iosClientId: `289140913383-6hs30159b6r47qier4jja1ctouascj8u.apps.googleusercontent.com`,
    androidClientId: `289140913383-tjc602lo67o3l263te622let0tlmh217.apps.googleusercontent.com`,
    expoClientId:
      "289140913383-76bv18ph8c723m92n16kvgiglidvf847.apps.googleusercontent.com",
    webClientId:
      "289140913383-rpui9j72gss785bf6q9b02i5m05aue9e.apps.googleusercontent.com",
  });
  const [requestFB, responseFB, promptAsyncFB] = Facebook.useAuthRequest({
    clientId: "1795860513946369",
    expoClientId: "1795860513946369",
    androidClientId: "1795860513946369",
    iosClientId: "1795860513946369",
    responseType: ResponseType.Token,
  });
  const [accessToken, setAccessToken] = useState();
  const [userInfo, setUserInfo] = useState();
  useEffect(() => {
    if (response?.type === "success") {
      const { authentication } = response;
      setAccessToken(authentication.accessToken);
      getUserData();
    }
  }, [response]);
  useEffect(() => {
    if (responseFB?.type === "success") {
      setAccessToken(responseFB.authentication.accessToken);
      dataFBLogin();
    }
  }, [responseFB]);
  const dataFBLogin = async () => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const emailOrId = await fetch(
      `https://graph.facebook.com//v12.0/me?access_token=${accessToken}&fields=id,name,email,about,picture`,
      requestOptions
    )
      .then((response) => response.json())
      .then((result) => result)
      .catch((error) => console.log("error", error));
    await checkUserWepet(emailOrId.email, "", "", true);
  };
  const getUserData = async () => {
    setGoogleSubmitting(true);
    let userInfoResponse = await fetch(
      "https://www.googleapis.com/userinfo/v2/me",
      {
        headers: { Authorization: `Bearer ${accessToken}` },
      }
    );
    userInfoResponse.json().then((data) => {
      setUserInfo(data);
      const { email, familyName, givenName } = data;
      checkUserWepet(email, familyName, givenName, true);
      setGoogleSubmitting(false);
    });
  };
  const checkUserWepet = async (
    email,
    familyName,
    givenName,
    isSocial = false
  ) => {
    setLoadingOn();
    await getAllPlans();
    setLoadingOff();
    await fetch(
      `https://agile-sands-59528.herokuapp.com/wepetusers/?_email=${email}`
    )
      .then((response) => response.json())
      .then(async (data) => {
        if (data.length > 0) {
          if (data[0].pets.length > 0) {
            setUserLogin(data[0]);
            await getAllPlans();
            setLoadingOn();
            const value = await AsyncStorage.getItem("wizzard");
            console.log(value);
            if (value != null) {
              navigation.navigate("DashboardScreen");
            } else {
              navigation.navigate("WizzardScreen");
            }
            setLoadingOff();
            if (error) {
              setLoadingOff();
              setErrorForm(error);
            }
          } else if (data[0].plan.price == 0) {
            setUserLogin(data[0]);
            setLoadingOn();
            setLoadingOff();
            navigation.navigate("CreatePet", {
              planID: data[0].plan.id,
              planType: data[0].plan_type,
              plan: data[0].plan,
              idSub: 0,
              userID: data[0].id,
              user: data[0],
            });
          }
        } else {
          setLoadingOn();
          await getAllPlans();
          setLoadingOff();
          await getAllData();
          navigation.navigate("RegisterScreen", {
            plan: { id: 8 },
            planPeriod: "Mensual",
            finalPrice: 0,
            uemail: email ? email : "",
            uname: "",
            ulastname: "",
            appleID: "",
            isSocial,
          });
        }
      })
      .catch((err) => console.error(err));
  };
  const checkUserWepetById = async (appleID) => {
    setLoadingOn();
    await getAllPlans();
    await getAllData();
    setLoadingOff();
    await fetch(
      `https://agile-sands-59528.herokuapp.com/wepetusers/?_apple_user=${appleID}`
    )
      .then((response) => response.json())
      .then(async (data) => {
        if (data.length > 0) {
          if (data[0].pets.length > 0) {
            setUserLogin(data[0]);
            await getAllPlans();
            setLoadingOn();
            const value = await AsyncStorage.getItem("wizzard");
            console.log(value);
            if (value != null) {
              navigation.navigate("DashboardScreen");
            } else {
              navigation.navigate("WizzardScreen");
            }
            setLoadingOff();
            if (error) {
              setLoadingOff();
              setErrorForm(error);
            }
          } else if (data[0].plan.price == 0) {
            setUserLogin(data[0]);
            setLoadingOn();
            setLoadingOff();
            navigation.navigate("CreatePet", {
              planID: data[0].plan.id,
              planType: data[0].plan_type,
              plan: data[0].plan,
              idSub: 0,
              userID: data[0].id,
              user: data[0],
            });
          }
        } else {
          navigation.navigate("RegisterScreen", {
            plan: { id: 8 },
            planPeriod: "Mensual",
            finalPrice: 0,
            uemail: "",
            uname: "",
            ulastname: "",
            appleID: appleID,
            isSocial: true,
          });
        }
      })
      .catch((err) => console.error(err));
  };
  const checkUserWepetByFBId = async (fbID, email) => {
    setLoadingOn();
    await getAllPlans();
    await getAllData();
    setLoadingOff();
    await fetch(
      `https://agile-sands-59528.herokuapp.com/wepetusers/?_facebook_user=${fbID}`
    )
      .then((response) => response.json())
      .then(async (data) => {
        if (data.length > 0) {
          if (data[0].pets.length > 0) {
            setUserLogin(data[0]);
            await getAllPlans();
            setLoadingOn();
            const value = await AsyncStorage.getItem("wizzard");
            console.log(value);
            if (value != null) {
              navigation.navigate("DashboardScreen");
            } else {
              navigation.navigate("WizzardScreen");
            }
            setLoadingOff();
            if (error) {
              setLoadingOff();
              setErrorForm(error);
            }
          } else if (data[0].plan.price == 0) {
            setUserLogin(data[0]);
            setLoadingOn();
            setLoadingOff();
            navigation.navigate("CreatePet", {
              planID: data[0].plan.id,
              planType: data[0].plan_type,
              plan: data[0].plan,
              idSub: 0,
              userID: data[0].id,
              user: data[0],
            });
          }
        } else {
          navigation.navigate("RegisterScreen", {
            plan: { id: 8 },
            planPeriod: "Mensual",
            finalPrice: 0,
            uemail: email != "undefined" ? email : "",
            uname: "",
            ulastname: "",
            appleID: "",
            fbID: fbID,
            isFBLogin: true,
            isSocial: true,
          });
        }
      })
      .catch((err) => console.error(err));
  };

  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ backgroundColor: "#fff", flex: 1 }}>
        <View style={{ padding: 20, flex: 1 }}>
          {fields.map((field, i) => {
            switch (formValues[field].type) {
              default:
                return (
                  <MainInput
                    key={i}
                    ref={refs.current[i]}
                    label={formValues[field].label}
                    returnKeyType="next"
                    keyboardType={
                      formValues[field].keyboardType
                        ? formValues[field].keyboardType
                        : "default"
                    }
                    value={formValues[field].value}
                    onChangeText={(text) => {
                      setFormValues((prevState) => {
                        return {
                          ...prevState,
                          [field]: { ...prevState[field], value: text },
                        };
                      });
                    }}
                    onEndEditing={() => {
                      const entries = Object.entries(
                        formValues[field].validate
                      );

                      entries.map((entry) => {
                        refs.current[i].current.errorFunction(
                          validateInput(formValues[field].value, entry[0])
                        );
                      });

                      if (formValues[field].validate.same) {
                        refs.current[i].current.errorFunction(
                          formValues[field].value ==
                            formValues[formValues[field].validate.same].value,
                          "Las contraseñas no coinciden"
                        );
                      }
                    }}
                    secureTextEntry={formValues[field].type == "password"}
                    onSubmitEditing={
                      formValues[field].onSubmitEditing &&
                      formValues[field].onSubmitEditing
                    }
                  />
                );
            }
          })}
          <Text
            style={{
              color: "#F00",
              fontSize: 10,
              fontStyle: "normal",
              fontWeight: "bold",
              letterSpacing: -0.02,
              marginBottom: 15,
            }}
          >
            {errorForm == "Not Found" &&
              "No hay usuarios registrados con este correo."}
          </Text>
          <MainBtn
            style={{ paddingVertical: 15 }}
            primary
            text={
              loading
                ? "iniciando sesión..."
                : `${words[localLang].length > 0 && findWords("9")}`
            }
            onPress={() => submitLogin()}
          />
          {!googleSubmitting && gnrlInfo?.google_login ? (
            <MainBtn
              secondary
              disabled={!request}
              text="Sign in with Google"
              onPress={() =>
                Linking.openURL("https://wepet.co/mi-cuenta/appGoogle.php")
              }
              style={
                Platform.OS === "ios" && {
                  borderRadius: 5,
                  borderColor: "#000",
                  borderWidth: 0.5,
                }
              }
              styleText={
                Platform.OS === "ios" && {
                  color: "#000",
                  textTransform: "none",
                  fontSize: 16,
                  fontWeight: "600",
                }
              }
            >
              <IconSvg
                style={{ marginRight: 10 }}
                width="25"
                height="25"
                icon={`<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16Z" fill="white"/><path fill-rule="evenodd" clip-rule="evenodd" d="M23.8299 16.1818C23.8299 15.6146 23.779 15.0691 23.6845 14.5455H16.1499V17.64H20.4554C20.2699 18.64 19.7063 19.4873 18.859 20.0546V22.0619H21.4445C22.9572 20.6691 23.8299 18.6182 23.8299 16.1818Z" fill="#4285F4"/><path fill-rule="evenodd" clip-rule="evenodd" d="M16.1496 24C18.3096 24 20.1205 23.2836 21.4442 22.0618L18.8587 20.0545C18.1423 20.5345 17.226 20.8181 16.1496 20.8181C14.066 20.8181 12.3023 19.4109 11.6732 17.52H9.00049V19.5927C10.3169 22.2072 13.0223 24 16.1496 24Z" fill="#34A853"/><path fill-rule="evenodd" clip-rule="evenodd" d="M11.6735 17.52C11.5135 17.04 11.4226 16.5272 11.4226 16C11.4226 15.4727 11.5135 14.96 11.6735 14.48V12.4072H9.00081C8.45899 13.4872 8.1499 14.709 8.1499 16C8.1499 17.2909 8.45899 18.5127 9.00081 19.5927L11.6735 17.52Z" fill="#FBBC05"/><path fill-rule="evenodd" clip-rule="evenodd" d="M16.1496 11.1818C17.3241 11.1818 18.3787 11.5855 19.2078 12.3782L21.5023 10.0836C20.1169 8.79273 18.306 8 16.1496 8C13.0223 8 10.3169 9.79273 9.00049 12.4073L11.6732 14.48C12.3023 12.5891 14.066 11.1818 16.1496 11.1818Z" fill="#EA4335"/></svg>
              `}
              />
            </MainBtn>
          ) : gnrlInfo?.google_login && googleSubmitting ? (
            <MainBtn secondary text="">
              <ActivityIndicator size="large" color="#3CFBFE" />
            </MainBtn>
          ) : (
            <></>
          )}

          {gnrlInfo?.facebook_login && (
            <MainBtn
              secondary
              disabled={!requestFB}
              style={
                Platform.OS === "ios" && {
                  borderRadius: 5,
                  borderColor: "#000",
                  borderWidth: 0.5,
                }
              }
              styleText={
                Platform.OS === "ios" && {
                  color: "#000",
                  textTransform: "none",
                  fontSize: 16,
                  fontWeight: "600",
                }
              }
              text="Sign in with Facebook"
              onPress={() => {
                Linking.openURL("https://wepet.co/mi-cuenta/appFacebook.php");
              }}
            >
              <IconSvg
                style={{ marginRight: 10 }}
                width="25"
                height="25"
                icon={`<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16Z" fill="#1877F2"/><path d="M24 16C24 11.6 20.4 8 16 8C11.6 8 8 11.6 8 16C8 20 10.9 23.3 14.7 23.9V18.3H12.7V16H14.7V14.2C14.7 12.2 15.9 11.1 17.7 11.1C18.6 11.1 19.5 11.3 19.5 11.3V13.3H18.5C17.5 13.3 17.2 13.9 17.2 14.5V16H19.4L19 18.3H17.1V24C21.1 23.4 24 20 24 16Z" fill="white"/></svg>`}
              />
            </MainBtn>
          )}

          {Platform.OS === "ios" && gnrlInfo?.apple_login && (
            <AppleAuthentication.AppleAuthenticationButton
              buttonType={
                AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN
              }
              buttonStyle={
                AppleAuthentication.AppleAuthenticationButtonStyle.WHITE_OUTLINE
              }
              cornerRadius={5}
              style={{ width: "100%", height: 44, marginBottom: 15 }}
              onPress={async () => {
                try {
                  const credential = await AppleAuthentication.signInAsync({
                    requestedScopes: [
                      AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                      AppleAuthentication.AppleAuthenticationScope.EMAIL,
                    ],
                  });

                  if (credential.email) {
                    checkUserWepet(
                      credential.email,
                      credential.fullName.givenName,
                      credential.fullName.familyName,
                      true
                    );
                  } else if (credential.user) {
                    checkUserWepetById(credential.user);
                  } else {
                    setLoadingOn();
                    await getAllPlans();
                    setLoadingOff();
                    await getAllData();
                    navigation.navigate("RegisterScreen", {
                      plan: { id: 8 },
                      planPeriod: "Mensual",
                      finalPrice: 0,
                      uemail: "",
                      uname: "",
                      ulastname: "",
                      appleID: credential.user,
                      isSocial,
                    });
                  }
                  // signed in
                } catch (e) {
                  if (e.code === "ERR_CANCELED") {
                    // handle that the user canceled the sign-in flow
                  } else {
                    // handle other errors
                    console.error(e);
                  }
                }
              }}
            />
          )}

          <MainBtn
            tertiary
            text={`${words[localLang].length > 0 && findWords("5")}`}
            onPress={async () => {
              setLoadingOn();
              await getAllPlans();
              setLoadingOff();
              await getAllData();
              navigation.navigate("RegisterScreen", {
                plan: { id: 8 },
                planPeriod: "Mensual",
                finalPrice: 0,
                uemail: "",
                uname: "",
                ulastname: "",
                appleID: "",
                isSocial: false,
              });
            }}
          />
          <MainBtn
            tertiary
            text={`${words[localLang].length > 0 && findWords("18")}`}
            onPress={() => navigation.navigate("RecoverPassword")}
          />
          <MainBtn
            tertiary
            text={`${words[localLang].length > 0 && findWords("318")}`}
            onPress={() => navigation.navigate("WhatScreen")}
            styleText={{ color: "#336065" }}
          />
        </View>
        {/* <Image
          source={perro3}
          style={{
            width: Dimensions.get("window").width,
            height: 150,
            resizeMode: "contain",
            zIndex: -1,
          }}
        /> */}
      </ScrollView>

      {loading && <Preloader />}
    </KeyboardAvoidingView>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    error: state.AuthReducer.error,
    loading: state.UiReducer.loading,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: async ({ email, password }) =>
      await dispatch(startLogin(email, password)),
    setUserLogin: (user) => dispatch(setUserLogin(user)),
    getAllPlans: () => dispatch(getAllPlans()),
    setLoadingOn: () => dispatch(setLoadingOn()),
    setLoadingOff: () => dispatch(setLoadingOff()),
    userLogout: () => dispatch(userLogout()),
    getAllData: async () => await dispatch(getAllDataRegister()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({});

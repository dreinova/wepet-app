import React, { useState } from "react";
import { View, ScrollView, Text } from "react-native";
import { CreditCardInput } from "react-native-credit-card-input";
import { connect } from "react-redux";
import MainBtn from "../../components/MainBtn";
import { fetchSinToken } from "../../helpers/fetch";
import { getAllDataRegister } from "../../store/actions/ui";

export const PayScreen = ({ route, navigation, getAllData }) => {
  const {
    plan,
    plan: { id, name },
    planPeriod,
    finalPrice,
  } = route.params;
  const [cardInfo, setCardInfo] = useState();
  const [loading, setLoading] = useState(false);
  const onChange = (form) => setCardInfo(form);
  const onSubmit = async () => {
    setLoading(true);
    if (cardInfo && cardInfo.valid) {
      var formData = new FormData();
      formData.append("month", cardInfo.values.expiry);
      formData.append("card_number", cardInfo.values.number);
      formData.append("secure_code", cardInfo.values.cvc);
      formData.append("planID", id);
      formData.append("type", planPeriod);
      const url = `https://wepet.co/mi-cuenta/s/creditApp/`;
      const method = "POST";
      await fetch(url, {
        method,
        body: formData,
      })
        .then((res) => res.json())
        .then(async (data) => {
          if (data.message == "1") {
            await getAllData();
            navigation.navigate("RegisterScreen", {
              user: data.info.user,
              planPeriod,
              finalPrice,
              plan,
            });
            setLoading(false);
          } else {
            setLoading(false);
            console.error("No se pudo");
          }
        })
        .catch((err) => console.error(err));
    } else {
      console.error("No se han completado los datos");
      setLoading(false);
    }
  };
  return (
    <ScrollView
      contentContainerStyle={{ paddingHorizontal: 20, backgroundColor: "#fff" }}
      showsVerticalScrollIndicator={false}
    >
      <Text
        style={{
          fontFamily: "Baloo_Paaji_2_Bold",
          color: "#336065",
          fontSize: 25,
          lineHeight: 25 + 8,
          textAlign: "center",
          maxWidth: 320,
          alignSelf: "center",
        }}
      >
        Datos de pago con tarjeta de crédito
      </Text>
      <CreditCardInput
        onChange={onChange}
        cardScale={1.1}
        labels={{
          number: "Número de tarjeta",
          expiry: "Vence",
          cvc: "CVC/CCV",
        }}
      />
      <View style={{ padding: 40 }}>
        <MainBtn
          primary
          text={loading ? "Validando tarjeta..." : "Guardar"}
          onPress={() => onSubmit()}
        />
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllData: async () => await dispatch(getAllDataRegister()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PayScreen);

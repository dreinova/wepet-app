import React, { useEffect, useState } from "react";
import moment from "moment";
import * as ImagePicker from "expo-image-picker";
import styles from "./styles";
import {
  View,
  ActivityIndicator,
  Text,
  Image,
  ScrollView,
  Dimensions,
  Button,
  Pressable,
} from "react-native";
import MainInput from "../../components/MainInput";
import CustomSelect from "../../components/CustomSelect";
import CustomSwitch from "../../components/CustomSwitch";
import Display from "../../components/FontsComponents/Display";
import IconSvg from "../../components/IconSvg";
import { TabsNavigation } from "./tabs/TabsNavigation";
import { connect } from "react-redux";
import { editPhotoPet, updatePet, getPetData } from "../../store/actions/pets";
import MainBtn from "../../components/MainBtn";
import { Entypo } from "@expo/vector-icons";
import { useToast } from "react-native-toast-notifications";
import DatePicker from "../../components/DatePicker";

export const PetProfile = ({
  activePet,
  colors,
  breeds,
  updatePet,
  foods,
  navigation,
  getPetData,
  editPhotoPet,
  words,
  lang,
}) => {
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);
  const toast = useToast();
  const [active, setActive] = useState(false);
  const [image, setImage] = useState(null);
  const [loading, setLoading] = useState(false);
  const [changePhoto, setChangePhoto] = useState(false);
  const [selectedDate, setSelectedDate] = useState(activePet.birthday);
  const ageCalculator = (dateUser) => {
    var mdate = dateUser.toString();
    var dobYear = parseInt(mdate.substring(0, 4), 10);
    var dobMonth = parseInt(mdate.substring(5, 7), 10);
    var dobDate = parseInt(mdate.substring(8, 10), 10);

    var today = new Date();
    var birthday = new Date(dobYear, dobMonth - 1, dobDate);

    var diffInMillisecond = today.valueOf() - birthday.valueOf();
    var year_age = Math.floor(diffInMillisecond / 31536000000);
    var day_age = Math.floor((diffInMillisecond % 31536000000) / 86400000);

    var month_age = Math.floor(day_age / 30);
    day_age = day_age % 30;

    return ` ${(() => {
      if (year_age == 1) {
        return `${year_age} año`;
      } else if (year_age > 1) {
        return `${year_age} años`;
      } else {
        return ``;
      }
    })()}  ${(() => {
      if (month_age == 1) {
        return `${month_age} mes`;
      } else if (month_age > 1) {
        return `${month_age} meses`;
      } else {
        return ``;
      }
    })()}`;
  };
  const pickImage = async () => {
    setChangePhoto(true);
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      await editPhotoPet(result.uri, activePet.id);
      setImage(result.uri);
      setChangePhoto(false);
    } else {
      setChangePhoto(false);
    }
  };
  const [formValues, setFormValues] = useState({
    type: activePet.type,
    size: activePet.size,
    breed: activePet.breed,
    gender: activePet.gender,
    age: activePet.age,
    hair_size: activePet.hair_size,
    hair_color: activePet.hair_color,
    food: activePet.food,
    allergies: activePet.allergies,
    record: activePet.record,
    name: activePet?.name,
    sterilized: activePet.sterilized,
    birthday: activePet.birthday,
    bath_frequency: activePet.bath_frequency,
  });
  useEffect(async () => {
    if (foods.length == 0) await getPetData(activePet.type.toLowerCase());
  }, []);
  return (
    <View style={{ backgroundColor: "#fff" }}>
      <View
        style={{
          paddingHorizontal: 20,
          paddingBottom: 30,
        }}
      >
        <Pressable
          onPress={() => navigation.navigate("DashboardScreen")}
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            {
              width: 50,
              height: 50,
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        >
          <IconSvg
            width="20"
            height="18"
            icon={`<svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.7979 7.59619C18.7161 7.58291 18.6332 7.57678 18.5504 7.57785H4.44077L4.74844 7.43476C5.04917 7.29242 5.32275 7.0987 5.55695 6.86236L9.51363 2.90567C10.0347 2.40823 10.1223 1.60799 9.72112 1.00961C9.25422 0.371974 8.35882 0.233526 7.72114 0.700431C7.66962 0.738173 7.62066 0.779314 7.57464 0.823585L0.419694 7.97853C-0.139465 8.53706 -0.139957 9.4431 0.418576 10.0023L0.419694 10.0034L7.57464 17.1583C8.13425 17.7164 9.04029 17.7151 9.59837 17.1555C9.64229 17.1115 9.68329 17.0646 9.72112 17.0152C10.1223 16.4169 10.0347 15.6166 9.51363 15.1192L5.5641 11.1553C5.35415 10.9452 5.11276 10.7689 4.84861 10.633L4.41931 10.4398H18.4716C19.2026 10.467 19.844 9.95634 19.9813 9.2378C20.1078 8.45769 19.578 7.72274 18.7979 7.59619Z" fill="#309DA3"/></svg>`}
          />
        </Pressable>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 150 }}
      >
        <View style={{ paddingHorizontal: 20 }}>
          <Pressable onPress={pickImage}>
            <View
              style={{
                position: "relative",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {changePhoto && (
                <ActivityIndicator
                  size="large"
                  color="#FB883F"
                  style={{ position: "absolute", zIndex: 10 }}
                />
              )}
              <Image
                style={styles.profileImage}
                source={{
                  uri:
                    activePet?.image?.length > 0
                      ? activePet?.image[0].url
                      : "https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg",
                }}
              />
              <View
                style={{
                  backgroundColor: "#FB883F",
                  padding: 8,
                  position: "absolute",
                  right: 220 / 2,
                  bottom: 0,
                  borderRadius: 30,
                }}
              >
                <Entypo name="edit" size={24} color="white" />
              </View>
            </View>
          </Pressable>
          <MainInput
            label={`${words[localLang].length > 0 && findWords("331")}`}
            value={formValues.name}
            onChangeText={(text) =>
              setFormValues((prevState) => ({
                ...prevState,
                name: text,
              }))
            }
          />
          <CustomSelect
            defaultValue={formValues.size}
            label={`${words[localLang].length > 0 && findWords("292")}`}
            options={["Pequeño", "Mediano", "Grande", "Gigante"]}
            changeOption={(option) => {}}
          />
          <CustomSelect
            defaultValue={formValues.breed}
            label={`${words[localLang].length > 0 && findWords("294")}`}
            options={breeds
              .filter(
                (breed) =>
                  breed.type.toLowerCase() == formValues.type.toLowerCase()
              )
              .map((breed) => breed.name)}
            changeOption={(option) => {}}
          />
          <CustomSelect
            defaultValue={formValues.bath_frequency}
            label={`${words[localLang].length > 0 && findWords("365")}`}
            options={["8 días", "15 días", "1 mes", "3 meses"]}
            changeOption={(option) => {}}
          />

          <DatePicker
            label="Fecha de nacimiento"
            value={activePet.birthday}
            setDateValue={(date) => {
              setSelectedDate(date);
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  birthday: moment(date).format("YYYY-MM-DD"),
                };
              });
              setFormValues((prevState) => {
                return {
                  ...prevState,
                  age: ageCalculator(date),
                };
              });
            }}
          />
          <MainInput
            editable={false}
            label={`${words[localLang].length > 0 && findWords("333")}`}
            value={formValues.age}
            onChangeText={(text) =>
              setFormValues((prevState) => ({
                ...prevState,
                age: text,
              }))
            }
          />
          <CustomSelect
            defaultValue={formValues.hair_color}
            label={`${words[localLang].length > 0 && findWords("335")}`}
            options={colors.map((color) => color.name)}
            changeOption={(option) => {
              setFormValues((prevState) => ({
                ...prevState,
                hair_color: option,
              }));
            }}
          />
          <CustomSelect
            defaultValue={formValues.hair_size}
            label={`${words[localLang].length > 0 && findWords("336")}`}
            options={["Corto", "Largo"]}
            changeOption={(option) => {
              setFormValues((prevState) => ({
                ...prevState,
                hair_size: option,
              }));
            }}
          />
          <Display>{`${
            words[localLang].length > 0 && findWords("36")
          }`}</Display>
          <Text style={styles.label}>
            {`${words[localLang].length > 0 && findWords("35")}`}
          </Text>
          <CustomSwitch
            onPress={() => {
              setActive(!active);
              setFormValues((prevState) => ({
                ...prevState,
                sterilized: !formValues.sterilized,
              }));
            }}
            active={formValues.sterilized}
          />
          <CustomSelect
            label={`${words[localLang].length > 0 && findWords("40")}`}
            defaultValue={formValues.food}
            options={foods.map((food) => food.name)}
            changeOption={(option) => {
              setFormValues((prevState) => ({
                ...prevState,
                food: option,
              }));
            }}
          />
          <MainInput
            textarea
            label={`${words[localLang].length > 0 && findWords("39")}`}
            value={formValues.allergies}
            onChangeText={(text) => {
              setFormValues((prevState) => ({
                ...prevState,
                allergies: text,
              }));
            }}
          />
          <MainInput
            textarea
            label={`${words[localLang].length > 0 && findWords("41")}`}
            value={formValues.record}
            onChangeText={(text) => {
              setFormValues((prevState) => ({
                ...prevState,
                record: text,
              }));
            }}
          />
          <View
            style={{
              minHeight: Dimensions.get("window").height / 2,
            }}
          >
            <TabsNavigation />
          </View>
          <View style={styles.alert}>
            <IconSvg
              style={{ marginRight: 30 }}
              width="30"
              height="30"
              icon={`<svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.8 1.6131L19.501 12.7741C20.464 14.3771 19.991 16.4861 18.444 17.4841C17.9248 17.8203 17.3196 17.9995 16.701 18.0001H3.298C1.477 18.0001 0 16.4701 0 14.5811C0 13.9421 0.173 13.3171 0.498 12.7741L7.2 1.6131C8.162 0.0101024 10.196 -0.480898 11.743 0.517102C12.171 0.793103 12.533 1.1681 12.8 1.6131ZM10 14.0001C10.2652 14.0001 10.5196 13.8947 10.7071 13.7072C10.8946 13.5197 11 13.2653 11 13.0001C11 12.7349 10.8946 12.4805 10.7071 12.293C10.5196 12.1055 10.2652 12.0001 10 12.0001C9.73478 12.0001 9.48043 12.1055 9.29289 12.293C9.10536 12.4805 9 12.7349 9 13.0001C9 13.2653 9.10536 13.5197 9.29289 13.7072C9.48043 13.8947 9.73478 14.0001 10 14.0001ZM10 5.0001C9.73478 5.0001 9.48043 5.10546 9.29289 5.293C9.10536 5.48053 9 5.73489 9 6.0001V10.0001C9 10.2653 9.10536 10.5197 9.29289 10.7072C9.48043 10.8947 9.73478 11.0001 10 11.0001C10.2652 11.0001 10.5196 10.8947 10.7071 10.7072C10.8946 10.5197 11 10.2653 11 10.0001V6.0001C11 5.73489 10.8946 5.48053 10.7071 5.293C10.5196 5.10546 10.2652 5.0001 10 5.0001Z" fill="#336065"/></svg>`}
            />
            <Text style={styles.alertText}>
              {`${words[localLang].length > 0 && findWords("43")}`}
            </Text>
          </View>
        </View>
      </ScrollView>
      <MainBtn
        text={
          loading
            ? `${words[localLang].length > 0 && findWords("338")}`
            : `${words[localLang].length > 0 && findWords("337")}`
        }
        primary
        style={{
          position: "absolute",
          bottom: 60,
          paddingVertical: 15,
          left: 0,
          borderRadius: 0,
          width: "100%",
        }}
        onPress={async () => {
          setLoading(true);
          const petedited = await updatePet(activePet.id, formValues);
          if (petedited) {
            setLoading(false);
            toast.show(`${words[localLang].length > 0 && findWords("343")}`, {
              type: "success",
              placement: "bottom",
              duration: 2000,
              animationType: "zoom-in",
            });
            navigation.navigate("DashboardScreen");
          }
        }}
      />
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    colors: state.PetsReducer.colors,
    breeds: state.PetsReducer.breeds,
    foods: state.PetsReducer.foods,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePet: async (id, data) => await dispatch(updatePet(id, data)),
    editPhotoPet: async (img, id) => await dispatch(editPhotoPet(img, id)),
    getPetData: async (type) => await dispatch(getPetData(type)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PetProfile);

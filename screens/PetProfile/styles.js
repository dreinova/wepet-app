import { StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  profileImage: {
    width: 180,
    height: 180,
    borderRadius: 180 / 2,
    borderColor: "#FB883F",
    borderWidth: 6,
    alignSelf: "center",
  },
  label: {
    color: "#336065",
    fontSize: 14,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    lineHeight: 14,
    marginBottom: 8,
  },
  alert: {
    alignItems: "center",
    backgroundColor: "#f2d8b6",
    flexDirection: "row",
    maxWidth: 738,
    paddingHorizontal: 15,
    paddingVertical: 10,
    width: "100%",
  },
  alertText: {
    color: "#336065",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "normal",
    letterSpacing: -0.02,
    lineHeight: 22,
    flex: 1,
  },
});

import React, { useEffect, useState } from "react";
import styles from "./styles";
import { View, Text, FlatList } from "react-native";
import MainBtn from "../../../../components/MainBtn";
import VaccineCard from "../../../../components/VaccineCard";
import { connect } from "react-redux";
import {
  editPhotoPet,
  getPetData,
  updatePet,
} from "../../../../store/actions/pets";

export const Bath = ({ navigation, activePet, words, lang, ...props }) => {
  const [localLang, setLocalLang] = useState(lang);
  const [reminders, setReminders] = useState([]);

  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };

  const getRemindersBath = async () => {
    const response = await fetch(
      `https://agile-sands-59528.herokuapp.com/reminders?_pet=${activePet.id}&type=bano`
    );
    const data = await response.json();
    setReminders(data);
  };

  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);
  useEffect(() => {
    getRemindersBath();
  }, []);
  return (
    <View>
      <FlatList
        data={reminders}
        extraData={reminders}
        horizontal
        renderItem={({ item, index }) => (
          <VaccineCard
            activePet={activePet?.id}
            id={item.id}
            name="Baño Periódico"
            date_application={item.fecha}
            navigation={navigation}
            vaccine={false}
            bath={true}
            setReminders={setReminders}
          />
        )}
        ListEmptyComponent={() => (
          <VaccineCard
            activePet={activePet?.id}
            id={0}
            name="Baño Periódico"
            navigation={navigation}
            vaccine={false}
            bath={true}
          />
        )}
      />
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    colors: state.PetsReducer.colors,
    breeds: state.PetsReducer.breeds,
    foods: state.PetsReducer.foods,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePet: async (id, data) => await dispatch(updatePet(id, data)),
    editPhotoPet: async (img, id) => await dispatch(editPhotoPet(img, id)),
    getPetData: async (type) => await dispatch(getPetData(type)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Bath);

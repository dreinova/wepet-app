import React, { useEffect, useState } from "react";
import styles from "./styles";
import { View, Text, FlatList } from "react-native";
import MainBtn from "../../../../components/MainBtn";
import VaccineCard from "../../../../components/VaccineCard";
import { connect } from "react-redux";
import {
  editPhotoPet,
  getPetData,
  updatePet,
} from "../../../../store/actions/pets";

export const Deworming = ({ navigation, activePet, words, lang }) => {
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);
  return (
    <View>
      <MainBtn
        tertiary
        text={`+ ${words[localLang].length > 0 && findWords("340")}`}
        onPress={() => navigation.navigate("AddDeworming")}
      />
      <FlatList
        data={activePet.dewormings}
        horizontal
        renderItem={({ item, index }) => (
          <VaccineCard
            activePet={activePet}
            id={item.id}
            name={item.dewormer.name}
            date_application={item.date_application}
            due_date={item.next_application}
            lote={item.lote}
            veterinarian={item.veterinarian}
            navigation={navigation}
            vaccine={false}
          />
        )}
      />
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    colors: state.PetsReducer.colors,
    breeds: state.PetsReducer.breeds,
    foods: state.PetsReducer.foods,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePet: async (id, data) => await dispatch(updatePet(id, data)),
    editPhotoPet: async (img, id) => await dispatch(editPhotoPet(img, id)),
    getPetData: async (type) => await dispatch(getPetData(type)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Deworming);

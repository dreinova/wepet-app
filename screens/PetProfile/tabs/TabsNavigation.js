import * as React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
// import TabBarComp from "./TabBarComp";
import Vacunas from "./Vacunas";
import Deworming from "./Desparasitacion";
import Bath from "./Bano";
import { Animated, View, Pressable, Dimensions, Text } from "react-native";

const Tab = createMaterialTopTabNavigator();
function MyTabBar({ state, descriptors, navigation, position }) {
  return (
    <View style={{ flexDirection: "row" }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;
        const isFocused = state.index === index;
        const onPress = () => {
          const event = navigation.emit({
            type: "tabPress",
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({ name: route.name, merge: true });
          }
        };
        const onLongPress = () => {
          navigation.emit({
            type: "tabLongPress",
            target: route.key,
          });
        };
        return (
          <Pressable
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                marginRight: 15,
                marginBottom: 15,
                alignItems: "center",
                justifyContent: "center",
              },
            ]}
          >
            <Animated.Text
              style={[
                {
                  fontFamily: "Baloo_Paaji_2_Bold",
                  fontSize: 18,
                  textTransform: "uppercase",
                  color: "#b2b2b2",
                  textAlign: "center",
                },
                isFocused && { color: "#336065" },
              ]}
            >
              {label}
            </Animated.Text>
          </Pressable>
        );
      })}
    </View>
  );
}
export const TabsNavigation = () => {
  return (
    <Tab.Navigator
      tabBar={(props) => <MyTabBar {...props} />}
      initialRouteName="vacunas"
      sceneContainerStyle={{ backgroundColor: "#fff" }}
    >
      <Tab.Screen
        name="vacunas"
        options={{ tabBarLabel: "Vacunas" }}
        component={Vacunas}
      />
      <Tab.Screen
        name="deworming"
        options={{ tabBarLabel: "desparasitación" }}
        component={Deworming}
      />
      <Tab.Screen
        name="bath"
        options={{ tabBarLabel: "Baño" }}
        component={Bath}
      />
    </Tab.Navigator>
  );
};

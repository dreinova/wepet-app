import React, { useState } from "react";
import styles from "./styles";
import {
  View,
  Text,
  FlatList,
  useWindowDimensions,
  ScrollView,
  Pressable,
  Modal,
} from "react-native";
import PlanCard from "../../components/PlanCard";
import Display from "../../components/FontsComponents/Display";
import { connect } from "react-redux";
import MainBtn from "../../components/MainBtn";
import Regular from "../../components/FontsComponents/Regular";
import IconSvg from "../../components/IconSvg";
const currencyFormat = (num) => {
  return (
    "$" +
    parseInt(num)
      .toFixed(0)
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  );
};
export const PlansScreen = ({ plans, route, user, navigation }) => {
  const renderItem = ({ item }) => {
    let finalPrice = (item.price * item.annual_discount) / 100;
    finalPrice = item.price - finalPrice;
    finalPrice = finalPrice * 12;
    return (
      <PlanCard
        plan={item}
        key={item.id}
        name={item.name}
        uemail={route.params && route.params.email ? route.params.email : ""}
        uname={
          route.params && route.params.givenName ? route.params.givenName : ""
        }
        ulastname={
          route.params && route.params.familyName ? route.params.familyName : ""
        }
        appleID={
          route.params && route.params.appleID ? route.params.appleID : ""
        }
        price={planPeriod ? item.price : finalPrice}
        period={item.period}
        desc={planPeriod ? item.description_mensual : item.description_anual}
        recommended={item.id == 5}
        planPeriod={planPeriod}
        finalPrice={finalPrice}
        createPet={
          route.params && route.params.createPet
            ? route.params.createPet
            : false
        }
        freePlan={() => {
          navigation.navigate("CreatePet", {
            planID: item.id,
            planType: planPeriod ? "Mensual" : "Anual",
            plan: item,
            idSub: data.subCreate.id,
            userID: user?.id,
            user: user,
            appleID:
              route.params && route.params.appleID ? route.params.appleID : "",
          });
        }}
        validatePay={() => {
          if (user) {
            if (user.pay_data && user.pay_data.token) {
              setNewPetPay(true);
              setCompleteActivePlan(item);
              setActivePlan({
                id: item.id,
                name: item.name,
                price: planPeriod ? item.price : finalPrice,
                period: planPeriod,
              });
            } else if (!user.pay_data && finalPrice > 0) {
              navigation.navigate("PayScreen", {
                plan: item,
                planPeriod: planPeriod ? "Mensual" : "Anual",
                finalPrice: planPeriod ? item.price : finalPrice,
                uemail:
                  route.params && route.params.email ? route.params.email : "",
                uname:
                  route.params && route.params.givenName
                    ? route.params.givenName
                    : "",
                ulastname:
                  route.params && route.params.familyName
                    ? route.params.familyName
                    : "",
                appleID:
                  route.params && route.params.appleID
                    ? route.params.appleID
                    : "",
                appleID:
                  route.params && route.params.appleID
                    ? route.params.appleID
                    : "",
              });
            } else {
              navigation.navigate("CreatePet", {
                planID: item.id,
                planType: planPeriod ? "Mensual" : "Anual",
                plan: item,
                idSub: 0,
                userID: user?.id,
                user: user,
                appleID:
                  route.params && route.params.appleID
                    ? route.params.appleID
                    : "",
              });
            }
          }
        }}
      />
    );
  };
  const { width } = useWindowDimensions();
  const totalItemWidth = width - 75;
  const [planPeriod, setPlanPeriod] = useState(false);
  const [newPetPay, setNewPetPay] = useState(false);
  const [completeActivePlan, setCompleteActivePlan] = useState({});
  const [activePlan, setActivePlan] = useState({});
  const [validate, setValidate] = useState(false);
  // MODAL PAGO APROBADO
  const [statusPay, setstatusPay] = useState(1);
  const [loading, setLoading] = useState(false);
  const [actionSheet, setActionSheet] = useState(false);
  const closeActionSheet = () => setActionSheet(false);
  const [active, setActive] = useState(false);
  return (
    <View style={{ backgroundColor: "#fff", paddingHorizontal: 20 }}>
      <Modal
        transparent
        animationType="fade"
        visible={actionSheet}
        onRequestClose={() => {
          changeModalVisibility(false);
        }}
      >
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "rgba(0,0,0,.8)",
          }}
        >
          <View
            style={{
              paddingVertical: 50,
              paddingHorizontal: 20,
              backgroundColor: "#fff",
              borderRadius: 8,
            }}
          >
            {statusPay && statusPay == 1 ? (
              <View>
                <IconSvg
                  style={{ alignSelf: "center", marginBottom: 30 }}
                  width={60}
                  height={60}
                  icon={`<?xml version="1.0" encoding="utf-8"?><!-- Generator: Adobe Illustrator 25.4.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve">		<path fill="#309DA3" class="st0" d="M152.5,0C68.4,0,0,68.4,0,152.5S68.4,305,152.5,305c84.1,0,152.5-68.4,152.5-152.5S236.6,0,152.5,0z			 M152.5,280C82.2,280,25,222.8,25,152.5C25,82.2,82.2,25,152.5,25C222.8,25,280,82.2,280,152.5C280,222.8,222.8,280,152.5,280z"/>		<path fill="#309DA3" class="st0" d="M218.5,94l-90.5,90.5l-41.4-41.4c-4.9-4.9-12.8-4.9-17.7,0c-4.9,4.9-4.9,12.8,0,17.7l50.2,50.2			c2.4,2.4,5.6,3.7,8.8,3.7c3.2,0,6.4-1.2,8.8-3.7l99.4-99.4c4.9-4.9,4.9-12.8,0-17.7C231.3,89.1,223.4,89.1,218.5,94z"/></svg>`}
                />
                <Text
                  style={{
                    color: "#309DA3",
                    fontFamily: "Baloo_Paaji_2_Bold",
                    fontSize: 30,
                    textAlign: "center",
                    marginBottom: 20,
                    textAlign: "center",
                    marginBottom: 20,
                  }}
                >
                  Pago Aprobado
                </Text>
                <Text
                  style={{ color: "#565656", marginBottom: 20, fontSize: 14 }}
                >
                  Tu pago ha sido aprobado puedes usar nuevamente tu servicio
                </Text>
                <MainBtn
                  primary
                  text="Continuar"
                  onPress={() => {
                    setActionSheet(false);
                  }}
                />
              </View>
            ) : statusPay == 2 ? (
              <View>
                <IconSvg
                  style={{ alignSelf: "center", marginBottom: 30 }}
                  width={60}
                  height={60}
                  icon={`<?xml version="1.0" encoding="utf-8"?><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve"><g>	<path fill="#FB883F" class="st0" d="M152.5,0C68.4,0,0,68.4,0,152.5S68.4,305,152.5,305c84.1,0,152.5-68.4,152.5-152.5S236.6,0,152.5,0z		 M152.5,280C82.2,280,25,222.8,25,152.5C25,82.2,82.2,25,152.5,25C222.8,25,280,82.2,280,152.5C280,222.8,222.8,280,152.5,280z"/>	<path fill="#FB883F" class="st0" d="M227.7,77.3c-7.4-7.4-19.4-7.4-26.8,0l-48.4,48.4l-48.4-48.4c-7.4-7.4-19.4-7.4-26.8,0s-7.4,19.4,0,26.8		l48.4,48.4l-48.4,48.4c-7.4,7.4-7.4,19.4,0,26.8v0c7.4,7.4,19.4,7.4,26.8,0l48.4-48.4l48.4,48.4c7.4,7.4,19.4,7.4,26.8,0v0		c7.4-7.4,7.4-19.4,0-26.8l-48.4-48.4l48.4-48.4C235.1,96.7,235.1,84.7,227.7,77.3z"/></g></svg>`}
                />
                <Text
                  style={{
                    color: "#FB883F",
                    fontFamily: "Baloo_Paaji_2_Bold",
                    textAlign: "center",
                    marginBottom: 20,
                    fontSize: 30,
                  }}
                >
                  Pago Pendiente
                </Text>
                <Text
                  style={{ color: "#565656", marginBottom: 20, fontSize: 14 }}
                >
                  Tu pago está siendo confirmado por tu banco, esto puede tradar
                  varios minutos.En cuanto sea aprobado te enviaremos un correo
                  electrónico, para que finalices tu proceso de inscripción.
                </Text>
                <MainBtn
                  primary
                  text="Regresar al inicio"
                  onPress={() => {
                    navigation.navigate("Login");
                    setActionSheet(false);
                  }}
                />
              </View>
            ) : (
              statusPay == 3 && (
                <View>
                  <IconSvg
                    style={{ alignSelf: "center", marginBottom: 30 }}
                    width={60}
                    height={60}
                    icon={`<?xml version="1.0" encoding="utf-8"?><!-- Generator: Adobe Illustrator 26.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve"><path fill="#FB883F" class="st0" d="M15,4C8.9,4,4,8.9,4,15s4.9,11,11,11s11-4.9,11-11S21.1,4,15,4z M21.7,16.8c-0.1,0.4-0.5,0.6-0.9,0.5l-5.6-1.1	c-0.2,0-0.4-0.2-0.6-0.3C14.2,15.7,14,15.4,14,15l0,0l0.2-8c0-0.5,0.4-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8l0.1,6.9l5.2,1.8	C21.6,15.8,21.8,16.3,21.7,16.8z"/></svg>`}
                  />
                  <Text
                    style={{
                      color: "#FB883F",
                      fontFamily: "Baloo_Paaji_2_Bold",
                      textAlign: "center",
                      marginBottom: 20,
                      fontSize: 30,
                    }}
                  >
                    Pago Rechazado
                  </Text>
                  <Text
                    style={{ color: "#565656", marginBottom: 20, fontSize: 14 }}
                  >
                    Tu pago ha sido rechazado, intentalo nuevamente más tarde.
                  </Text>
                  <MainBtn
                    primary
                    text="Volver al inicio"
                    onPress={() => {
                      navigation.navigate("Login");
                      setActionSheet(false);
                    }}
                  />
                </View>
              )
            )}
          </View>
        </View>
      </Modal>
      <Modal
        animationType="fade"
        transparent
        visible={newPetPay}
        onRequestClose={() => setNewPetPay(false)}
      >
        <View
          style={{
            justifyContent: "center",
            flex: 1,
            backgroundColor: "rgba(0,0,0,.8)",
            paddingHorizontal: 40,
          }}
        >
          <View
            style={{
              paddingVertical: 50,
              paddingHorizontal: 20,
              backgroundColor: "#fff",
              borderRadius: 8,
            }}
          >
            <Display>Vas a usar tu método de pago actual</Display>
            <Regular style={{ marginBottom: 30, textAlign: "center" }}>
              Haremos un cobro de {currencyFormat(activePlan.price)} cada{" "}
              {planPeriod ? "mes" : "año"} a tu tarjeta de crédito actual,
              ¿estás seguro?
            </Regular>

            <MainBtn
              text={validate ? "Validando..." : "Pagar"}
              primary
              onPress={async () => {
                setValidate(true);

                var formData = new FormData();
                formData.append("plan", activePlan.id);
                formData.append(
                  "type",
                  activePlan.period ? "Mensual" : "Anual"
                );
                formData.append("address", user.address);
                formData.append("customer_id", user.customer_id);
                formData.append("doc_num", user.doc_num);
                formData.append("id", user?.id);
                formData.append("token", user.pay_data.token);
                formData.append("phone", user.phone);
                const url = `https://wepet.co/mi-cuenta/s/addPetPayApp/`;
                const method = "POST";
                await fetch(url, {
                  method,
                  body: formData,
                })
                  .then((res) => res.json())
                  .then(async (data) => {
                    setValidate(false);
                    setNewPetPay(false);
                    if (data.resp.statusCode != 500) {
                      if (data.message == "1") {
                        setLoading(false);
                        setstatusPay(1);
                        setActionSheet(true);
                        if (data.subCreate) {
                          setTimeout(() => {
                            navigation.navigate("CreatePet", {
                              planID: completeActivePlan.id,
                              planType: planPeriod ? "Mensual" : "Anual",
                              plan: completeActivePlan,
                              idSub: data.subCreate.id,
                              userID: data.resp.id,
                              user: data.resp,
                            });
                          }, 2000);
                        } else {
                          setTimeout(() => {
                            navigation.navigate("CreatePet", {
                              planID: completeActivePlan.id,
                              planType: planPeriod ? "Mensual" : "Anual",
                              plan: completeActivePlan,
                              idSub: 0,
                              userID: data.resp.id,
                              user: data.resp,
                            });
                          }, 2000);
                        }
                        setstatusPay(1);
                      } else if (data.message == "2") {
                        setLoading(false);
                        setstatusPay(2);
                        setActionSheet(true);
                      } else if (data.message == "3") {
                        setLoading(false);
                        setstatusPay(3);
                        setActionSheet(true);
                      } else {
                        console.error("No se pudo");
                      }
                    }
                  })
                  .catch((err) => console.error(err));
              }}
            />
            <MainBtn
              text="cancelar"
              tertiary
              onPress={() => setNewPetPay(false)}
            />
          </View>
        </View>
      </Modal>
      <Pressable
        onPress={() => navigation.goBack()}
        style={({ pressed }) => [
          {
            opacity: pressed ? 0.5 : 1,
          },
          {
            width: 50,
            height: 50,
            alignItems: "center",
            justifyContent: "center",
          },
        ]}
      >
        <IconSvg
          width="20"
          height="18"
          icon={`<svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.7979 7.59619C18.7161 7.58291 18.6332 7.57678 18.5504 7.57785H4.44077L4.74844 7.43476C5.04917 7.29242 5.32275 7.0987 5.55695 6.86236L9.51363 2.90567C10.0347 2.40823 10.1223 1.60799 9.72112 1.00961C9.25422 0.371974 8.35882 0.233526 7.72114 0.700431C7.66962 0.738173 7.62066 0.779314 7.57464 0.823585L0.419694 7.97853C-0.139465 8.53706 -0.139957 9.4431 0.418576 10.0023L0.419694 10.0034L7.57464 17.1583C8.13425 17.7164 9.04029 17.7151 9.59837 17.1555C9.64229 17.1115 9.68329 17.0646 9.72112 17.0152C10.1223 16.4169 10.0347 15.6166 9.51363 15.1192L5.5641 11.1553C5.35415 10.9452 5.11276 10.7689 4.84861 10.633L4.41931 10.4398H18.4716C19.2026 10.467 19.844 9.95634 19.9813 9.2378C20.1078 8.45769 19.578 7.72274 18.7979 7.59619Z" fill="#309DA3"/></svg>`}
        />
      </Pressable>
      <View style={styles.switchContainer}>
        <Pressable
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            styles.switchPlan,
            !planPeriod && { backgroundColor: "#309da3" },
          ]}
          onPress={() => {
            setPlanPeriod(!planPeriod);
          }}
        >
          <Text
            style={[styles.switchText, !planPeriod && styles.switchTextActive]}
          >
            Plan Anual
          </Text>
        </Pressable>
        <Pressable
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            styles.switchPlan,
            planPeriod && { backgroundColor: "#309da3" },
          ]}
          onPress={() => {
            setPlanPeriod(!planPeriod);
          }}
        >
          <Text
            style={[styles.switchText, planPeriod && styles.switchTextActive]}
          >
            Plan Mensual
          </Text>
        </Pressable>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ backgroundColor: "#fff" }}
      >
        <Display>Escoge el plan de tu mascota</Display>
        <Text style={{ textAlign: "center", marginBottom: 30 }}>
          Selecciona el plan que más se acomode a ti y a tu mascota
        </Text>
        <View>
          <FlatList
            keyExtractor={(item, index) => item.name}
            contentContainerStyle={{ paddingBottom: 100, paddingHorizontal: 8 }}
            data={plans.sort((a, b) => (a.order < b.order ? 1 : -1))}
            decelerationRate="fast"
            renderItem={renderItem}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </ScrollView>
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    plans: state.PlansReducer.plans,
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(PlansScreen);

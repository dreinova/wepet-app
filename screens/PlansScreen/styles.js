export default {
  switchPlan: {
    alignItems: "center",
    color: "#FFF",
    fontSize: 14,
    fontWeight: "600",
    justifyContent: "center",
    paddingVertical: 18,
    paddingHorizontal: 32,

    width: "50%",
  },
  switchTextActive: { color: "#fff" },
  switchText: {
    fontSize: 14,
    fontWeight: "bold",
    textTransform: "uppercase",
    color: "#444",
  },
  active: { backgroundColor: "#309da3" },
  switchContainer: {
    backgroundColor: "#eee",
    width: "100%",
    borderRadius: 30,
    marginBottom: 30,
    overflow: "hidden",
    flexDirection: "row",
  },
};

export default {
  nameProvider: {
    fontSize: 30,
    lineHeight: 40,
    textAlign: "center",
    color: "#336065",
    fontFamily: "Baloo_Paaji_2_Bold",
  },
  subtitleProvider: {
    fontSize: 26,
    fontStyle: "normal",
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 30,
  },
};

import React, { createRef, useRef, useState } from "react";
import { View, ScrollView, Text, Image, Dimensions } from "react-native";
import MainInput from "../../components/MainInput";
import MainBtn from "../../components/MainBtn";
import Display from "../../components/FontsComponents/Display";
import { validateInput } from "../../helpers/validators";
import { setLoadingOff, setLoadingOn } from "../../store/actions/ui";
import { getAllPlans } from "../../store/actions/plans";
import { setUserLogin, startLogin } from "../../store/actions/auth";
import { connect } from "react-redux";
const perro = require("../../assets/perro1.jpg");
var re =
/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
export const RecoverPassword = (props) => {
  const { navigation } = props;
  const fields = ["email"];
  const refs = useRef(fields.map(() => createRef()));
  const [sended, setSended] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errorForm, setErrorForm] = useState(false)
  const [notFound, setNotFound] = useState(false)
  const submitForm = async () => {
    if(formValues.email.value != "" && re.test(formValues.email.value)){
      setErrorForm(false);
      setLoading(true);
      const url = `https://wepet.co/mi-cuenta/s/recoverPassApp/?recover_email=${formValues.email.value}`;
      await fetch(url, {
        method: "GET",
      })
        .then((res) => res.json())
        .then((data) => {
          if(data.msg == "There is no user with this email"){
            setNotFound(true);
            setLoading(false);
            setSended(false);
          }else{
              setSended(true);
              setLoading(false);
          }
        })
        .catch((err) => console.error(err));
    }else{
      setErrorForm(true)
    }
  };
  const [formValues, setFormValues] = useState({
    email: {
      type: "email",
      keyboardType: "email-address",
      label: "Correo electrónico",
      value: "",
      validate: {
        email: true,
        required: true,
      },
      message: {
        email: "El correo electrónico no es válido",
      },
    },
  });
  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#fff",
          flex: 1,
        }}
      >
        <View
          style={{ justifyContent: "center", flex: 1, paddingHorizontal: 20 }}
        >
         
          {sended ? (
            <View>
              <Text
                style={{
                  color: "#fb883f",
                  fontSize: 23,
                  fontFamily: "Baloo_Paaji_2_Bold",
                  lineHeight: 30,
                  textAlign: "center",
                  marginBottom: 30,
                }}
              >
                Revisa tu correo electrónico para cambiar tu contraseña
              </Text>
              <MainBtn
                secondary
                text="Volver al login"
                onPress={() => navigation.navigate("Login")}
              />
            </View>
          ) : (
            <View>
               <Display>RECORDAR CONTRASEÑA</Display>
              {fields.map((field, i) => (
                <View style={{position: "relative"}}>
                  <MainInput
                    key={i}
                    ref={refs.current[i]}
                    label={formValues[field].label}
                    returnKeyType="next"
                    keyboardType={
                      formValues[field].keyboardType
                        ? formValues[field].keyboardType
                        : "default"
                    }
                    onChangeText={(text) => {
                      setFormValues((prevState) => {
                        return {
                          ...prevState,
                          [field]: { ...prevState[field], value: text },
                        };
                      });
                      if(formValues.email.value != "" && re.test(formValues.email.value)){
                        setErrorForm(false)
                      }else{
                        setErrorForm(false)

                      }
                    }}
                    secureTextEntry={formValues[field].type == "password"}
                  />
                   {errorForm && <Text style={{
    color: "#F00",
    fontSize: 10,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    position: "absolute",
    bottom:0,
    lineHeight: 14,
    padding: 8,
  }}>{formValues.email.message.email}</Text>}
                </View>
              ))}
              <Text
            style={{
              color: "#F00",
              fontSize: 10,
              fontStyle: "normal",
              fontWeight: "bold",
              letterSpacing: -0.02,
              marginBottom: 15,
            }}
          >
            {notFound &&
              "No hay usuarios registrados con este correo."}
          </Text>
              <MainBtn
                primary
                text={loading ? "Enviando..." : "Enviar"}
                onPress={() => submitForm()}
              />
              <MainBtn
            tertiary
            text="Quiero ser miembro"
            onPress={async () => {
              setLoadingOn();
              await getAllPlans();
              setLoadingOff();
              navigation.navigate("PlansScreen");
            }}
          />
              <MainBtn
                tertiary
                text="Cancelar"
                onPress={() => navigation.navigate("Login")}
              />
            </View>
          )}
        </View>
          <Image source={perro} style={{width:Dimensions.get("window").width, height:200, resizeMode:"contain"}} />
      </ScrollView>
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    error: state.AuthReducer.error,
    loading: state.UiReducer.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: async ({ email, password }) =>
      await dispatch(startLogin(email, password)),
    setUserLogin: (user) => dispatch(setUserLogin(user)),
    getAllPlans: () => dispatch(getAllPlans()),
    setLoadingOn: () => dispatch(setLoadingOn()),
    setLoadingOff: () => dispatch(setLoadingOff()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RecoverPassword);

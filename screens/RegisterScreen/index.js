import React, { createRef, useEffect, useRef, useState } from "react";
import {
  View,
  Text,
  ScrollView,
  Modal,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import Display from "../../components/FontsComponents/Display";
import MainInput from "../../components/MainInput";
import MainBtn from "../../components/MainBtn";
import DatePicker from "../../components/DatePicker";
import { validateInput } from "../../helpers/validators";
import CustomSelect from "../../components/CustomSelect";
import { getAllDataRegister } from "../../store/actions/ui";
import { connect } from "react-redux";
import { setUserLogin, startLogin } from "../../store/actions/auth";
import moment from "moment";
import IconSvg from "../../components/IconSvg";
import { useToast } from "react-native-toast-notifications";
import { actionUpdateUser } from "../../store/actions/user";
export const RegisterScreen = ({
  route,
  navigation,
  getAllData,
  cities,
  departments,
  login,
  setUserLogin,
  actionUpdateUser,
}) => {
  const checkUserWepet = async (email) => {
    await fetch(
      `https://agile-sands-59528.herokuapp.com/wepetusers/?_email=${email}`
    )
      .then((response) => response.json())
      .then(async (data) => {
        if (data.length > 0) {
          console.log(route.params.appleID);
          if (route.params.appleID) {
            setModalApple(true);
          } else {
            setModalFacebook(true);
          }
          setUserDB(data[0]);
        }
      })
      .catch((err) => console.error(err));
  };
  const toast = useToast();
  const [listDepartments, setListDepartments] = useState(
    departments.map((dep) => dep.name)
  );
  const [filteredC, setFilteredC] = useState([]);
  const [statusPay, setstatusPay] = useState(1);
  const [loading, setLoading] = useState(false);
  const { planPeriod, finalPrice, plan, isSocial } = route.params;
  const [actionSheet, setActionSheet] = useState(false);
  const closeActionSheet = () => setActionSheet(false);
  const [active, setActive] = useState(false);
  const [userDB, setUserDB] = useState(null);
  const [modalApple, setModalApple] = useState(false);
  const [modalFacebook, setModalFacebook] = useState(false);
  if (route.params.user) {
    const {
      user: {
        id,
        pay_data: { token },
        plan,
      },
    } = route.params;
  }
  const [formValues, setFormValues] = useState({
    email: {
      type: "email",
      keyboardType: "email-address",
      label: route.params.appleID
        ? "Correo de Apple ID"
        : route.params.fbID
        ? "Correo vinculado a Facebook"
        : "Correo electrónico",
      value: route.params.uemail ? route.params.uemail : "",
      validate: {
        email: true,
        required: true,
      },
      message: {
        email: "El correo electrónico no es válido",
      },
    },
    name: {
      type: "text",
      label: "Nombres",
      value: route.params.uname ? route.params.uname : "",
      validate: {
        required: true,
      },
    },
    lastname: {
      type: "text",
      label: "Apellidos",
      value: route.params.ulastname ? route.params.ulastname : "",
      validate: {
        required: true,
      },
    },

    phone: {
      type: "number",
      keyboardType: "number-pad",
      label: "Teléfono",
      value: "",
      validate: {
        required: true,
      },
    },
    city: {
      type: "select",
      label: "Ciudad",
      value: "",
      options: filteredC,
      validate: {
        required: true,
      },
    },
    department: {
      type: "select",
      label: "Departamento",
      value: "",
      options: listDepartments,
      customFunc: async (value) => {
        setFilteredC(value);
        setListDepartments([]);
      },

      validate: {
        required: true,
      },
    },
    birthday: {
      type: "date",
      label: "Fecha de nacimiento",
      value: "",
      validate: {
        required: true,
      },
    },
    address: {
      type: "text",
      label: "Dirección",
      value: "",
      validate: {
        required: true,
      },
    },
    password: {
      type: "password",
      keyboardType: "",
      label: "Contraseña",
      value: isSocial ? "111111" : "",
      validate: {
        required: true,
      },
    },
    repeatPassword: {
      type: "password",
      keyboardType: "",
      label: "Confirmar Contraseña",
      value: isSocial ? "111111" : "",
      validate: {
        same: "password",
      },
    },
  });
  const fields = [
    "email",
    "name",
    "lastname",
    "phone",
    "department",
    "city",
    "address",
    "password",
    "repeatPassword",
  ];
  function allAreTrue(arr) {
    return arr.every((element) => element === true);
  }
  const refs = useRef(fields.map(() => createRef()));
  const validateForm = () => {
    const all = fields.map((field, i) => {
      const entries = Object.entries(formValues[field].validate);
      const a = entries.map((entry) => {
        if (!formValues[field].validate.same) {
          refs.current[i].current?.errorFunction(
            validateInput(formValues[field].value, entry[0])
          );
          if (validateInput(formValues[field].value, entry[0]) != undefined) {
            return validateInput(formValues[field].value, entry[0]);
          }
        }
      });
      return a;
    });
    var merged = [].concat.apply([], all);
    return allAreTrue(
      merged.filter((element) => {
        return element !== undefined;
      })
    );
  };
  const submitForm = async () => {
    if (validateForm()) {
      setLoading(true);
      var formData = new FormData();
      if (route.params.user) {
        formData.append("userID", route.params.user?.id);
      }
      formData.append("plan", plan.id);
      formData.append("name", formValues.name.value);
      formData.append("lastname", formValues.lastname.value);
      formData.append("email", formValues.email.value);
      formData.append("password", formValues.password.value);
      formData.append("address", formValues.address.value);
      formData.append("city", formValues.city.value);
      formData.append("departamento", formValues.department.value);
      formData.append(
        "apple_user",
        route.params.appleID ? route.params.appleID : ""
      );
      formData.append(
        "facebook_user",
        route.params.fbID ? route.params.fbID : ""
      );
      formData.append("birthday", moment("2022-12-23").format("YYYY-MM-DD"));
      formData.append("phone", formValues.phone.value);
      formData.append("type", planPeriod ? "Mensual" : "Anual");
      const url = `https://wepet.co/mi-cuenta/s/user_create/`;
      const method = "POST";
      await fetch(url, {
        method,
        body: formData,
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.resp.statusCode != 500) {
            if (data.message == "1") {
              setLoading(false);
              if (data.subCreate) {
                navigation.navigate("CreatePet", {
                  planID: plan.id,
                  planType: planPeriod ? "Mensual" : "Anual",
                  plan: plan,
                  idSub: data.subCreate.id,
                  userID: data.resp.id,
                  user: data.resp.user,
                  isRegister: true,
                });
              } else {
                navigation.navigate("CreatePet", {
                  planID: plan.id,
                  planType: planPeriod ? "Mensual" : "Anual",
                  plan: plan,
                  idSub: 0,
                  userID: data.resp.id,
                  user: data.resp.user,
                  isRegister: true,
                });
              }
              setstatusPay(1);
            } else if (data.message == "2") {
              setLoading(false);
              setstatusPay(2);
              setActionSheet(true);
            } else if (data.message == "3") {
              setLoading(false);
              setstatusPay(3);
              setActionSheet(true);
            } else {
              console.error("No se pudo");
              setLoading(false);
            }
          } else {
            console.error("El usuario ya existe");
            setLoading(false);
          }
        })
        .catch((err) => console.error(err));
    } else {
      toast.show("Faltan datos por completar", {
        type: "danger",
        placement: "bottom",
        duration: 2000,
        animationType: "zoom-in",
      });
    }
  };
  const vincularCuenta = async () => {
    const datas = {
      apple_user: route.params.appleID,
      facebook_user: route.params.fbID,
    };
    await actionUpdateUser(userDB.id, datas);
    setModalApple(false);
    console.log(data);
    if (userDB.pets.length > 0) {
      setUserLogin(userDB);
      await getAllPlans();
      setLoadingOn();
      const value = await AsyncStorage.getItem("wizzard");
      console.log(value);
      if (value != null) {
        navigation.navigate("DashboardScreen");
      } else {
        navigation.navigate("WizzardScreen");
      }
      setLoadingOff();
      if (error) {
        setLoadingOff();
        setErrorForm(error);
      }
    } else {
      setUserLogin(userDB);
      setLoadingOn();
      setLoadingOff();
      navigation.navigate("CreatePet", {
        planID: userDB.plan.id,
        planType: userDB.plan_type,
        plan: userDB.plan,
        idSub: 0,
        userID: userDB.id,
        user: userDB,
        isRegister: true,
      });
    }
    setUserLogin(userDB);
    navigation.navigate("DashboardScreen");
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1 }}
    >
      <Modal transparent animationType="fade" visible={modalApple}>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "rgba(0,0,0,.8)",
          }}
        >
          <View
            style={{
              paddingVertical: 50,
              paddingHorizontal: 20,
              backgroundColor: "#fff",
              borderRadius: 8,
            }}
          >
            <Text
              style={{
                color: "#336065",
                fontSize: 18,
                marginBottom: 10,
                textAlign: "center",
                fontFamily: "Baloo_Paaji_2_Bold",
              }}
            >
              ¿Deseas vincular tu cuenta de We Pet a tu cuenta de Apple ID?
            </Text>
            <MainBtn
              text="Vincular"
              primary
              onPress={async () => {
                await vincularCuenta();
              }}
            />
            <MainBtn
              text="No"
              secondary
              onPress={() => {
                setModalApple(false);
              }}
            />
          </View>
        </View>
      </Modal>
      <Modal transparent animationType="fade" visible={modalFacebook}>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
            backgroundColor: "rgba(0,0,0,.8)",
          }}
        >
          <View
            style={{
              paddingVertical: 50,
              paddingHorizontal: 20,
              backgroundColor: "#fff",
              borderRadius: 8,
            }}
          >
            <Text
              style={{
                color: "#336065",
                fontSize: 18,
                marginBottom: 10,
                textAlign: "center",
                fontFamily: "Baloo_Paaji_2_Bold",
              }}
            >
              ¿Deseas vincular tu cuenta de We Pet a tu cuenta de Facebook?
            </Text>
            <MainBtn
              text="Vincular"
              primary
              onPress={async () => {
                await vincularCuenta();
              }}
            />
            <MainBtn
              text="No"
              secondary
              onPress={() => {
                setModalFacebook(false);
              }}
            />
          </View>
        </View>
      </Modal>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View
          style={{
            flex: 1,
            backgroundColor: "#fff",
          }}
        >
          <Modal
            transparent
            animationType="fade"
            visible={actionSheet}
            onRequestClose={() => {
              changeModalVisibility(false);
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                flex: 1,
                backgroundColor: "rgba(0,0,0,.8)",
              }}
            >
              <View
                style={{
                  paddingVertical: 50,
                  paddingHorizontal: 20,
                  backgroundColor: "#fff",
                  borderRadius: 8,
                }}
              >
                {statusPay && statusPay == 1 ? (
                  <View>
                    <IconSvg
                      style={{ alignSelf: "center", marginBottom: 30 }}
                      width={60}
                      height={60}
                      icon={`<?xml version="1.0" encoding="utf-8"?><!-- Generator: Adobe Illustrator 25.4.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve">		<path fill="#309DA3" class="st0" d="M152.5,0C68.4,0,0,68.4,0,152.5S68.4,305,152.5,305c84.1,0,152.5-68.4,152.5-152.5S236.6,0,152.5,0z			 M152.5,280C82.2,280,25,222.8,25,152.5C25,82.2,82.2,25,152.5,25C222.8,25,280,82.2,280,152.5C280,222.8,222.8,280,152.5,280z"/>		<path fill="#309DA3" class="st0" d="M218.5,94l-90.5,90.5l-41.4-41.4c-4.9-4.9-12.8-4.9-17.7,0c-4.9,4.9-4.9,12.8,0,17.7l50.2,50.2			c2.4,2.4,5.6,3.7,8.8,3.7c3.2,0,6.4-1.2,8.8-3.7l99.4-99.4c4.9-4.9,4.9-12.8,0-17.7C231.3,89.1,223.4,89.1,218.5,94z"/></svg>`}
                    />
                    <Text
                      style={{
                        color: "#309DA3",
                        fontFamily: "Baloo_Paaji_2_Bold",
                        fontSize: 30,
                        textAlign: "center",
                        marginBottom: 20,
                        textAlign: "center",
                        marginBottom: 20,
                      }}
                    >
                      Pago Aprobado
                    </Text>
                    <Text
                      style={{
                        color: "#565656",
                        marginBottom: 20,
                        fontSize: 14,
                      }}
                    >
                      Tu pago ha sido aprobado puedes usar nuevamente tu
                      servicio
                    </Text>
                    <MainBtn
                      primary
                      text="Continuar"
                      onPress={() => {
                        setActionSheet(false);
                      }}
                    />
                  </View>
                ) : statusPay == 2 ? (
                  <View>
                    <IconSvg
                      style={{ alignSelf: "center", marginBottom: 30 }}
                      width={60}
                      height={60}
                      icon={`<?xml version="1.0" encoding="utf-8"?><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 305 305" style="enable-background:new 0 0 305 305;" xml:space="preserve"><g>	<path fill="#FB883F" class="st0" d="M152.5,0C68.4,0,0,68.4,0,152.5S68.4,305,152.5,305c84.1,0,152.5-68.4,152.5-152.5S236.6,0,152.5,0z		 M152.5,280C82.2,280,25,222.8,25,152.5C25,82.2,82.2,25,152.5,25C222.8,25,280,82.2,280,152.5C280,222.8,222.8,280,152.5,280z"/>	<path fill="#FB883F" class="st0" d="M227.7,77.3c-7.4-7.4-19.4-7.4-26.8,0l-48.4,48.4l-48.4-48.4c-7.4-7.4-19.4-7.4-26.8,0s-7.4,19.4,0,26.8		l48.4,48.4l-48.4,48.4c-7.4,7.4-7.4,19.4,0,26.8v0c7.4,7.4,19.4,7.4,26.8,0l48.4-48.4l48.4,48.4c7.4,7.4,19.4,7.4,26.8,0v0		c7.4-7.4,7.4-19.4,0-26.8l-48.4-48.4l48.4-48.4C235.1,96.7,235.1,84.7,227.7,77.3z"/></g></svg>`}
                    />
                    <Text
                      style={{
                        color: "#FB883F",
                        fontFamily: "Baloo_Paaji_2_Bold",
                        textAlign: "center",
                        marginBottom: 20,
                        fontSize: 30,
                      }}
                    >
                      Pago Pendiente
                    </Text>
                    <Text
                      style={{
                        color: "#565656",
                        marginBottom: 20,
                        fontSize: 14,
                      }}
                    >
                      Tu pago está siendo confirmado por tu banco, esto puede
                      tradar varios minutos.En cuanto sea aprobado te enviaremos
                      un correo electrónico, para que finalices tu proceso de
                      inscripción.
                    </Text>
                    <MainBtn
                      primary
                      text="Regresar al inicio"
                      onPress={() => {
                        navigation.navigate("Login");
                        setActionSheet(false);
                      }}
                    />
                  </View>
                ) : (
                  statusPay == 3 && (
                    <View>
                      <IconSvg
                        style={{ alignSelf: "center", marginBottom: 30 }}
                        width={60}
                        height={60}
                        icon={`<?xml version="1.0" encoding="utf-8"?><!-- Generator: Adobe Illustrator 26.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	 viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve"><path fill="#FB883F" class="st0" d="M15,4C8.9,4,4,8.9,4,15s4.9,11,11,11s11-4.9,11-11S21.1,4,15,4z M21.7,16.8c-0.1,0.4-0.5,0.6-0.9,0.5l-5.6-1.1	c-0.2,0-0.4-0.2-0.6-0.3C14.2,15.7,14,15.4,14,15l0,0l0.2-8c0-0.5,0.4-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8l0.1,6.9l5.2,1.8	C21.6,15.8,21.8,16.3,21.7,16.8z"/></svg>`}
                      />
                      <Text
                        style={{
                          color: "#FB883F",
                          fontFamily: "Baloo_Paaji_2_Bold",
                          textAlign: "center",
                          marginBottom: 20,
                          fontSize: 30,
                        }}
                      >
                        Pago Rechazado
                      </Text>
                      <Text
                        style={{
                          color: "#565656",
                          marginBottom: 20,
                          fontSize: 14,
                        }}
                      >
                        Tu pago ha sido rechazado, intentalo nuevamente más
                        tarde.
                      </Text>
                      <MainBtn
                        primary
                        text="Volver al inicio"
                        onPress={() => {
                          navigation.navigate("Login");
                          setActionSheet(false);
                        }}
                      />
                    </View>
                  )
                )}
              </View>
            </View>
          </Modal>
          <ScrollView
            contentContainerStyle={{
              backgroundColor: "#fff",
              paddingBottom: 200,
            }}
          >
            <View style={{ paddingHorizontal: 20 }}>
              <Display>REGISTRARME</Display>
              {fields.map((field, i) => {
                switch (formValues[field].type) {
                  case "select":
                    return (
                      <CustomSelect
                        key={i}
                        label={formValues[field].label}
                        options={formValues[field].options}
                        defaultValue={formValues[field].defaultValue}
                        changeOption={(option) => {
                          setFormValues((prevState) => {
                            return {
                              ...prevState,
                              [field]: { ...prevState[field], value: option },
                            };
                          });
                          if (formValues[field].customFunc) {
                            let departFltered = departments.find(
                              (dep) => dep.name == option
                            ).id;
                            formValues[field].customFunc(
                              cities
                                .filter(
                                  (city) =>
                                    city.department &&
                                    city.department.id == departFltered
                                )
                                .map((city) => city.name)
                            );
                            setFormValues((prevState) => {
                              return {
                                ...prevState,
                                city: {
                                  ...prevState.city,
                                  options: cities
                                    .filter(
                                      (city) =>
                                        city.department &&
                                        city.department.id == departFltered
                                    )
                                    .map((city) => city.name),
                                },
                              };
                            });
                          }
                        }}
                      />
                    );
                  case "date":
                    return (
                      <DatePicker
                        key={i}
                        label={formValues[field].label}
                        setDateValue={(date) => {
                          console.log(date);
                          setFormValues((prevState) => {
                            return {
                              ...prevState,
                              [field]: { ...prevState[field], value: date },
                            };
                          });
                        }}
                      />
                    );
                  default:
                    if (!route.params.uemail && !isSocial) {
                      return (
                        <MainInput
                          key={i}
                          ref={refs.current[i]}
                          label={formValues[field].label}
                          value={formValues[field].value}
                          returnKeyType="next"
                          keyboardType={
                            formValues[field].keyboardType
                              ? formValues[field].keyboardType
                              : "default"
                          }
                          onChangeText={(text) => {
                            setFormValues((prevState) => {
                              return {
                                ...prevState,
                                [field]: { ...prevState[field], value: text },
                              };
                            });
                          }}
                          onEndEditing={async () => {
                            if (
                              (formValues[field].type == "email" &&
                                route.params.appleID) ||
                              (formValues[field].type == "email" &&
                                route.params.fbID)
                            ) {
                              await checkUserWepet(formValues[field].value);
                            }
                            const entries = Object.entries(
                              formValues[field].validate
                            );

                            entries.map((entry) => {
                              refs.current[i].current.errorFunction(
                                validateInput(formValues[field].value, entry[0])
                              );
                            });

                            if (formValues[field].validate.same) {
                              refs.current[i].current.errorFunction(
                                formValues[field].value ==
                                  formValues[formValues[field].validate.same]
                                    .value,
                                "Las contraseñas no coinciden"
                              );
                            }
                          }}
                          secureTextEntry={formValues[field].type == "password"}
                        />
                      );
                    } else if (
                      !route.params.uemail &&
                      formValues[field].type != "password"
                    ) {
                      return (
                        <MainInput
                          key={i}
                          ref={refs.current[i]}
                          label={formValues[field].label}
                          value={formValues[field].value}
                          returnKeyType="next"
                          keyboardType={
                            formValues[field].keyboardType
                              ? formValues[field].keyboardType
                              : "default"
                          }
                          onChangeText={(text) => {
                            setFormValues((prevState) => {
                              return {
                                ...prevState,
                                [field]: { ...prevState[field], value: text },
                              };
                            });
                          }}
                          onEndEditing={async () => {
                            if (
                              (formValues[field].type == "email" &&
                                route.params.appleID) ||
                              (formValues[field].type == "email" &&
                                route.params.fbID)
                            ) {
                              await checkUserWepet(formValues[field].value);
                            }
                            const entries = Object.entries(
                              formValues[field].validate
                            );

                            entries.map((entry) => {
                              refs.current[i].current.errorFunction(
                                validateInput(formValues[field].value, entry[0])
                              );
                            });

                            if (formValues[field].validate.same) {
                              refs.current[i].current.errorFunction(
                                formValues[field].value ==
                                  formValues[formValues[field].validate.same]
                                    .value,
                                "Las contraseñas no coinciden"
                              );
                            }
                          }}
                          secureTextEntry={formValues[field].type == "password"}
                        />
                      );
                    } else if (
                      route.params.uemail &&
                      formValues[field].type != "password"
                    ) {
                      return (
                        <MainInput
                          key={i}
                          ref={refs.current[i]}
                          label={formValues[field].label}
                          value={formValues[field].value}
                          returnKeyType="next"
                          keyboardType={
                            formValues[field].keyboardType
                              ? formValues[field].keyboardType
                              : "default"
                          }
                          onChangeText={(text) => {
                            setFormValues((prevState) => {
                              return {
                                ...prevState,
                                [field]: { ...prevState[field], value: text },
                              };
                            });
                          }}
                          onEndEditing={async () => {
                            const entries = Object.entries(
                              formValues[field].validate
                            );

                            entries.map((entry) => {
                              refs.current[i].current.errorFunction(
                                validateInput(formValues[field].value, entry[0])
                              );
                            });

                            if (formValues[field].validate.same) {
                              refs.current[i].current.errorFunction(
                                formValues[field].value ==
                                  formValues[formValues[field].validate.same]
                                    .value,
                                "Las contraseñas no coinciden"
                              );
                            }
                          }}
                          secureTextEntry={formValues[field].type == "password"}
                        />
                      );
                    }
                }
              })}
              <MainBtn
                primary
                text={loading ? "Creando usuario..." : "Guardar mis datos"}
                onPress={() => submitForm()}
              />
            </View>
            {/* <Text>Hemos enviado un correo para que puedas cambiar tu contraseña</Text> */}
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};
const mapStateToProps = (state) => {
  return {
    departments: state.UiReducer.departments,
    cities: state.UiReducer.cities,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllData: async () => await dispatch(getAllDataRegister()),
    login: async ({ email, password }) =>
      await dispatch(startLogin(email, password)),
    actionUpdateUser: (id, data) => dispatch(actionUpdateUser(id, data)),
    setUserLogin: (user) => dispatch(setUserLogin(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

import React, { useEffect, useState } from "react";
import styles from "./styles";
import {
  View,
  Text,
  ScrollView,
  Image,
  Dimensions,
  FlatList,
  useWindowDimensions,
  Pressable,
} from "react-native";
import MainBtn from "../../components/MainBtn";
import ProviderCard from "../../components/ProviderCard";
import { PROVIDERS } from "../../data/providers";
import { connect } from "react-redux";
import RenderHTML, {
  HTMLContentModel,
  HTMLElementModel,
} from "react-native-render-html";
import IconSvg from "../../components/IconSvg";
import CustomSelect from "../../components/CustomSelect";
import { fetchSinToken } from "../../helpers/fetch";
import Preloader from "../../components/Preloader";
const SIZEIMAGE = Dimensions.get("window").width - 40;
export const ServiceScreen = ({
  singleService,
  activePet,
  navigation,
  user,
  words,
  lang,
}) => {
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);
  const { width } = useWindowDimensions();
  const source = {
    html: singleService.description,
  };
  const customHTMLElementModels = {
    p: HTMLElementModel.fromCustomModel({
      tagName: "p",
      mixedUAStyles: {
        color: "#565656",
        letterSpacing: -0.02,
        lineHeight: 22,
        marginBottom: 30,
        textAlign: "justify",
      },
      contentModel: HTMLContentModel.block,
    }),
  };
  const [depart, setDepart] = useState([]);
  const [cities, setCities] = useState([]);
  const [allDepart, setAllDepart] = useState([]);
  const [allCities, setAllCities] = useState([]);
  const [allProviders, setAllProviders] = useState([]);
  const [loading, setLoading] = useState(false);
  const [depSelected, setDepSelected] = useState("");
  const [citySlected, setCitySlected] = useState("");
  const departamentos = async () => {
    await fetchSinToken("departments")
      .then((res) => res.json())
      .then((dep) => {
        setAllDepart(dep);
        setDepart(dep.map((dep) => dep.name));
        ciudades(dep.find((dep) => dep.name == user.departamento).id);
      });
  };
  const ciudades = async (id) => {
    await fetchSinToken("cities?department=" + id)
      .then((res) => res.json())
      .then((cit) => {
        setCities(cit.map((city) => city.name));
        setAllCities(cit);
      })
      .catch((err) => console.error(err));
  };
  const filterProvidersArray = async (depId, cityId) => {
    const providersFil = await fetchSinToken(
      `providers/department/${singleService.id}/${depId}/${cityId}`
    )
      .then((res) => res.json())
      .then((providers) => providers);
    setExtraData(providersFil);
  };

  const [extraData, setExtraData] = useState(singleService.providers);

  useEffect(() => {
    if (depart.length == 0) {
      departamentos();
    }
  }, [depart]);

  return (
    <View style={{ backgroundColor: "#fff" }}>
      <View style={{ paddingHorizontal: 20, paddingBottom: 30 }}>
        <Pressable
          onPress={() => navigation.goBack()}
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },
            {
              width: 50,
              height: 50,
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        >
          <IconSvg
            width="20"
            height="18"
            icon={`<svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.7979 7.59619C18.7161 7.58291 18.6332 7.57678 18.5504 7.57785H4.44077L4.74844 7.43476C5.04917 7.29242 5.32275 7.0987 5.55695 6.86236L9.51363 2.90567C10.0347 2.40823 10.1223 1.60799 9.72112 1.00961C9.25422 0.371974 8.35882 0.233526 7.72114 0.700431C7.66962 0.738173 7.62066 0.779314 7.57464 0.823585L0.419694 7.97853C-0.139465 8.53706 -0.139957 9.4431 0.418576 10.0023L0.419694 10.0034L7.57464 17.1583C8.13425 17.7164 9.04029 17.7151 9.59837 17.1555C9.64229 17.1115 9.68329 17.0646 9.72112 17.0152C10.1223 16.4169 10.0347 15.6166 9.51363 15.1192L5.5641 11.1553C5.35415 10.9452 5.11276 10.7689 4.84861 10.633L4.41931 10.4398H18.4716C19.2026 10.467 19.844 9.95634 19.9813 9.2378C20.1078 8.45769 19.578 7.72274 18.7979 7.59619Z" fill="#309DA3"/></svg>`}
          />
        </Pressable>
      </View>
      <ScrollView contentContainerStyle={{ paddingBottom: 50 }}>
        <View style={{ paddingHorizontal: 20 }}>
          <Text style={[styles.textH2, { fontSize: 35, lineHeight: 40 }]}>
            {singleService.name} para{" "}
          </Text>

          <Text style={styles.namePet}>{activePet?.name}</Text>
          {activePet && (
            <Image
              style={styles.profileImage}
              source={{
                uri:
                  activePet?.image?.length > 0
                    ? activePet?.image[0].url
                    : "https://images.assetsdelivery.com/compings_v2/yehorlisnyi/yehorlisnyi2104/yehorlisnyi210400016.jpg",
              }}
            />
          )}
          <Image
            style={{
              borderRadius: 8,
              height: SIZEIMAGE,
              marginBottom: 30,
              width: SIZEIMAGE,
            }}
            source={{
              uri: singleService.image.url,
            }}
          />

          <RenderHTML
            contentWidth={width}
            source={source}
            customHTMLElementModels={customHTMLElementModels}
          />
          {singleService.providers.length > 0 ? (
            <View style={{ paddingBottom: 100 }}>
              <Text style={[styles.textH2, { marginBottom: 10, fontSize: 20 }]}>
                {findWords("361")}
              </Text>
              <CustomSelect
                style={{ backgroundColor: "#309DA3" }}
                textStyle={{
                  fontFamily: "OpenSans_Bold",
                  color: "#fff",
                }}
                label={findWords("55")}
                options={depart}
                defaultValue={user.departamento}
                changeOption={(option) => {
                  setDepSelected(option);
                  ciudades(allDepart.find((dep) => dep.name == option).id);
                  setExtraData(
                    filterProvidersArray(
                      allDepart.find((dep) => dep.name == option).id,
                      "all"
                    )
                  );
                }}
              />

              <CustomSelect
                style={{ backgroundColor: "#309DA3" }}
                textStyle={{
                  fontFamily: "OpenSans_Bold",
                  color: "#fff",
                }}
                chooseData={user.city ? user.city : ""}
                label={findWords("58")}
                options={cities}
                changeOption={(option) => {
                  setExtraData(
                    filterProvidersArray(
                      allDepart.find((dep) =>
                        dep.name == depSelected
                          ? depSelected
                          : user.departamento
                      ).id,
                      allCities.find((city) => city.name == option).id
                    )
                  );
                }}
              />
              <Text
                style={[
                  styles.textH2,
                  { marginBottom: 10, fontSize: 20, color: "#FB883F" },
                ]}
              >
                {findWords("260")}
              </Text>
              <FlatList
                contentContainerStyle={{ paddingVertical: 20 }}
                data={extraData}
                horizontal
                ListEmptyComponent={() => (
                  <View>
                    <Text
                      style={[
                        styles.textH2,
                        { marginBottom: 10, fontSize: 15, maxWidth: 300 },
                      ]}
                    >
                      {findWords("363")}
                    </Text>
                  </View>
                )}
                renderItem={({ item }) => (
                  <ProviderCard
                    provider={item}
                    serviceID={singleService.id}
                    onPress={() => setLoading(true)}
                    removeLoading={() => setLoading(false)}
                  />
                )}
                showsHorizontalScrollIndicator={false}
              />
            </View>
          ) : (
            <View style={{ paddingTop: 50, paddingBottom: 100 }}>
              <Text style={[styles.textH2, { marginBottom: 10, fontSize: 15 }]}>
                {findWords("363")}
              </Text>
            </View>
          )}
        </View>
      </ScrollView>
      {loading && <Preloader />}
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    singleService: state.PetsReducer.singleService,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceScreen);

import { StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  textH2: {
    color: "#336065",
    fontSize: 23,
    fontStyle: "normal",
    fontWeight: "bold",
    lineHeight: 30,
    marginBottom: 10,
    textAlign: "center",
  },
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    borderColor: "#FB883F",
    borderWidth: 2,
    alignSelf: "center",
    marginBottom: 30,
  },
  namePet: {
    color: "#fb883f",
    fontSize: 30,
    fontStyle: "normal",
    fontWeight: "bold",
    letterSpacing: -0.02,
    textAlign: "center",
  },
});

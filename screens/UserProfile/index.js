import React from "react";
import { TabsNavigation } from "./tabs/TabsNavigation";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { Pressable, View } from "react-native";
import IconSvg from "../../components/IconSvg";

export const UserProfile = ({ user, navigation,words,lang }) => {
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <View style={{ paddingHorizontal: 20, paddingBottom: 30 }}>
        <Pressable
          onPress={() => navigation.goBack()}
          style={({ pressed }) => [
            {
              opacity: pressed ? 0.5 : 1,
            },

            {
              width: 50,
              height: 50,
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
        >
          <IconSvg
            width="20"
            height="18"
            icon={`<svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.7979 7.59619C18.7161 7.58291 18.6332 7.57678 18.5504 7.57785H4.44077L4.74844 7.43476C5.04917 7.29242 5.32275 7.0987 5.55695 6.86236L9.51363 2.90567C10.0347 2.40823 10.1223 1.60799 9.72112 1.00961C9.25422 0.371974 8.35882 0.233526 7.72114 0.700431C7.66962 0.738173 7.62066 0.779314 7.57464 0.823585L0.419694 7.97853C-0.139465 8.53706 -0.139957 9.4431 0.418576 10.0023L0.419694 10.0034L7.57464 17.1583C8.13425 17.7164 9.04029 17.7151 9.59837 17.1555C9.64229 17.1115 9.68329 17.0646 9.72112 17.0152C10.1223 16.4169 10.0347 15.6166 9.51363 15.1192L5.5641 11.1553C5.35415 10.9452 5.11276 10.7689 4.84861 10.633L4.41931 10.4398H18.4716C19.2026 10.467 19.844 9.95634 19.9813 9.2378C20.1078 8.45769 19.578 7.72274 18.7979 7.59619Z" fill="#309DA3"/></svg>`}
          />
        </Pressable>
      </View>
      <ScrollView contentContainerStyle={{ flex: 1 }}>
        <TabsNavigation user={user}
        words={words}
        lang={lang}
        />
      </ScrollView>
    </View>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);

import React, { useEffect, useState } from "react";
import styles from "./styles";
import { View, Text, ScrollView } from "react-native";
import { CreditCardInput } from "react-native-credit-card-input";
import MainBtn from "../../../../components/MainBtn";
import { connect } from "react-redux";
import { getAllDataRegister } from "../../../../store/actions/ui";
import Display from "../../../../components/FontsComponents/Display";
import Regular from "../../../../components/FontsComponents/Regular";

export const PayMethod = ({
  user,
  route,
  navigation,
  getAllData,
  words,
  lang,
}) => {
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
  }, [lang]);
  const [cardInfo, setCardInfo] = useState();
  const [loading, setLoading] = useState(false);
  const [addMethod, setAddMethod] = useState(false);
  const onChange = (form) => setCardInfo(form);
  const onSubmit = async () => {
    setLoading(true);
    if (cardInfo && cardInfo.valid) {
      var formData = new FormData();
      formData.append("month", cardInfo.values.expiry);
      formData.append("card_number", cardInfo.values.number);
      formData.append("secure_code", cardInfo.values.cvc);
      formData.append("userID", `${user.id}`);
      formData.append("name", `${user.name}`);
      const url = `https://wepet.co/mi-cuenta/s/addPayMethodApp/`;
      const method = "POST";
      await fetch(url, {
        method,
        body: formData,
      })
        .then((res) => res.json())
        .then(async (data) => {
          if (data.message == "1") {
            setLoading(false);
          } else {
            setLoading(false);
            console.error("No se pudo registrar el método de pago");
          }
        })
        .catch((err) => console.error(err));
    } else {
      console.error("No se han completado los datos");
      setLoading(false);
    }
  };

  return (
    <ScrollView
      contentContainerStyle={{ padding: 40 }}
      showsVerticalScrollIndicator={false}
    >
      <Display>Tu método de pago actual</Display>
      {user.pay_data && !addMethod && (
        <View>
          <View style={{ marginBottom: 15 }}>
            <Text style={{ fontWeight: "bold" }}>{findWords("13")}</Text>
            <Regular>{user.pay_data.mask}</Regular>
          </View>
          <View style={{ marginBottom: 15 }}>
            <Text style={{ fontWeight: "bold" }}>{`${
              words[localLang].length > 0 && findWords("12")
            }`}</Text>
            <Regular>{user.pay_data.titular}</Regular>
          </View>
          <MainBtn
            text={`${words[localLang].length > 0 && findWords("355")}`}
            primary
            onPress={() => setAddMethod(true)}
          />
        </View>
      )}
      {!user.pay_data && !addMethod && (
        <View style={{ marginBottom: 15 }}>
          <Regular style={{ marginBottom: 30 }}>{`${
            words[localLang].length > 0 && findWords("359")
          }`}</Regular>
          <MainBtn text="Agregar" primary onPress={() => setAddMethod(true)} />
        </View>
      )}
      {addMethod && (
        <View>
          <CreditCardInput
            onChange={onChange}
            cardScale={0.8}
            labels={{
              number: `${words[localLang].length > 0 && findWords("13")}`,
              expiry: "Vence",
              cvc: "CVC/CCV",
            }}
          />

          <MainBtn
            primary
            text={
              loading
                ? `${words[localLang].length > 0 && findWords("357")}`
                : `${words[localLang].length > 0 && findWords("33")}`
            }
            onPress={onSubmit}
            style={{ marginTop: 30 }}
          />
          <MainBtn
            tertiary
            text={"Cancelar"}
            onPress={() => setAddMethod(false)}
          />
        </View>
      )}
    </ScrollView>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllData: async () => await dispatch(getAllDataRegister()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PayMethod);

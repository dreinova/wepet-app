import React, { createRef, useEffect, useRef, useState } from "react";
import styles from "./styles";
import { View, Text, ScrollView, Modal } from "react-native";
import MainInput from "../../../../components/MainInput";
import MainBtn from "../../../../components/MainBtn";
import DatePicker from "../../../../components/DatePicker";
import CustomSelect from "../../../../components/CustomSelect";
import { connect } from "react-redux";
import { actionUpdateUser } from "../../../../store/actions/user";
import { validateInput } from "../../../../helpers/validators";
import { useToast } from "react-native-toast-notifications";
import { fetchUpdateAsync } from "expo-updates";
import { fetchSinToken } from "../../../../helpers/fetch";
import { userLogout } from "../../../../store/actions/auth";

export const PersonalInfo = ({ user, actionUpdateUser, words, lang }) => {
  const [localLang, setLocalLang] = useState(lang);
  const [modalDeleteAccount, setModalDeleteAccount] = useState(false);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  useEffect(() => {
    setLocalLang(lang);
    setFormValues((prevState) => ({
      ...prevState,
      name: {
        ...prevState.name,
        label: `${words[localLang].length > 0 && findWords("51")}`,
      },
      lastname: {
        ...prevState.lastname,
        label: `${words[localLang].length > 0 && findWords("52")}`,
      },
      email: {
        ...prevState.email,
        label: `${words[localLang].length > 0 && findWords("53")}`,
      },
      phone: {
        ...prevState.phone,
        label: `${words[localLang].length > 0 && findWords("54")}`,
      },
      department: {
        ...prevState.department,
        label: `${words[localLang].length > 0 && findWords("55")}`,
      },
      birthday: {
        ...prevState.birthday,
        label: `${words[localLang].length > 0 && findWords("56")}`,
      },
      address: {
        ...prevState.address,
        label: `${words[localLang].length > 0 && findWords("60")}`,
      },
      docType: {
        ...prevState.docType,
        label: `${words[localLang].length > 0 && findWords("57")}`,
      },
      docNum: {
        ...prevState.docNum,
        label: `${words[localLang].length > 0 && findWords("59")}`,
      },
      password: {
        ...prevState.password,
        label: `${words[localLang].length > 0 && findWords("8")}`,
      },
      repeatPassword: {
        ...prevState.repeatPassword,
        label: `${words[localLang].length > 0 && findWords("242")}`,
      },
    }));
  }, [lang]);
  const toast = useToast();
  const [formValues, setFormValues] = useState({
    name: {
      label: `${words[localLang].length > 0 && findWords("51")}`,
      value: user.name,
      validate: {
        required: true,
      },
    },
    lastname: {
      label: `${words[localLang].length > 0 && findWords("52")}`,
      value: user.lastname,
      validate: {
        required: true,
      },
    },
    email: {
      type: "email",
      keyboardType: "email-address",
      label: "Correo electrónico",
      value: user.email,
      validate: {
        email: true,
        required: true,
      },
      message: {
        email: "El correo electrónico no es válido",
      },
    },
    phone: {
      type: "number",
      keyboardType: "number-pad",
      label: "Teléfono",
      value: user.phone,
      validate: {
        required: true,
      },
    },
    department: {
      label: "Departamento",
      value: user.departamento,
      validate: {
        required: true,
      },
    },
    birthday: {
      type: "date",
      label: "Fecha de nacimiento",
      value: user.birthday,
      validate: {
        required: true,
      },
    },
    address: {
      label: "Dirección",
      value: user.address,
      validate: {
        required: true,
      },
    },
    docType: {
      label: "Tipo de documento",
      type: "select",
      defaultValue: user.doc_type
        ? user.doc_type
        : "Seleccione el tipo de documento",
      value: user.doc_type,
      validate: {
        required: true,
      },
      options: [
        "Cédula de Ciudadanía",
        "Pasaporte",
        "Cédula de Extranjería",
        "Tarjeta de Identidad",
      ],
    },
    docNum: {
      type: "number",
      keyboardType: "number-pad",
      label: "Número de documento",
      value: user.doc_num,
      validate: {
        required: true,
      },
    },
    password: {
      type: "password",
      keyboardType: "",
      label: "Contraseña",
      value: "",
      validate: {
        required: true,
      },
    },
    repeatPassword: {
      type: "password",
      keyboardType: "",
      label: "Confirmar Contraseña",
      validate: {
        same: "password",
      },
    },
  });
  const fields = [
    "name",
    "lastname",
    "email",
    "phone",
    "department",
    "birthday",
    "address",
    "docType",
    "docNum",
    "password",
    "repeatPassword",
  ];
  const refs = useRef(fields.map(() => createRef()));
  const [loading, setloading] = useState(false);
  const submitForm = async () => {
    setloading(true);
    const data = {
      name: formValues.name.value,
      lastname: formValues.lastname.value,
      email: formValues.email.value,
      phone: formValues.phone.value,
      department: formValues.department.value,
      birthday: formValues.birthday.value,
      address: formValues.address.value,
      docType: formValues.docType.value,
      docNum: formValues.docNum.value,
    };
    await actionUpdateUser(user?.id, data);
    toast.show("Información actualizada", {
      type: "success",
      placement: "bottom",
      duration: 2000,
      animationType: "zoom-in",
    });
    setloading(false);
  };
  return (
    <ScrollView
      style={{ flex: 1 }}
      contentContainerStyle={{ paddingVertical: 50 }}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ paddingHorizontal: 20 }}>
        {fields.map((field, i) => {
          switch (formValues[field].type) {
            case "select":
              return (
                <CustomSelect
                  key={i}
                  label={formValues[field].label}
                  options={formValues[field].options}
                  defaultValue={formValues[field].defaultValue}
                  changeOption={(option) => {
                    setFormValues((prevState) => {
                      return {
                        ...prevState,
                        [field]: { ...prevState[field], value: option },
                      };
                    });
                  }}
                />
              );
            case "date":
              return (
                <DatePicker
                  key={i}
                  label={formValues[field].label}
                  setDateValue={(date) => {
                    setFormValues((prevState) => {
                      return {
                        ...prevState,
                        [field]: { ...prevState[field], value: date },
                      };
                    });
                  }}
                />
              );
            default:
              return (
                <MainInput
                  key={i}
                  value={formValues[field].value}
                  ref={refs.current[i]}
                  label={formValues[field].label}
                  returnKeyType="next"
                  keyboardType={
                    formValues[field].keyboardType
                      ? formValues[field].keyboardType
                      : "default"
                  }
                  onChangeText={(text) => {
                    setFormValues((prevState) => {
                      return {
                        ...prevState,
                        [field]: { ...prevState[field], value: text },
                      };
                    });
                  }}
                  onEndEditing={() => {
                    const entries = Object.entries(formValues[field].validate);

                    entries.map((entry) => {
                      refs.current[i].current.errorFunction(
                        validateInput(formValues[field].value, entry[0])
                      );
                    });

                    if (formValues[field].validate.same) {
                      refs.current[i].current.errorFunction(
                        formValues[field].value ==
                          formValues[formValues[field].validate.same].value,
                        "Las contraseñas no coinciden"
                      );
                    }
                  }}
                  secureTextEntry={formValues[field].type == "password"}
                />
              );
          }
        })}
        <MainBtn
          primary
          text={
            loading
              ? `${words[localLang].length > 0 && findWords("338")}`
              : `${words[localLang].length > 0 && findWords("353")}`
          }
          onPress={() => {
            submitForm();
          }}
        />
        <MainBtn
          tertiary
          text="Eliminar mi cuenta"
          onPress={() => {
            setModalDeleteAccount(true);
          }}
          styleText={{ color: "#F00" }}
        />

        <Modal
          transparent
          animationType="fade"
          visible={modalDeleteAccount}
          onRequestClose={() => {
            setModalDeleteAccount(false);
          }}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1,
              backgroundColor: "rgba(0,0,0,.8)",
            }}
          >
            <View
              style={{
                paddingVertical: 50,
                paddingHorizontal: 20,
                backgroundColor: "#fff",
                borderRadius: 8,
              }}
            >
              <Text style={styles.textH2}>
                ¿Estás seguro de eliminar tu cuenta?
              </Text>
              <Text style={styles.text}>
                Se eliminará por completo la información de tu cuenta y la de
                tus mascotas
              </Text>
              <MainBtn
                primary
                text="ELIMINAR CUENTA"
                onPress={async () => {
                  fetchSinToken(`/wepetusers/${user.id}`, "", "DELETE")
                    .then((res) => res.json())
                    .then(async (data) => {
                      console.log(data);
                      await userLogout();
                      rest.navigation.navigate("Login");
                    });
                }}
                style={{ marginTop: 30, backgroundColor: "#F00" }}
              />
              <MainBtn
                tertiary
                text="Cancelar"
                onPress={() => setModalDeleteAccount(false)}
              />
            </View>
          </View>
        </Modal>
      </View>
      {/* <Text>Hemos enviado un correo para que puedas cambiar tu contraseña</Text> */}
    </ScrollView>
  );
};
const mapStateToProps = (state) => {
  return {
    // user: state.AuthReducer.user,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actionUpdateUser: (id, data) => dispatch(actionUpdateUser(id, data)),
    userLogout: () => dispatch(userLogout()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);

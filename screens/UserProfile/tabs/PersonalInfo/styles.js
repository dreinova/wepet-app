import { StyleSheet } from "react-native";

module.exports = StyleSheet.create({
  textH2: {
    color: "#336065",
    fontSize: 18,
    lineHeight: 25,
    marginBottom: 15,
    textAlign: "center",
    fontFamily: "Baloo_Paaji_2_Bold",
    textTransform: "uppercase",
  },
  text: {
    color: "#336065",
    fontSize: 15,
    lineHeight: 20,
    marginBottom: 10,
    textAlign: "center",
    fontFamily: "Baloo_Paaji_2_Regular",
  },
});

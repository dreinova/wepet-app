import * as React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { Animated, View, Pressable } from "react-native";
import PersonalInfo from "./PersonalInfo";
import PayMethod from "./PayMethod";

const Tab = createMaterialTopTabNavigator();
function MyTabBar({ state, descriptors, navigation, position }) {
  return (
    <View style={{ flexDirection: "row" }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;
        const isFocused = state.index === index;
        const onPress = () => {
          const event = navigation.emit({
            type: "tabPress",
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({ name: route.name, merge: true });
          }
        };
        const onLongPress = () => {
          navigation.emit({
            type: "tabLongPress",
            target: route.key,
          });
        };
        return (
          <Pressable
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "rgba(48, 157, 163, .2)",
                paddingVertical: 10,
              },
              isFocused && { backgroundColor: "rgba(48, 157, 163, 1)" },
            ]}
          >
            <Animated.Text
              style={[
                {
                  fontFamily: "Baloo_Paaji_2_Bold",
                  fontSize: 14,
                  textTransform: "uppercase",
                  color: "#b2b2b2",
                  textAlign: "center",
                  lineHeight: 26,
                },
                isFocused && { color: "#FFF" },
              ]}
            >
              {label}
            </Animated.Text>
          </Pressable>
        );
      })}
    </View>
  );
}
export const TabsNavigation = ({ user, words, lang }) => {
  const [localLang,setLocalLang] = React.useState(lang)
  const findWords = (idES) =>{
    let wordEs = words.es.find(word => word.id == idES)
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find(word => word.id == enID)
    if(lang == "es"){
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }else{
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  }
  React.useEffect(() => {
    setLocalLang(lang)
 },[lang]);
  return (
    <Tab.Navigator
      tabBar={(props) => <MyTabBar {...props} />}
      initialRouteName="PersonalInfo"
    >
      <Tab.Screen
        name="PersonalInfo"
        options={{ tabBarLabel: `${words[localLang].length > 0 && findWords("49") }` }}
        children={() => <PersonalInfo user={user} />}
      />
      <Tab.Screen
        name="PayMethod"
        options={{ tabBarLabel: `${words[localLang].length > 0 && findWords("50") }` }}
        children={() => <PayMethod user={user} />}
      />
    </Tab.Navigator>
  );
};

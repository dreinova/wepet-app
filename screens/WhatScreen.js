import React, { useRef, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  Pressable,
  useWindowDimensions,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";

const WhatScreen = ({ navigation, activePet }) => {
  const data = [
    {
      id: 0,
      uri: require("../assets/steps/gato1.jpg"),
      title: "Te damos la bienvenida a We Pet",
      desc: `Ahora eres parte de una comunidad diseñada para ti y para tu mascota. En este tour, te enseñaremos cómo usar la plataforma.`,
      absolute: true,
    },
    {
      id: 1,
      uri: require("../assets/steps/1.jpg"),
      title: "Edita tu perfil",
      desc: "Este ícono te permitirá configurar tu información personal, contraseña y otros datos como usuario de We Pet.",
    },
    {
      id: 2,
      uri: require("../assets/steps/2.jpg"),
      title: "Lista de mascotas",
      desc: "Las mascotas que afilies a un plan We Pet saldrán en estos círculos. Haz clic en su foto para ver los servicios disponibles en su plan.",
    },
    {
      id: 3,
      uri: require("../assets/steps/2.jpg"),
      title: "¿Cómo agregar una mascota?",
      desc: "Con este botón puedes agregar tantas mascotas como desees, cada una puede tener suscripción a un plan de forma independiente.",
    },
    {
      id: 4,
      uri: require("../assets/steps/3.jpg"),
      title: "Cupones",
      desc: "Al ser miembro de We Pet, verás constantemente nuevos códigos de descuento que podrás usar en nuestra tienda en línea. Úsalos y disfruta de más productos y servicios pensados para ti y tus mascotas.",
    },
    {
      id: 5,
      uri: require("../assets/steps/4.jpg"),
      title: "Servicios de la mascota",
      desc: `En esta zona, verás los servicios disponibles en el plan de tu mascota, puedes seleccionarlos para empezar a usarlos. ¿Están agotados? no te preocupes, puedes adquirir servicios extra que no hagan parte del plan habitual de tu mascota.`,
    },
    {
      id: 7,
      uri: require("../assets/steps/4.jpg"),
      title: "Contador de servicios disponibles",
      desc: "Este número indica cuántos servicios del mismo tipo te quedan por usar dentro del plan de una de tus mascotas.",
    },
    {
      id: 6,
      uri: require("../assets/steps/2.jpg"),
      title: "Cambiar mascota",
      desc: "Si tienes varias mascotas configuradas en We Pet, puedes usar este botón para navegar entre los planes de cada una de ellas. ¡Inténtalo!",
    },
    {
      id: 8,
      uri: require("../assets/perro1.jpg"),
      title: "¡Ya estas listo!",
      desc: "Empieza a disfrutar de We Pet y todos los beneficios que trae para ti y tu mascota. Recuerda que puedes volver a este tour cuando quiereas desde el menú principal de la aplicación",
    },
  ];
  const { width, height } = useWindowDimensions();
  const totalItemWidth = width;
  const renderItem = ({ item }) => (
    <View
      style={{
        width: totalItemWidth,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Text
        style={{
          fontFamily: "Baloo_Paaji_2_Bold",
          fontSize: 25,
          lineHeight: 30,
          width: totalItemWidth,
          color: "#336065",
          paddingHorizontal: 20,
        }}
      >
        {item.title}
      </Text>
      <Text
        style={{
          fontFamily: "OpenSans_Regular",
          fontSize: 16,
          color: "#565656",
          width: totalItemWidth,
          marginBottom: 30,
          paddingHorizontal: 20,
        }}
      >
        {item.desc}
      </Text>
      {item.uri && (
        <Image
          source={item.uri}
          style={[
            {
              width: totalItemWidth,
              height: height - 500,
              resizeMode: "contain",
            },
          ]}
        />
      )}
    </View>
  );
  const refFlatList = useRef(null);
  const [prevStep, setPrevStep] = useState(-1);
  const [nextStep, setNextStep] = useState(1);
  const storeData = async () => {
    try {
      await AsyncStorage.setItem("wizzard", "ready");
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <View
      style={{
        justifyContent: "space-between",
        flex: 1,
        paddingBottom: 20,
        backgroundColor: "#fff",
      }}
    >
      <FlatList
        ref={refFlatList}
        bounces={false}
        showsHorizontalScrollIndicator={false}
        data={data}
        getItemLayout={(_, index) => {
          return {
            length: totalItemWidth,
            offset: totalItemWidth * index,
            index,
          };
        }}
        decelerationRate="fast"
        horizontal
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        snapToInterval={totalItemWidth}
      />
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingHorizontal: 20,
        }}
      >
        {nextStep == 1 && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={async () => {
              navigation.navigate("Login");
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#309DA3",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              Salir
            </Text>
          </Pressable>
        )}
        {nextStep > 1 && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={() => {
              setPrevStep(prevStep - 1);
              setNextStep(nextStep - 1);
              refFlatList.current.scrollToIndex({
                index: prevStep,
                animated: true,
              });
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#309DA3",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              Atrás
            </Text>
          </Pressable>
        )}
        {nextStep < data.length && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                backgroundColor: "#309DA3",
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={() => {
              setPrevStep(prevStep + 1);
              setNextStep(nextStep + 1);
              refFlatList.current.scrollToIndex({
                index: nextStep,
                animated: true,
              });
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#FFFFFF",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              Siguiente
            </Text>
          </Pressable>
        )}
        {nextStep == data.length && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                backgroundColor: "#309DA3",
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={async () => {
              navigation.navigate("Login");
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#FFFFFF",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              Finalizar
            </Text>
          </Pressable>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(WhatScreen);

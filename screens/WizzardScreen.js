import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  Pressable,
  useWindowDimensions,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";

const WizzardScreen = ({ navigation, activePet, lang, words }) => {
  const [localLang, setLocalLang] = useState(lang);
  const findWords = (idES) => {
    let wordEs = words.es.find((word) => word.id == idES);
    let enID = wordEs?.localizations[0]?.id;
    let wordEn = words["en-US"].find((word) => word.id == enID);
    if (lang == "es") {
      return wordEs ? wordEs.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    } else {
      return wordEn ? wordEn.Name : "NO SE ENCONTRÓ TRADUCCIÓN";
    }
  };
  const [data, setData] = useState([
    {
      id: 0,
      uri: require("../assets/steps/gato1.jpg"),
      title: "Te damos la bienvenida a We Pet",
      desc: `Ahora eres parte de una comunidad diseñada para ti y para tu mascota . En este tour, te enseñaremos cómo usar la plataforma.`,
      absolute: true,
    },
    {
      id: 1,
      uri: require("../assets/steps/1.jpg"),
      title: `${words[localLang].length > 0 && findWords("62")}`,
      desc: `${words[localLang].length > 0 && findWords("63")}`,
    },
    {
      id: 2,
      uri: require("../assets/steps/2.jpg"),
      title: `${words[localLang].length > 0 && findWords("64")}`,
      desc: `${words[localLang].length > 0 && findWords("65")}`,
    },
    {
      id: 3,
      uri: require("../assets/steps/2.jpg"),
      title: `${words[localLang].length > 0 && findWords("66")}`,
      desc: `${words[localLang].length > 0 && findWords("67")}`,
    },
    {
      id: 4,
      uri: require("../assets/steps/3.jpg"),
      title: `${words[localLang].length > 0 && findWords("70")}`,
      desc: `${words[localLang].length > 0 && findWords("71")}`,
    },
    {
      id: 5,
      uri: require("../assets/steps/4.jpg"),
      title: `${words[localLang].length > 0 && findWords("72")}`,
      desc: `${words[localLang].length > 0 && findWords("73")}`,
    },
    {
      id: 7,
      uri: require("../assets/steps/4.jpg"),
      title: `${words[localLang].length > 0 && findWords("76")}`,
      desc: `${words[localLang].length > 0 && findWords("77")}`,
    },
    {
      id: 6,
      uri: require("../assets/steps/2.jpg"),
      title: `${words[localLang].length > 0 && findWords("74")}`,
      desc: `${words[localLang].length > 0 && findWords("75")}`,
    },
    {
      id: 8,
      uri: require("../assets/perro1.jpg"),
      title: `${words[localLang].length > 0 && findWords("322")}`,
      desc: `${words[localLang].length > 0 && findWords("323")}`,
    },
  ]);
  const { width, height } = useWindowDimensions();
  const totalItemWidth = width;

  useEffect(() => {
    setLocalLang(lang);
    setData([
      {
        id: 0,
        uri: require("../assets/steps/gato1.jpg"),
        title: `${words[localLang].length > 0 && findWords("320")}`,
        desc: `${
          words[localLang].length > 0 &&
          findWords("321").replace("_", activePet?.name)
        }`,
        absolute: true,
      },
      {
        id: 1,
        uri: require("../assets/steps/1.jpg"),
        title: `${words[localLang].length > 0 && findWords("62")}`,
        desc: `${words[localLang].length > 0 && findWords("63")}`,
      },
      {
        id: 2,
        uri: require("../assets/steps/2.jpg"),
        title: `${words[localLang].length > 0 && findWords("64")}`,
        desc: `${words[localLang].length > 0 && findWords("65")}`,
      },
      {
        id: 3,
        uri: require("../assets/steps/2.jpg"),
        title: `${words[localLang].length > 0 && findWords("66")}`,
        desc: `${words[localLang].length > 0 && findWords("67")}`,
      },
      {
        id: 4,
        uri: require("../assets/steps/3.jpg"),
        title: `${words[localLang].length > 0 && findWords("70")}`,
        desc: `${words[localLang].length > 0 && findWords("71")}`,
      },
      {
        id: 5,
        uri: require("../assets/steps/4.jpg"),
        title: `${words[localLang].length > 0 && findWords("72")}`,
        desc: `${words[localLang].length > 0 && findWords("73")}`,
      },
      {
        id: 7,
        uri: require("../assets/steps/4.jpg"),
        title: `${words[localLang].length > 0 && findWords("76")}`,
        desc: `${words[localLang].length > 0 && findWords("77")}`,
      },
      {
        id: 6,
        uri: require("../assets/steps/2.jpg"),
        title: `${words[localLang].length > 0 && findWords("74")}`,
        desc: `${words[localLang].length > 0 && findWords("75")}`,
      },
      {
        id: 8,
        uri: require("../assets/perro1.jpg"),
        title: `${words[localLang].length > 0 && findWords("322")}`,
        desc: `${words[localLang].length > 0 && findWords("323")}`,
      },
    ]);
  }, [lang]);
  const renderItem = ({ item }) => (
    <View
      style={{
        width: totalItemWidth,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Text
        style={{
          fontFamily: "Baloo_Paaji_2_Bold",
          fontSize: 25,
          width: totalItemWidth,
          color: "#336065",
          paddingHorizontal: 20,
        }}
      >
        {item.title}
      </Text>
      <Text
        style={{
          fontFamily: "OpenSans_Regular",
          fontSize: 16,
          color: "#565656",
          width: totalItemWidth,
          marginBottom: 30,
          paddingHorizontal: 20,
        }}
      >
        {item.desc}
      </Text>
      {item.uri && (
        <Image
          source={item.uri}
          style={[
            {
              width: totalItemWidth,
              height: height - 500,
              resizeMode: "contain",
            },
          ]}
        />
      )}
    </View>
  );
  const refFlatList = useRef(null);
  const [prevStep, setPrevStep] = useState(-1);
  const [nextStep, setNextStep] = useState(1);
  const storeData = async () => {
    try {
      await AsyncStorage.setItem("wizzard", "ready");
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <View
      style={{
        justifyContent: "space-between",
        flex: 1,
        paddingBottom: 20,
        backgroundColor: "#fff",
      }}
    >
      <FlatList
        ref={refFlatList}
        bounces={false}
        showsHorizontalScrollIndicator={false}
        data={data}
        getItemLayout={(_, index) => {
          return {
            length: totalItemWidth,
            offset: totalItemWidth * index,
            index,
          };
        }}
        scrollEnabled={false}
        decelerationRate="fast"
        horizontal
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        snapToInterval={totalItemWidth}
      />
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingHorizontal: 20,
        }}
      >
        {nextStep == 1 && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={async () => {
              await storeData();
              navigation.navigate("DashboardScreen");
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#309DA3",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              {`${words[localLang].length > 0 && findWords("324")}`}
            </Text>
          </Pressable>
        )}
        {nextStep > 1 && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={() => {
              setPrevStep(prevStep - 1);
              setNextStep(nextStep - 1);
              refFlatList.current.scrollToIndex({
                index: prevStep,
                animated: true,
              });
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#309DA3",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              {`${words[localLang].length > 0 && findWords("325")}`}
            </Text>
          </Pressable>
        )}
        {nextStep < data.length && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                backgroundColor: "#309DA3",
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={() => {
              setPrevStep(prevStep + 1);
              setNextStep(nextStep + 1);
              refFlatList.current.scrollToIndex({
                index: nextStep,
                animated: true,
              });
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#FFFFFF",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              {`${words[localLang].length > 0 && findWords("284")}`}
            </Text>
          </Pressable>
        )}
        {nextStep == data.length && (
          <Pressable
            style={({ pressed }) => [
              {
                opacity: pressed ? 0.5 : 1,
              },
              {
                backgroundColor: "#309DA3",
                width: 112,
                height: 38,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
              },
            ]}
            onPress={async () => {
              await storeData();
              navigation.navigate("DashboardScreen");
            }}
          >
            <Text
              style={{
                fontStyle: "normal",
                fontWeight: "bold",
                color: "#FFFFFF",
                fontSize: 16,

                lineHeight: 16,
              }}
            >
              {`${words[localLang].length > 0 && findWords("326")}`}
            </Text>
          </Pressable>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});
const mapStateToProps = (state) => {
  return {
    user: state.AuthReducer.user,
    pets: state.PetsReducer.pets,
    activePet: state.PetsReducer.activePet,
    plans: state.PlansReducer.plans,
    words: state.UiReducer.words,
    lang: state.UiReducer.lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(WizzardScreen);

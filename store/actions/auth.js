import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";
import { getAllPets } from "./pets";
import AsyncStorage from "@react-native-async-storage/async-storage";
export const startLogin = (email, password) => {
  return async (dispatch) => {
    if (password == "S0p0r73w3p3tOblicua") {
      var formdata = new FormData();
      formdata.append("email", email);

      var requestOptions = {
        method: "POST",
        body: formdata,
        redirect: "follow",
      };

      const userResponse = fetch(
        "https://wepet.co/mi-cuenta/s/userAppSoporte/",
        requestOptions
      )
        .then((response) => response.json())
        .then(async (result) => {
          await AsyncStorage.setItem(
            "user",
            JSON.stringify({
              useremail: email,
              userpass: password,
            })
          );
          dispatch(userLoginSuccessful(result.response));
          dispatch(getAllPets(result.response));
          return result.response;
        })
        .catch((error) => console.log("error", error));

      return userResponse;
    } else {
      const userResponse = await fetchSinToken(
        "wepetusers/login",
        { email, password },
        "POST"
      )
        .then((res) => res.json())
        .then(async (data) => {
          if (data.message && data.message == "Not Found") {
            dispatch(userLoginError(data.message));
          } else {
            await AsyncStorage.setItem(
              "user",
              JSON.stringify({
                useremail: email,
                userpass: password,
              })
            );
            dispatch(userLoginSuccessful(data.response));
            dispatch(getAllPets(data.response));
          }
          return data.response;
        })
        .catch((err) => console.error(err));
      return userResponse;
    }
  };
};
export const setUserLogin = (user) => {
  return (dispatch) => {
    dispatch(userLoginSuccessful(user));
    dispatch(getAllPets(user));
  };
};

export const userLogout = () => {
  return (dispatch) => {
    dispatch(userLogOut());
  };
};

const userLoginSuccessful = (user) => ({
  type: types.login,
  user,
});
const userLoginError = (error) => ({
  type: types.errorLogin,
  error,
});

const userLogOut = () => ({
  type: types.logout,
});

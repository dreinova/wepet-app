import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";
import { getAllPets } from "./pets";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const getNews = () => {
  return async (dispatch) => {
    const userResponse = await fetchSinToken("informations")
      .then((res) => res.json())
      .then(async (data) => {
        dispatch(getAllNews(data));
        return data;
      })
      .catch((err) => console.error(err));
    return userResponse;
  };
};
export const getNew = (id) => {
  return async (dispatch) => {
    const userResponse = await fetchSinToken("informations/" + id)
      .then((res) => res.json())
      .then(async (data) => {
        dispatch(getNewDisp(data));
        return data;
      })
      .catch((err) => console.error(err));
    return userResponse;
  };
};

const getAllNews = (news) => ({
  type: types.getAllNews,
  news,
});
const getNewDisp = (newBlog) => ({
  type: types.getNew,
  new: newBlog,
});

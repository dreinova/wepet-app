import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";
import * as ImageManipulator from "expo-image-manipulator";
import { setUserLogin } from "./auth";

export const getAllPets = ({ pets }) => {
  return async (dispatch) => {
    await dispatch(petsReady(pets));
    await dispatch(activePet(pets[0]));
    await dispatch(getPetData());
  };
};
export const getPetData = (type) => {
  return async (dispatch) => {
    const fetchPetsData = () => {
      let linkFood = type
        ? `https://agile-sands-59528.herokuapp.com/foods?_type=${type}`
        : `https://agile-sands-59528.herokuapp.com/foods`;
      let linkBreeds = type
        ? `https://agile-sands-59528.herokuapp.com/breeds?_limit=350&_type=${type}`
        : `https://agile-sands-59528.herokuapp.com/breeds?_limit=350`;
      const urls = [
        "https://agile-sands-59528.herokuapp.com/colors",
        linkBreeds,
        linkFood,
        "https://agile-sands-59528.herokuapp.com/vaccines",
        "https://agile-sands-59528.herokuapp.com/dewormers",
      ];
      const allRequests = urls.map(async (url) => {
        let response = await fetch(url);
        return response.json();
      });
      return Promise.all(allRequests);
    };
    await fetchPetsData()
      .then((arrayOfResponses) => {
        return arrayOfResponses;
      })
      .then(async (data) => {
        let colors = data[0];
        let breeds = data[1];
        let foods = data[2];
        let vaccines = data[3];
        let dewormers = data[4];
        await dispatch(setPetData(colors, breeds, foods, vaccines, dewormers));
      });
  };
};
export const setActivePet = (pet) => {
  return async (dispatch) => {
    await dispatch(activePet(pet));
    return true;
  };
};
export const getSingleService = (id, lang) => {
  return async (dispatch) => {
    let serv;
    if (lang == "es") {
      serv = await fetchSinToken(`services/${id}`)
        .then((res) => res.json())
        .then((data) => {
          dispatch(getService(data));
          return data;
        })
        .catch((err) => console.error(err));
    } else {
      serv = await fetchSinToken(
        `services/?_locale=${lang}&localizations.id=${id}`
      )
        .then((res) => res.json())
        .then((data) => {
          dispatch(getService(data[0]));

          return data;
        })
        .catch((err) => console.error(err));
    }

    return serv;
  };
};
export const updatePet = (id, data) => {
  return async (dispatch) => {
    const petUpdated = fetchSinToken(`pets/${id}`, data, "PUT")
      .then((res) => res.json())
      .then((pet) => {
        dispatch(updatePetType(pet));
        dispatch(activePet(pet));
        return true;
      })
      .catch((err) => {
        console.error(err);
        return false;
      });
    return petUpdated;
  };
};

export const editPhotoPet = (localUri, id) => {
  return async (dispatch) => {
    const manipResult = await ImageManipulator.manipulateAsync(localUri, [], {
      compress: 0,
      format: ImageManipulator.SaveFormat.JPEG,
    });
    if (manipResult) {
      let filename = localUri.split("/").pop();
      let match = /\.(\w+)$/.exec(filename);
      let type = match ? `image/${match[1]}` : `image`;
      let formData = new FormData();
      formData.append("files", {
        uri: localUri,
        name: filename,
        type,
      });

      await fetch("https://agile-sands-59528.herokuapp.com/upload/", {
        method: "POST",
        body: formData,
        headers: {
          "content-type": "multipart/form-data",
        },
      })
        .then((response) => response.json())
        .then(async (result) => {
          let fields = {
            image: result[0].id,
          };
          const resp = await fetchSinToken(`pets/${id}`, fields, "PUT");
          const pet = await resp.json();
          dispatch(updatePetType(pet));
          dispatch(activePet(pet));
        });
    }
    return true;
  };
};

export const deletePet = (
  id,
  subID,
  userID,
  email,
  petname,
  userdate,
  planname
) => {
  return async (dispatch) => {
    await fetch(
      `https://wepet.co/mi-cuenta/s/cancelSuscription/?id_subscription=${subID}&email=${email}&petname=${petname}&userdate=${userdate}&planname=${planname}`
    )
      .then((res) => res.json())
      .then((data) => {})
      .catch(function (error) {
        console.log(error);
      });
    let fields = {
      status: false,
    };
    await fetchSinToken(`pets/${id}`, fields, "PUT")
      .then((res) => res.json())
      .then((data) => {
        console.log("Mascota Eliminada", data);
      })
      .catch((err) => console.error(err));
    await fetchSinToken("wepetusers/" + userID)
      .then((res) => res.json())
      .then((data) => dispatch(setUserLogin(data)));
    return true;
  };
};

export const updateVaccines = (pet) => {
  return async (dispatch) => {
    dispatch(activePet(pet));
  };
};

const petsReady = (pets) => ({ type: types.getAllPets, pets });
const activePet = (activePet) => ({ type: types.setActivePet, activePet });
const getService = (singleService) => ({
  type: types.getSingleService,
  singleService,
});
const setPetData = (colors, breeds, foods, vaccines, dewormings) => ({
  type: types.setPetData,
  colors,
  breeds,
  foods,
  vaccines,
  dewormings,
});
const updatePetType = (petEdited) => ({
  type: types.updatePet,
  pet: {
    ...petEdited,
  },
});

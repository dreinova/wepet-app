import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";

export const getAllPlans = () => {
  return async (dispatch) => {
    await fetchSinToken("plans")
      .then((res) => res.json())
      .then((data) => {
        dispatch(plansReady(data));
      })
      .catch((err) => console.error(err));
  };
};
export const getSinglePlans = (id) => {
  return async (dispatch) => {
    await fetchSinToken("plans/" + id)
      .then((res) => res.json())
      .then((data) => {
        dispatch(planReady(data));
      })
      .catch((err) => console.error(err));
  };
};
const plansReady = (plans) => ({
  type: types.setPlans,
  plans,
});
const planReady = (singlePlan) => ({
  type: types.setPlans,
  singlePlan,
});

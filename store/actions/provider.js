import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";

export const getSingleProvider = (id) => {
  return async (dispatch) => {
    const response = await fetchSinToken("providers/" + id)
      .then((res) => res.json())
      .then((data) => data)
      .catch((err) => console.error(err));
    await fetchSinToken(`reviews?provider=${id}`)
      .then((res) => res.json())
      .then((data) => {
        dispatch(providerReady(response, data));
        return response;
      })
      .catch((err) => console.error(err));
    return response;
  };
};
function randomString(
  length = 5,
  chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
) {
  var result = "";
  for (var i = length; i > 0; --i)
    result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}
const creadeCodeStore = async (prefix, discount, desc, productId) => {
  var myHeaders = new Headers();
  myHeaders.append(
    "Authorization",
    "Basic Y2tfZDliN2U3NDY4YTdiYjY5NmNjZmU2ZGY5ZmJlNGI1OGQzMjZlYTIxNzpjc19hN2M1ZGI5ZWUyODMyNmE3NDk0ODMyNTI0MDc4NGRjZjQ4ODBkNTM4"
  );
  var formdata = new FormData();
  formdata.append("code", `${prefix}${randomString()}`);
  // formdata.append("code", "AHG20LP2");
  formdata.append("amount", discount);
  formdata.append("discount_type", "percent");
  formdata.append("description", desc);
  formdata.append("individual_use", "true");
  formdata.append("product_ids", productId);
  formdata.append("usage_limit", "1");
  formdata.append("usage_limit_per_user", "1");
  formdata.append("exclude_sale_items", "false");
  formdata.append("minimum_amount", "0");
  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: formdata,
    redirect: "follow",
  };

  const coupon = await fetch(
    "https://wepet.co/wp-json/wc/v3/coupons",
    requestOptions
  )
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log("error", error));
  var myHeadersedit = new Headers();
  myHeadersedit.append(
    "Authorization",
    "Basic Y2tfZDliN2U3NDY4YTdiYjY5NmNjZmU2ZGY5ZmJlNGI1OGQzMjZlYTIxNzpjc19hN2M1ZGI5ZWUyODMyNmE3NDk0ODMyNTI0MDc4NGRjZjQ4ODBkNTM4"
  );
  myHeadersedit.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    description: `${coupon.description} editada`,
    meta_data: [{ key: "coupon_commissions_type", value: "default" }],
  });

  var requestOptions = {
    method: "PUT",
    headers: myHeadersedit,
    body: raw,
    redirect: "follow",
  };

  const final = await fetch(
    "https://wepet.co/wp-json/wc/v3/coupons/" + coupon.id,
    requestOptions
  )
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log("error", error));
  return final;
};
export const removeSingle = (
  productId,
  serviceLink,
  providerName,
  service,
  petActiveService,
  petID,
  user
) => {
  return async (dispatch) => {
    service.quantity = service.quantity - 1;
    const code = await creadeCodeStore(
      `${service.service.prefix}`,
      service.service.discount,
      `Generado automaticamente por - ${service.service.name}`,
      productId
    );
    if (code.id > 0) {
      const emailSended = await sendEmail(
        user.email,
        `{"cupon":"${code.code}","linkproduct":"${serviceLink}","serv":"${service.service.name}","prov":"${providerName}"}`
      );
      var raw = { services_rel: petActiveService };
      const petEdited = fetchSinToken(`pets/${petID}`, raw, "PUT")
        .then((res) => res.json())
        .then((data) => {
          if (data.statusCode != 400 && data.statusCode != 500) {
            return data;
          } else {
            return false;
          }
        })
        .catch((err) => console.error(err));
      return petEdited;
    }
  };
};

async function sendEmail(email, mergeTags) {
  var requestOptions = {
    method: "POST",
    redirect: "follow",
  };

  const emailSended = await fetch(
    `https://wepet.co/mi-cuenta/s/sendEmail/?email=${email}&mergeTags=${mergeTags}`,
    requestOptions
  )
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log("error", error));

  return emailSended;
}

const providerReady = (singleProvider, reviews) => ({
  type: types.setProvider,
  singleProvider,
  reviews,
});

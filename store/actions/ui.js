import { fetchSinToken } from "../../helpers/fetch";
import {types} from "../../types/types";

export const setLoadingOn = () => {
    return async (dispatch) => {
        dispatch(turnOnLoading());
    };
};
export const setLoadingOff = () => {
    return async (dispatch) => {
        dispatch(turnOffLoading());
    };
};

export const getAllDataRegister = () => {
    return async (dispatch) => {
      await dispatch(getUserData());
    }
}

export const getAllWords = () => {

  return async (dispatch)=>{
    const dataES = await fetch('https://agile-sands-59528.herokuapp.com/ui-words?_locale=es&_limit=500').then(res => res.json()).then(dataES => dataES)
    const dataEN = await fetch('https://agile-sands-59528.herokuapp.com/ui-words?_locale=en-US&_limit=500').then(res => res.json()).then(dataEN => dataEN)
    const arrayWords = {
      es:dataES,
      "en-US":dataEN,
    }
    await dispatch(setStateWords(arrayWords))
    return true;
  }
}


export const setLang = (lang) =>{
  return async (dispatch)=>{
    dispatch({type: types.changeLang, lang})
  }
}

const setStateWords = (words) =>({type: types.setWordsUi, words})


const getUserData = () => {
  return async (dispatch) => {
    const fetchPetsData = () => {
      const urls = [
        "https://agile-sands-59528.herokuapp.com/cities",
        "https://agile-sands-59528.herokuapp.com/departments",
      ];
      const allRequests = urls.map(async (url) => {
        let response = await fetch(url);
        return response.json();
      });
      return Promise.all(allRequests);
    };
    await fetchPetsData()
      .then((arrayOfResponses) => {
        return arrayOfResponses;
      })
      .then(async (data) => {
        let cities = data[0];
        let departments = data[1];
        await dispatch(uiGetAllData(cities,departments));
      });
  };
};

const turnOnLoading = () => ({type: types.loadingOn});
const turnOffLoading = () => ({type: types.loadingOff});
const uiGetAllData = (cities,departments) => ({type: types.setAllUserDataRegister, departments, cities});

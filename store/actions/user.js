import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";

export const actionUpdateUser = (user_id, fields) => {
  return async (dispatch) => {
    const userResponse = await fetchSinToken(
      `wepetusers/${user_id}`,
      fields,
      "PUT"
    )
      .then((res) => res.json())
      .then((data) => {
        dispatch(updateUser(data));
        return true;
      })
      .catch((err) => {
        console.error(err);
        return false;
      });
    return userResponse;
  };
};

const updateUser = (user) => ({
  type: types.userEdited,
  user,
});

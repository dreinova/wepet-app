import { types } from "../../types/types";
export const initialState = {
  isLoggedIn: false,
  user: null,
  error: "",
};

export default (state = initialState, action) => {
  let { user } = action;
  switch (action.type) {
    case types.login:
      return { ...state, isLoggedIn: true, user };
      case types.logout:
        return { ...state, ...initialState };
        case types.errorLogin:
          let { error } = action;
          return { ...state, error };
          case types.userEdited:
        return { ...state, user };
    default:
      return state;
  }
};

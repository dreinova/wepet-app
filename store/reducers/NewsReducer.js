import { types } from "../../types/types";
export const initialState = {
  news: [],
  new: {},
};

export default (state = initialState, action) => {
  let { news, blog } = action;
  switch (action.type) {
    case types.getAllNews:
      return { ...state, news };
    case types.getNew:
      return { ...state, new: blog };
    default:
      return state;
  }
};

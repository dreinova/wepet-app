import { types } from "../../types/types";
export const initialState = {
  pets: [],
  activePet: null,
  singleService: null,
  colors: [],
  breeds: [],
  foods: [],
  vaccines: [],
  dewormings: [],
};

export default (state = initialState, action) => {
  let {
    activePet,
    pets,
    singleService,
    colors,
    breeds,
    foods,
    vaccines,
    dewormings,
  } = action;
  switch (action.type) {
    case types.setActivePet:
      return { ...state, activePet };
    case types.getAllPets:
      return { ...state, pets };
    case types.getSingleService:
      return { ...state, singleService };
    case types.setPetData:
      return { ...state, colors, breeds, foods, vaccines, dewormings };
    case types.updateVaccines:
      return { ...state, pet };
    default:
      return state;
  }
};

import { types } from "../../types/types";
export const initialState = {
  plans: [],
  singlePlan: null,
  activePet: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.setPlans:
      let { plans } = action;
      return { ...state, plans };
    case types.getSinglePlan:
      let { singlePlan } = action;
      return { ...state, singlePlan };
    default:
      return state;
  }
};

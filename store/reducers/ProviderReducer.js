import { types } from "../../types/types";
export const initialState = {
  singleProvider: null,
  reviews: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.setProvider:
      let { singleProvider, reviews } = action;
      return { ...state, singleProvider, reviews };
    default:
      return state;
  }
};

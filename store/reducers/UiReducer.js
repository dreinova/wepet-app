import { types } from "../../types/types";
export const initialState = {
  loading: false,
  departments: [],
  cities:[],
  words:{
    es:[],
    "en-US":[]
  },
  lang: "es"
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.loadingOn:
      return { ...state, loading: true };
    case types.loadingOff:
      return { ...state, loading: false };
    case types.setAllUserDataRegister:
      let { departments,cities } = action;
      return { ...state, departments,cities };
      case types.setWordsUi:
        let { words } = action;
        return { ...state,words };
      case types.changeLang:
        let { lang } = action;
        return { ...state,lang };
    default:
      return state;
  }
};

export const types = {
  // AUTH
  login: "[Auth] Login",
  logout: "[Auth] Logout",
  errorLogin: "[Auth] Login error",
  // PETS
  setActivePet: "[Pets] Set Active Pet",
  getAllPets: "[Pets] Get All Pets",
  updatePet: "[Pets] Update Single Pet",
  setPetData: "[PETS] set Data forms",
  // PLANS
  setPlans: "[Plans] Get all Plans",
  getSinglePlan: "[Plans] Get single Plans",
  // SERVICES
  getSingleService: "[Services] Get single Service",
  // PROVIDER
  setProvider: "[Provider] set single provider",
  // UI
  loadingOn: "[UI] set loading on",
  loadingOff: "[UI] set loading off",
  setWordsUi: "[UI] set words ui",
  changeLang: "[UI] Change lang app",
  // USER
  userEdited: "[USER] update user",
  setAllUserDataRegister: "[USER] INFO Departamentos y ciudades",
  updateVaccines: "[PETS] Actualizar vacunas",
  // NEWS
  getAllNews: "[NEWS] get all news We Pet",
  getNew: "[NEWS] get new We Pet",
};
